#ifndef GE5_EXCEPTION_HPP
#define GE5_EXCEPTION_HPP

#include <stdexcept>
#include "Position.hpp"

namespace GE5
{

class Exception : public std::runtime_error
{
public:
  explicit Exception( std::string const& aWhat ) : runtime_error( aWhat )
  {
  }
  virtual ~Exception() {}
};

class EGTException : public Exception
{
public:
  explicit EGTException( std::string const& aWhat ) : Exception( aWhat )
  {
  }
  virtual ~EGTException() {}
};

class InsufficientReductorsException : public Exception
{
public:
  explicit InsufficientReductorsException( std::string const& aWhat ) : Exception( aWhat )
  {
  }
  virtual ~InsufficientReductorsException() {}
};

class InvalidReductorException : public Exception
{
public:
  explicit InvalidReductorException( std::string const& aWhat ) : Exception( aWhat )
  {
  }
  virtual ~InvalidReductorException() {}
};

class ParseError : public Exception
{
public:
  explicit ParseError( GE5::Position const& position, std::wstring const& aWhat ) :
    Exception( boost::locale::conv::from_utf( aWhat, "UTF-8" ) ), mwWhat( aWhat ), mPosition( position )
  {
  }
  virtual ~ParseError() {}

  GE5::Position const& position() const
  {
    return mPosition;
  }

  std::wstring const& wWhat() const
  {
    return mwWhat;
  }

protected:
  std::wstring mwWhat;
  GE5::Position mPosition;
};

} //namespace GE5

#endif //GE5_EXCEPTION_HPP
