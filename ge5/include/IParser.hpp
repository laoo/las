#ifndef GE5_IPARSER_HPP
#define GE5_IPARSER_HPP

namespace GE5
{

class IParser
{
public:
  enum Result
  {
    Accept,
    LexicalError,
    SyntaxError,
    InternalError
  };

  virtual ~IParser() {}

  virtual Result parse() = 0;

};

} //namespace GE5

#endif //GE5_IPARSER_HPP
