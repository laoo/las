#ifndef GE5_IPARSERSERVER_HPP
#define GE5_IPARSERSERVER_HPP

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "IParser.hpp"
#include "Position.hpp"
#include "any.hpp"

namespace GE5
{

class ReductorArg
{
public:
  class Impl;

  ReductorArg( int id, Impl & impl );

  /**
   * Returns the data held in the idx'th token
   */
  any const& operator[]( int idx ) const;
  any & operator[]( int idx );

  template<typename HeldType>
  HeldType const& get( int idx ) const
  {
    return any_cast< HeldType >( operator[]( idx ) );
  }

  template<typename HeldType>
  HeldType move( int idx )
  {
    return any_cast< HeldType >( std::move( operator[]( idx ) ) );
  }


  Position const& pos( int idx ) const;
  Position const& pos() const;

  int id() const;

private:
  int mId;
  Impl & mImpl;
};

typedef std::function<any( ReductorArg & )> Reductor;

class IParserServer
{
public:
  virtual ~IParserServer() {};

  virtual std::unique_ptr<IParser> open( char const* aBegin, char const* aEnd, int id = 0, bool rememberText = false ) = 0;
  virtual std::unique_ptr<IParser> open( char16_t const* aBegin, char16_t const* aEnd, int id = 0, bool rememberText = false ) = 0;
  virtual std::unique_ptr<IParser> open( std::string text, int id = 0, bool rememberText = false ) = 0;
  virtual std::unique_ptr<IParser> open( std::u16string text, int id = 0, bool rememberText = false ) = 0;
  virtual std::unique_ptr<IParser> open( std::wstring source, std::istream & istrm, int id = 0, bool rememberText = false ) = 0;

  virtual void addReductor( std::u16string const& name, Reductor reductor ) = 0;
  virtual void addReductor( std::wstring const& name, Reductor reductor ) = 0;

  virtual bool validateReductors( std::u16string & invalidList ) = 0;

};

std::shared_ptr<IParserServer> createServer( char const* aBegin, char const* aEnd );
std::shared_ptr<IParserServer> createServer( std::vector<char> const& tables );
std::shared_ptr<IParserServer> createServer( std::wstring const& aTableFileName );
std::shared_ptr<IParserServer> createServer( std::ifstream & fin );

} //namespace GE5

#endif //GE5_IPARSERSERVER_HPP
