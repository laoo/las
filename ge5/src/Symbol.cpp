#include "stdafx.h"
#include "Symbol.hpp"
#include "Exception.hpp"

namespace GE5
{

Symbol::Symbol()
{
}

Symbol::Symbol( std::u16string name, Type type, size_t idx ) :
  mName{ std::make_shared<std::u16string>( std::move( name ) ) },
  mType{ type },
  mIdx{ idx },
  mGroupIdx{}
{
}

Symbol::Symbol( Symbol const& other ) :
  mName{ other.mName },
  mType{ other.mType },
  mIdx{ other.mIdx },
  mGroupIdx{ other.mGroupIdx }

{
}

Symbol::Symbol( Symbol && other ) :
  mName{ std::move( other.mName ) },
  mType{ std::move( other.mType ) },
  mIdx{ std::move( other.mIdx ) },
  mGroupIdx{ std::move( other.mGroupIdx ) }
{
}

Symbol & Symbol::operator=( Symbol const& other )
{
  mName = other.mName;
  mType = other.mType;
  mIdx = other.mIdx;
  mGroupIdx = other.mGroupIdx;
  return *this;
}

Symbol & Symbol::operator=( Symbol && other )
{
  mName = std::move( other.mName );
  mType = std::move( other.mType );
  mIdx = std::move( other.mIdx );
  mGroupIdx = std::move( other.mGroupIdx );
  return *this;
}

Symbol::Type Symbol::type() const
{
  return mType;
}

std::u16string const& Symbol::name() const
{
  return *mName;
}

size_t Symbol::idx() const
{
  return mIdx;
}

size_t Symbol::groupIdx() const
{
  if ( !mGroupIdx )
    throw EGTException( "No group in symbol" );
  return *mGroupIdx;
}

void Symbol::setGroup( size_t idx )
{
  mGroupIdx = idx;
}

}

