#include "stdafx.h"
#include "Exception.hpp"
#include "Parser.hpp"
#include "TextReader.hpp"
#include "TextSourceProxy.hpp"
#include "any.hpp"

namespace GE5
{

class ReductorArg::Impl
{
public:
  ReductorArg::Impl( std::vector<Token>::iterator cbegin, std::vector<Token>::iterator cend, Position const& pos );
  ReductorArg::Impl( Position const& pos );

  any const& item( int idx ) const;
  any & item( int idx );
  Position const& pos( int idx ) const;
  Position const& pos() const;

private:
  std::vector<Token>::iterator mCbegin;
  std::vector<Token>::iterator mCend;
  Position const& mPos;
};

ReductorArg::ReductorArg( int id, Impl & impl ) : mId{ id }, mImpl{ impl }
{
}

any const& ReductorArg::operator[]( int idx ) const
{
  return mImpl.item( idx );
}

any & ReductorArg::operator[]( int idx )
{
  return mImpl.item( idx );
}

Position const& ReductorArg::pos( int idx ) const
{
  return mImpl.pos( idx );
}

Position const& ReductorArg::pos() const
{
  return mImpl.pos();
}

int ReductorArg::id() const
{
  return mId;
}

ReductorArg::Impl::Impl( std::vector<Token>::iterator cbegin, std::vector<Token>::iterator cend, Position const& pos ) :
  mCbegin{ cbegin },
  mCend{ cend },
  mPos{ pos }
{
}

ReductorArg::Impl::Impl( Position const& pos ) : mCbegin{}, mCend{}, mPos{ pos }
{
}

any const& ReductorArg::Impl::item( int idx ) const
{
  if ( idx > mCend - mCbegin )
    throw Exception( "Reductor arg out of range" );

  return ( mCbegin + idx )->value();
}

any & ReductorArg::Impl::item( int idx )
{
  if ( idx > mCend - mCbegin )
    throw Exception( "Reductor arg out of range" );

  return ( mCbegin + idx )->value();
}

Position const& ReductorArg::Impl::pos( int idx ) const
{
  if ( idx > mCend - mCbegin )
    throw Exception( "Reductor arg out of range" );

  return ( mCbegin + idx )->pos();
}

Position const& ReductorArg::Impl::pos() const
{
  return mPos;
}

Parser::~Parser()
{
}

Parser::Parser( std::shared_ptr<ParserServer> aServer, std::unique_ptr<TextReader> aTextReader, int id, bool rememberText ) :
  mServer{ std::move( aServer ) },
  mTextReader{ std::move( aTextReader ) },
  mRememberText{ rememberText },
  mCharacterSetTable{ mServer->mCharacterSetTable },
  mSymbolTable{ mServer->mSymbolTable },
  mGroupTable{ mServer->mGroupTable },
  mProductionTable{ mServer->mProductionTable },
  mDFATable{ mServer->mDFATable },
  mLALRTable{ mServer->mLALRTable },
  mCurrentLALR{ mServer->mLALRInitialState },
  mInternalLine{ 1 },
  mInternalColumn{ 1 },
  mStack{},
  mLookaheadBuffer{},
  mId{ id }
{
  if ( mRememberText )
  {
    mSourceProxy = std::make_shared<TextSourceProxy>( mTextReader->source() );
  }
  else
  {
    mSourceProxy = std::make_shared<SourceProxy>( mTextReader->source() );
  }

  mStack.push_back( Token( Symbol(), Position( mSourceProxy ), any(), mServer->mLALRInitialState ) );
}

Parser::Parser( Parser && other ) :
  mServer{ std::move( other.mServer ) },
  mTextReader{ std::move( other.mTextReader ) },
  mSourceProxy{ std::move( other.mSourceProxy ) },
  mRememberText{ other.mRememberText },
  mCharacterSetTable{ mServer->mCharacterSetTable },
  mSymbolTable{ mServer->mSymbolTable },
  mGroupTable{ mServer->mGroupTable },
  mProductionTable{ mServer->mProductionTable },
  mDFATable{ mServer->mDFATable },
  mLALRTable{ mServer->mLALRTable },
  mCurrentLALR{ mServer->mLALRInitialState },
  mInternalLine{ std::move( other.mInternalLine ) },
  mInternalColumn{ std::move( other.mInternalColumn ) },
  mStack{ std::move( other.mStack ) },
  mLookaheadBuffer{ std::move( other.mLookaheadBuffer ) }
{
}


char16_t Parser::lookahead( size_t aIdx )
{
  //should be 'while' but we're reading in strict order.
  if ( aIdx >= mLookaheadBuffer.size() )
  {
    char16_t result = EOFChar; // lead unit of U+d00000 ( 13th unassigned page ).
    mTextReader->read( result );
    mLookaheadBuffer.push_back( result );
  }
  return mLookaheadBuffer[ aIdx ];
}

std::wstring Parser::lookaheadString( size_t size ) const
{
  assert( size < mLookaheadBuffer.size() );

  return boost::locale::conv::utf_to_utf<wchar_t>( &mLookaheadBuffer[ 0 ], &mLookaheadBuffer[ size ] + 1 );
}

void Parser::comsumeBuffer( size_t aCnt )
{
  if ( mRememberText )
  {
    TextSourceProxy * text = static_cast< TextSourceProxy* >( mSourceProxy.get() );

    for ( size_t i = 0; i < aCnt; ++i )
    {
      char16_t c = mLookaheadBuffer[ i ];
      switch ( c )
      {
      case '\n':
        mInternalColumn = 1;
        mInternalLine += 1;
        text->markEol();
        break;
      case '\f':
        break;
      default:
        mInternalColumn += 1;
        text->addChar( c );
        break;
      }
    }
  }
  else
  {
    for ( size_t i = 0; i < aCnt; ++i )
    {
      switch ( mLookaheadBuffer[ i ] )
      {
      case '\n':
        mInternalColumn = 1;
        mInternalLine += 1;
        break;
      case '\f':
        break;
      default:
        mInternalColumn += 1;
        break;
      }
    }
  }

  auto it = std::copy( mLookaheadBuffer.begin() + aCnt, mLookaheadBuffer.end(), mLookaheadBuffer.begin() );
  mLookaheadBuffer.erase( it, mLookaheadBuffer.end() );
}

Symbol const& Parser::findKind( Symbol::Type type ) const
{
  auto symbolIt = std::find_if( mSymbolTable.cbegin(), mSymbolTable.cend(), [type]( Symbol const& symbol ) { return symbol.type() == type; } );
  if ( symbolIt != mSymbolTable.cend() )
    return *symbolIt;
  else
    throw Exception( "No specified symbol" );
}

Token Parser::lookaheadDFA()
{
  int currentDFA = mServer->mDFAInitialState;
  int target = -1;
  int lastAcceptState = -1;
  size_t lastAcceptPosition = 0;

  char16_t ch = lookahead( 0 );

  if ( ch == EOFChar )
    return Token( findKind( Symbol::End ), Position( mSourceProxy, mInternalLine, mInternalColumn ), std::wstring() );

  for ( size_t currentPosition = 0; ; ++currentPosition, ch = lookahead( currentPosition ) )
  {
    if ( ch != EOFChar )
    {
      auto dfa = std::find_if( mDFATable[ currentDFA ].cbegin(), mDFATable[ currentDFA ].cend(), [this, ch]( DFAState::Edge const& e )
      {
        return mCharacterSetTable[ e.charSetIndex ].contains( ch );
      } );

      if ( dfa != mDFATable[ currentDFA ].cend() )
      {
        currentDFA = dfa->targetIndex;
        if ( mDFATable[ currentDFA ].terminalIndex() )
        {
          lastAcceptState = currentDFA;
          lastAcceptPosition = currentPosition;
        }
        continue;
      }
    }

    if ( lastAcceptState == -1 )
    {
      return Token( findKind( Symbol::Error ), Position( mSourceProxy, mInternalLine, mInternalColumn ), lookaheadString( 0 ) );
    }
    else
    {
      return Token( mSymbolTable[ *mDFATable[ lastAcceptState ].terminalIndex() ], Position( mSourceProxy, mInternalLine, mInternalColumn ),
        lookaheadString( lastAcceptPosition ) );
    }
  }
}

Token Parser::produceToken()
{
  for ( ;; )
  {
    Token t = lookaheadDFA();

    bool nestGroup = false;
    if ( t.type() == Symbol::GroupStart )
    {
      if ( mGroupStack.empty() )
        nestGroup = true;
      else
        nestGroup = mGroupTable[ mGroupStack.back().groupIdx() ].containsNesting( t.groupIdx() );
    }

    if ( nestGroup )
    {
      comsumeBuffer( any_cast< std::wstring const& >( t.value() ).size() );
      mGroupStack.push_back( t );
    }
    else if ( mGroupStack.empty() )
    {
      //The token is ready to be analyzed.
      comsumeBuffer( any_cast< std::wstring const& >( t.value() ).size() );
      t.pos().lineEnd = mInternalLine;
      t.pos().columnEnd = mInternalColumn;
      return t;
    }
    else if ( mGroupTable[ mGroupStack.back().groupIdx() ].endIdx() == t.idx() )
    {
      //End the current group
      Token pop = std::move( mGroupStack.back() );
      mGroupStack.pop_back();

      if ( mGroupTable[ pop.groupIdx() ].endingMode() == Group::Closed )
      {
        comsumeBuffer( pop.append( t ) );
      }

      if ( mGroupStack.empty() )
      {
        pop.pos().lineEnd = mInternalLine;
        pop.pos().columnEnd = mInternalColumn;
        return Token( mSymbolTable[ mGroupTable[ pop.groupIdx() ].containerIdx() ], pop.pos(), pop.value() );
      }
      else
      {
        mGroupStack.back().append( pop );
      }
    }
    else if ( t.type() == Symbol::End )
    {
      //EOF always stops the loop. The caller function (Parse) can flag a runaway group error.
      t.pos().lineEnd = mInternalLine;
      t.pos().columnEnd = mInternalColumn;
      return t;
    }
    else
    {
      //We are in a group, Append to the Token on the top of the stack.
      //Take into account the Token group mode  
      Token & top = mGroupStack.back();

      if ( mGroupTable[ top.groupIdx() ].advanceMode() == Group::Token )
      {
        comsumeBuffer( top.append( t ) );
      }
      else
      {
        comsumeBuffer( top.append( t, 1 ) );
      }
    }
  }
}

Parser::LALRResult Parser::parseLALR( Token & token )
{
  LALRState::Action const* pAction = mLALRTable[ mCurrentLALR ].action( token.idx() );

  if ( !pAction )
  {
    std::wstring tok = boost::locale::conv::utf_to_utf< wchar_t >( token.name() );

    std::wstringstream ss;
    ss << L"Unexpected ";

    if ( tok == L"NewLine" )
      ss << "end of line";
    else
    {
      ss << L"Token '" << tok << L"'";

      std::wstring const* pContent = any_cast< std::wstring const >( &token.value() );
      if ( pContent )
        ss << L" of content '" << *pContent << L"'";
    }

    throw GE5::ParseError( Position( mSourceProxy, mInternalLine, mInternalColumn ), ss.str() );
  }

  switch ( pAction->action )
  {
  case LALRState::Action::Shift:
    mStack.push_back( Token( std::move( token ), mCurrentLALR = pAction->targetIndex ) );
    return LALRShift;
  case LALRState::Action::Reduce:
  {
    Production const& p = mProductionTable[ pAction->targetIndex ];
    any reduced;
    Position newPos = mStack.back().pos();
    if ( !p.symbolIndices().empty() )
    {
      size_t elems = p.symbolIndices().size();
      auto b = mStack.begin() + mStack.size() - elems;
      newPos.lineBegin = b->pos().lineBegin;
      newPos.columnBegin = b->pos().columnBegin;
      auto e = mStack.end();
      reduced = p.getReductor()( ReductorArg( mId, ReductorArg::Impl( b, e, newPos ) ) );
      mStack.erase( b, e );
    }
    else
    {
      reduced = p.getReductor()( ReductorArg( mId, ReductorArg::Impl( newPos ) ) );
    }
    int index = mStack.back().state();
    if ( LALRState::Action const* pA = mLALRTable[ index ].action( p.nonterminal() ) )
    {
      mCurrentLALR = pA->targetIndex;
      mStack.push_back( Token( mSymbolTable[ p.nonterminal() ], newPos, std::move( reduced ), mCurrentLALR ) );
      return LALRReduceNormal;
    }
    else
    {
      throw GE5::ParseError( Position( mSourceProxy, mInternalLine, mInternalColumn ), L"LALR parsing internal error" );
    }
  }
  case LALRState::Action::Accept:
    return LALRAccept;
  default:
    throw GE5::ParseError( Position( mSourceProxy, mInternalLine, mInternalColumn ), L"LALR parsing internal error" );
  }
}


Parser::Result Parser::parse()
{
  for ( ;; )
  {
    Token t = produceToken();
    while ( t.type() == Token::Noise )
    {
      t = produceToken();
    }

    if ( t.type() == GE5::Token::Error )
    {
      std::wstringstream ss;
      ss << L"Unexpected character '" << any_cast< std::wstring >( t.value() ) << L"'";

      throw GE5::ParseError( t.pos(), ss.str() );
    }

    LALRResult r;
    do
    {
      r = parseLALR( t );
    } while ( r == LALRReduceNormal );

    if ( r == LALRAccept )
      return Accept;
  }
}

}
