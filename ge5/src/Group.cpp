#include "stdafx.h"
#include "Group.hpp"

namespace GE5
{

Group::Group()
{
}

Group::Group( std::u16string name, int containerIndex, int startIndex, int endIndex, AdvanceMode advanceMode, EndingMode endingMode, std::vector<size_t> nesting )
  : mName{ std::move( name ) }, mContainerIndex{ containerIndex }, mStartIndex{ startIndex }, mEndIndex{ endIndex },
  mAdvanceMode{ advanceMode }, mEndingMode{ endingMode }, mNesting{ std::move( nesting ) }
{
}

size_t Group::containerIdx() const
{
  return mContainerIndex;
}

size_t Group::endIdx() const
{
  return mEndIndex;
}

bool Group::containsNesting( size_t id ) const
{
  return std::find( mNesting.cbegin(), mNesting.cend(), id ) != mNesting.cend();
}

Group::EndingMode Group::endingMode() const
{
  return mEndingMode;
}

Group::AdvanceMode Group::advanceMode() const
{
  return mAdvanceMode;
}


}

