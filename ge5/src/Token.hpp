#ifndef GE5_TOKEN_HPP
#define GE5_TOKEN_HPP

#include "Symbol.hpp"
#include "Position.hpp"
#include "any.hpp"

namespace GE5
{

class Token : public Symbol
{
public:
  Token( Symbol const& aSymbol, Position begin, any aValue, int state = -1 );

  Token( Token const& other );
  Token( Token && other );
  Token & operator=( Token const& other );
  Token & operator=( Token && other );

  // Constructor used during shifting
  Token( Token && aSource, int state );

  any const& value() const;
  any & value();

  Position const& pos() const;
  Position & pos();

  int state() const;

  /**
   * @param limit==0 - no limit, limit>0 - add only limit characters
   * @return number of appended characters
   */
  size_t append( Token const& other, size_t limit = 0 );

private:
  Position mPos;
  any mValue;
  int mState;

};

}

#endif //GE5_SYMBOL_HPP
