#include "stdafx.h"
#include "Production.hpp"

namespace GE5
{

Production::Production()
{
}

Production::Production( int nonterminal, std::vector<int> symbolIndices ) : mNonterminal{ nonterminal }, mSymbolIndices{ std::move( symbolIndices ) }
{
}

void Production::setName( std::u16string name )
{
  mName = name;
}

std::u16string Production::getName() const
{
  return mName;
}

void Production::setReductor( Reductor reductor )
{
  mReductor = std::move( reductor );
}

int Production::nonterminal() const
{
  return mNonterminal;
}

std::vector<int> const& Production::symbolIndices() const
{
  return mSymbolIndices;
}

Reductor const& Production::getReductor() const
{
  return mReductor;
}

}

