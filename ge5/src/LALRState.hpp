#ifndef GE5_LALRSTATE_HPP
#define GE5_LALRSTATE_HPP

namespace GE5
{

class LALRState
{
public:

  struct Action
  {
    enum Type
    {
      Shift = 1,
      Reduce = 2,
      Goto = 3,
      Accept = 4
    };

    Action( size_t aSymbolIndex, Type aAction, int aTargetIndex ) : symbolIndex{ aSymbolIndex }, action{ aAction }, targetIndex{ aTargetIndex } {}

    size_t symbolIndex;
    Type action;
    int targetIndex;
  };


  LALRState();
  LALRState( std::vector< Action > action );

  Action const* action( size_t symbolIdx ) const;

private:
  std::vector< Action > mAction;
};

}

#endif //GE5_LALRSTATE_HPP
