#ifndef GE5_SOURCEPROXY_HPP
#define GE5_SOURCEPROXY_HPP

#include "ISourceProxy.hpp"

namespace GE5
{

class SourceProxy : public ISourceProxy
{
public:

  SourceProxy( std::wstring name );
  virtual ~SourceProxy();

  virtual std::wstring const& getName() const;

private:
  std::wstring const mName;

};

}

#endif //GE5_SOURCEPROXY_HPP
