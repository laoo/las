#ifndef GE5_DFASTATE_HPP
#define GE5_DFASTATE_HPP

namespace GE5
{

class DFAState
{
public:

  struct Edge
  {
    Edge( int aCharSetIndex, int aTargetIndex ) : charSetIndex{ aCharSetIndex }, targetIndex{ aTargetIndex } {}
    int charSetIndex;
    int targetIndex;
  };

  std::vector< DFAState::Edge >::const_iterator cbegin() const;
  std::vector< DFAState::Edge >::const_iterator cend() const;
  std::optional<int> const& terminalIndex() const;

  DFAState();
  DFAState( std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges );

private:
  std::optional<int> mTerminalIndex;
  std::vector< DFAState::Edge > mEdges;

};

}

#endif //GE5_DFASTATE_HPP
