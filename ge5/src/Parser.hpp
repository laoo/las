#ifndef GE5_PARSER_HPP
#define GE5_PARSER_HPP

#include "ParserServer.hpp"
#include "Token.hpp"

namespace GE5
{
class TextReader;
class ISourceProxy;

class Parser : public IParser
{
public:

  ~Parser() override;
  Parser( Parser && other );


  Result parse() override;

private:

  enum LALRResult
  {
    LALRAccept,
    LALRShift,
    LALRReduceNormal
  };

  friend class ParserServer;

  Parser( std::shared_ptr<ParserServer> aServer, std::unique_ptr<TextReader> aTextReader, int id, bool rememberText );
  Parser( Parser const& );

  char16_t lookahead( size_t aIdx );
  std::wstring Parser::lookaheadString( size_t size ) const;

  Token produceToken();
  Token lookaheadDFA();
  LALRResult parseLALR( Token & token );
  void comsumeBuffer( size_t aCnt );
  Symbol const& Parser::findKind( Symbol::Type type ) const;

private:
  std::shared_ptr<ParserServer> mServer;
  std::unique_ptr<TextReader> mTextReader;
  std::shared_ptr<ISourceProxy> mSourceProxy;
  bool mRememberText;

  std::vector<CharacterSetTable> const& mCharacterSetTable;
  std::vector<Symbol> const& mSymbolTable;
  std::vector<Group> const& mGroupTable;
  std::vector<Production> const& mProductionTable;
  std::vector<DFAState> const& mDFATable;
  std::vector<LALRState> const& mLALRTable;

  int mCurrentLALR;
  int mInternalLine;
  int mInternalColumn;

  std::vector<Token> mStack;
  std::vector<Token> mGroupStack;
  std::vector<char16_t> mLookaheadBuffer;

  int mId;

  static const char16_t EOFChar = 0xdb00;
};

}

#endif //GE5_PARSER_HPP
