#include "stdafx.h"
#include "SourceProxy.hpp"

namespace GE5
{

SourceProxy::SourceProxy( std::wstring name ) : mName{ std::move( name ) }
{
}

SourceProxy::~SourceProxy()
{
}

std::wstring const& SourceProxy::getName() const
{
  return mName;
}

}

