#ifndef GE5_SYMBOL_HPP
#define GE5_SYMBOL_HPP

namespace GE5
{

class Symbol
{
public:

  enum Type
  {
    Nonterminal = 0,
    Content = 1,
    Noise = 2,
    End = 3,
    GroupStart = 4,
    GroupEnd = 5,
    Error = 7
  };


  Symbol();
  Symbol( std::u16string name, Type type, size_t idx );

  Symbol( Symbol const& other );
  Symbol( Symbol && other );
  Symbol & operator=( Symbol const& other );
  Symbol & operator=( Symbol && other );

  Type type() const;
  std::u16string const& name() const;
  size_t idx() const;
  size_t groupIdx() const;
  void setGroup( size_t idx );

private:
  std::shared_ptr<std::u16string> mName;
  Type mType;
  size_t mIdx;
  std::optional<size_t> mGroupIdx;

};

}

#endif //GE5_SYMBOL_HPP
