#include "stdafx.h"
#include "Exception.hpp"
#include "ParserServer.hpp"
#include "Parser.hpp"
#include "Record.hpp"
#include "EGTReader.hpp"
#include "TextReader.hpp"

namespace GE5
{

std::shared_ptr<IParserServer> createServer( std::wstring const& aTableFileName )
{
  try
  {
    std::ifstream fin( aTableFileName, std::ios::binary );

    if ( fin.good() )
    {
      return createServer( fin );
    }
    else
      throw EGTException( "Can't open EGT file" );
  }
  catch ( EGTException const& e )
  {
    std::cerr << e.what();
    return std::shared_ptr<ParserServer>();
  }
}

std::shared_ptr<IParserServer> createServer( char const* aBegin, char const* aEnd )
{
  return std::make_shared<ParserServer>( aBegin, aEnd );
}


std::shared_ptr<IParserServer> createServer( std::ifstream & fin )
{
  fin.seekg( 0, std::ios::end );
  size_t length = ( size_t )fin.tellg();
  fin.seekg( 0, std::ios::beg );

  std::vector<char> buf( length );
  fin.read( &buf[ 0 ], length );
  return createServer( buf );
}

std::shared_ptr<IParserServer> createServer( std::vector<char> const& tables )
{
  return std::make_shared<ParserServer>( &tables.front(), &tables.front() + tables.size() );
}

ParserServer::ParserServer( char const* aBegin, char const* aEnd ) : mDFAInitialState{}, mLALRInitialState{}, mReductorsValidated{}
{
  char const* next = readHeader( aBegin );

  EGTReader reader( next, aEnd );

  for ( ;; )
  {
    Record record = reader.read();
    if ( !record.visit( *this ) )
      break;
  }

  std::for_each( mProductionTable.begin(), mProductionTable.end(), [this]( Production & p )
  {
    std::basic_stringstream<char16_t, std::char_traits<char16_t>, std::allocator<char16_t> > ss;
    ss << '<' << mSymbolTable[ p.nonterminal() ].name() << '>' << ' ' << ':' << ':' << '=';

    if ( p.symbolIndices().empty() )
    {
      ss << ' ' << '<' << '>';
    }
    else
    {
      for ( auto it = p.symbolIndices().cbegin(); it != p.symbolIndices().cend() && ( ss << ' ', true ); ++it )
      {
        Symbol const& s = mSymbolTable[ *it ];
        if ( s.type() == Symbol::Nonterminal )
        {
          ss << '<' << s.name() << '>';
        }
        else if ( s.type() == Symbol::Content )
        {
          ss << s.name();
        }
        else
        {
          assert( false );
        }
      }
    }
    p.setName( ss.str() );

  } );

}

ParserServer::~ParserServer()
{
}

char const* ParserServer::readHeader( char const* aHeader ) const
{
#ifdef WIN32
  static wchar_t const header[] = L"GOLD Parser Tables/v5.0";
  typedef wchar_t const wtype;
#else
  static char16_t const header[] = u"GOLD Parser Tables/v5.0";
  typedef char16_t const wtype;
#endif

  bool eq = std::equal( header, header + sizeof( header ) / sizeof( wtype ), reinterpret_cast< wtype* >( aHeader ) );

  if ( !eq )
    throw EGTException( "GOLD Parser Tables/v5.0" );

  return aHeader + sizeof header;
}

std::unique_ptr<IParser> ParserServer::open( char const* aBegin, char const* aEnd, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) );

  return std::unique_ptr<IParser>( new Parser( shared_from_this(), TextReader::create( aBegin, aEnd ), id, rememberText ) );
}

std::unique_ptr<IParser> ParserServer::open( char16_t const* aBegin, char16_t const* aEnd, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) );

  return std::unique_ptr<IParser>( new Parser( shared_from_this(), TextReader::create( aBegin, aEnd ), id, rememberText ) );
}

std::unique_ptr<IParser> ParserServer::open( std::string text, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) );

  return std::unique_ptr<IParser>( new Parser( shared_from_this(), TextReader::create( std::move( text ) ), id, rememberText ) );
}

std::unique_ptr<IParser> ParserServer::open( std::u16string text, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) );

  return std::unique_ptr<IParser>( new Parser( shared_from_this(), TextReader::create( std::move( text ) ), id, rememberText ) );
}

std::unique_ptr<IParser> ParserServer::open( std::wstring source, std::istream & istrm, int id, bool rememberText )
{
  std::u16string invalidList;
  if ( !validateReductors( invalidList ) )
    throw InsufficientReductorsException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( invalidList ), "UTF-8" ) );

  return std::unique_ptr<IParser>( new Parser( shared_from_this(), TextReader::create( std::move( source ), istrm ), id, rememberText ) );
}

void ParserServer::addProperty( int index, std::u16string name, std::u16string value )
{
  mProperties.insert( std::make_pair( std::move( name ), std::move( value ) ) );
}

void ParserServer::setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable )
{
  mSymbolTable.resize( symbolTable );
  mCharacterSetTable.resize( characterSetTable );
  mProductionTable.resize( ruleTable );
  mDFATable.resize( dfaTable );
  mLALRTable.resize( lalrTable );
  mGroupTable.resize( groupTable );
}

void ParserServer::setInitialState( int dfa, int lalr )
{
  mDFAInitialState = dfa;
  mLALRInitialState = lalr;

}

void ParserServer::setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges )
{
  mCharacterSetTable[ index ] = CharacterSetTable( std::move( ranges ) );
}

void ParserServer::addSymbol( int index, std::u16string name, Symbol::Type type )
{
  mSymbolTable[ index ] = Symbol( std::move( name ), type, index );
}

void ParserServer::addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<size_t> nesting )
{
  mGroupTable[ index ] = Group( std::move( name ), containerIndex, startIndex, endIndex, advanceMode, endingMode, std::move( nesting ) );

  mSymbolTable[ containerIndex ].setGroup( index );
  mSymbolTable[ startIndex ].setGroup( index );
  mSymbolTable[ endIndex ].setGroup( index );
}

void ParserServer::addProduction( int index, int nonterminal, std::vector<int> symbolIndices )
{
  mProductionTable[ index ] = Production( nonterminal, std::move( symbolIndices ) );
}

void ParserServer::addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges )
{
  mDFATable[ index ] = DFAState( std::move( terminalIndex ), std::move( edges ) );
}

void ParserServer::addLALRState( int index, std::vector< LALRState::Action > actions )
{
  mLALRTable[ index ] = LALRState( std::move( actions ) );
}

void ParserServer::addReductor( std::u16string const& name, Reductor reductor )
{
  auto it = std::find_if( mProductionTable.begin(), mProductionTable.end(), [&name]( Production & p )
  {
    return p.getName() == name;
  } );

  if ( it == mProductionTable.end() )
    throw InvalidReductorException( boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( name ), "UTF-8" ) );

  it->setReductor( std::move( reductor ) );
}

void ParserServer::addReductor( std::wstring const& name, Reductor reductor )
{
  return addReductor( boost::locale::conv::utf_to_utf<char16_t>( name ), std::move( reductor ) );
}

bool ParserServer::validateReductors( std::u16string & invalidList )
{
  if ( mReductorsValidated )
    return true;

  std::for_each( mProductionTable.cbegin(), mProductionTable.cend(), [&]( Production const& p )
  {
    if ( !p.getReductor() )
    {
      invalidList += p.getName();
      invalidList += '\n';
    }
  } );

  return mReductorsValidated = invalidList.empty();
}

}

