#ifndef GE5_TEXTSOURCEPROXY_HPP
#define GE5_TEXTSOURCEPROXY_HPP

#include "SourceProxy.hpp"

namespace GE5
{

class TextSourceProxy : public SourceProxy
{
public:

  TextSourceProxy( std::wstring name );
  virtual ~TextSourceProxy();

  virtual std::wstring getLine( int number ) const;
  virtual int lines() const;

  void addChar( char16_t c );
  void markEol();

private:
  std::vector< char16_t > mData;
  std::vector< size_t > mLinesOffsets;
};

}

#endif //GE5_TEXTSOURCEPROXY_HPP
