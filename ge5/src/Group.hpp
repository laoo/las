#ifndef GE5_GROUP_HPP
#define GE5_GROUP_HPP

namespace GE5
{

class Group
{
public:

  enum AdvanceMode
  {
    Token = 0,
    Character = 1
  };

  enum EndingMode
  {
    Open = 0,
    Closed = 1
  };

  Group();
  Group( std::u16string name, int containerIndex, int startIndex, int endIndex, AdvanceMode advanceMode, EndingMode endingMode, std::vector<size_t> nesting );

  size_t containerIdx() const;
  size_t endIdx() const;

  bool containsNesting( size_t id ) const;

  EndingMode endingMode() const;
  AdvanceMode advanceMode() const;

private:

  std::u16string mName;
  int mContainerIndex;
  int mStartIndex;
  int mEndIndex;
  AdvanceMode mAdvanceMode;
  EndingMode mEndingMode;
  std::vector<size_t> mNesting;
};

}

#endif //GE5_GROUP_HPP
