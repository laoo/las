#ifndef GE5_PARSERSERVER_HPP
#define GE5_PARSERSERVER_HPP

#include "IParserServer.hpp"
#include "Visitor.hpp"
#include "Production.hpp"


namespace GE5
{
class Parser;




class ParserServer : public Visitor, public IParserServer, public std::enable_shared_from_this<ParserServer>
{
public:
  virtual ~ParserServer();

  virtual std::unique_ptr<IParser> open( char const* aBegin, char const* aEnd, int id, bool rememberText );
  virtual std::unique_ptr<IParser> open( char16_t const* aBegin, char16_t const* aEnd, int id, bool rememberText );
  virtual std::unique_ptr<IParser> open( std::string text, int id, bool rememberText );
  virtual std::unique_ptr<IParser> open( std::u16string text, int id, bool rememberText );
  virtual std::unique_ptr<IParser> open( std::wstring source, std::istream & istrm, int id, bool rememberText );

  virtual void addReductor( std::u16string const& name, Reductor reductor );
  virtual void addReductor( std::wstring const& name, Reductor reductor );

  virtual bool validateReductors( std::u16string & invalidList );

  virtual void addProperty( int index, std::u16string name, std::u16string value );
  virtual void setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable );
  virtual void setInitialState( int dfa, int lalr );
  virtual void setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges );
  virtual void addSymbol( int index, std::u16string name, Symbol::Type type );
  virtual void addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<size_t> nesting );
  virtual void addProduction( int index, int nonterminal, std::vector<int> symbolIndices );
  virtual void addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges );
  virtual void addLALRState( int index, std::vector< LALRState::Action > actions );

  ParserServer( char const* aBegin, char const* aEnd );

private:
  char const* readHeader( char const* aHeader ) const;

private:
  friend class Parser;

  int mDFAInitialState;
  int mLALRInitialState;

  std::vector<CharacterSetTable> mCharacterSetTable;
  std::vector<Symbol> mSymbolTable;
  std::vector<Group> mGroupTable;
  std::vector<Production> mProductionTable;
  std::vector<DFAState> mDFATable;
  std::vector<LALRState> mLALRTable;
  std::map<std::u16string, std::u16string> mProperties;

  bool mReductorsValidated;

};
}

#endif //GE5_PARSERSERVER_HPP
