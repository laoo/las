#include "stdafx.h"
#include "Token.hpp"

namespace GE5
{

Token::Token( Symbol const& aSymbol, Position pos, any aValue, int state ) :
  Symbol{ aSymbol },
  mPos{ pos },
  mValue{ std::move( aValue ) },
  mState{ state }
{
}

Token::Token( Token && aSource, int state ) :
  Symbol{ aSource },
  mPos{ aSource.mPos },
  mValue{ std::move( aSource.mValue ) },
  mState{ state }
{
}

Token::Token( Token const& other ) :
  Symbol{ other },
  mPos{ other.mPos },
  mValue{ other.mValue },
  mState{ other.mState }
{
}

Token::Token( Token && other ) :
  Symbol{ other },
  mPos{ other.mPos },
  mValue{ std::move( other.mValue ) },
  mState{ other.mState }
{
}

Token & Token::operator=( Token const& other )
{
  Symbol::operator=( other );
  mPos = other.mPos;
  mValue = other.mValue;
  mState = other.mState;

  return *this;
}

Token & Token::operator=( Token && other )
{
  Symbol::operator=( std::move( other ) );
  mPos = std::move( other.mPos );
  mValue = std::move( other.mValue );
  mState = std::move( other.mState );

  return *this;
}

any const& Token::value() const
{
  return mValue;
}

any & Token::value()
{
  return mValue;
}

Position const& Token::pos() const
{
  return mPos;
}

Position& Token::pos()
{
  return mPos;
}

int Token::state() const
{
  return mState;
}

size_t Token::append( Token const& other, size_t limit )
{
  std::wstring * data = any_cast< std::wstring >( &mValue );
  size_t oldSize = data->size();
  if ( limit == 0 )
  {
    data->append( any_cast< std::wstring >( other.mValue ) );
    mPos.columnEnd = other.mPos.columnEnd;
    mPos.lineEnd = other.mPos.lineEnd;
    return data->size() - oldSize;
  }
  else
  {
    assert( any_cast< std::wstring >( &other.mValue )->size() >= limit );
    data->append( any_cast< std::wstring >( &other.mValue )->data(), limit );
    // the new position is not calculated because we don't have enough information to do this
    // and this part of code is executed only from within a group processing and the position
    // will be propely updated at group end.
    return limit;
  }
}

}

