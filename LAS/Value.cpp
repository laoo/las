#include "stdafx.h"
#include "Value.hpp"
#include "ge5/include/Position.hpp"
#include "value/Int.hpp"
#include "value/AnonymousLabel.hpp"
#include "value/PC.hpp"
#include "value/Id.hpp"
#include "value/Fun.hpp"
#include "value/Opcode.hpp"
#include "value/OpUnary.hpp"
#include "value/OpBinary.hpp"
#include "value/String.hpp"
#include "value/Decoration.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "Exception.hpp"

namespace las
{

Value Value::intLiteral( GE5::Position const& position, int value )
{
  Value ret;
  ret.mHolder = std::make_shared<value::Int>( position, value );
  return ret;
}

Value Value::anonymousLabel( GE5::Position const& position, bool forward, int skip )
{
  Value ret;
  ret.mHolder = std::make_shared<value::AnonymousLabel>( position, forward ? value::AnonymousLabel::FORWARD : value::AnonymousLabel::BACKWARD, skip );
  return ret;
}

Value Value::pc( GE5::Position const& position )
{
  Value ret;
  ret.mHolder = std::make_shared<value::PC>( position );
  return ret;
}

Value Value::id( GE5::Position const& position, std::vector<std::wstring > name )
{
  Value ret;
  ret.mHolder = std::make_shared<value::Id>( position, std::move( name ) );
  return ret;
}

Value Value::string( GE5::Position const& position, std::wstring value, value::CharType type )
{
  Value ret;
  ret.mHolder = std::make_shared<value::String>( position, value.substr( 1, value.size() - 2 ), type );
  return ret;
}

Value Value::fun( GE5::Position const& position, std::wstring name )
{
  Value ret;
  ret.mHolder = std::make_shared<value::Fun>( position, std::move( name ), std::shared_ptr<value::Holder>() );
  return ret;
}

Value Value::fun( GE5::Position const& position, std::wstring name, Value && arg )
{
  arg = applySpecial( position, std::move( arg ) );
  Value ret;
  ret.mHolder = std::make_shared<value::Fun>( position, std::move( name ), std::move( arg.mHolder ) );
  return ret;
}

Value Value::opcode( GE5::Position const& position, std::shared_ptr<Command> cmd )
{
  Value ret;
  ret.mHolder = std::make_shared<value::Opcode>( position, std::move( cmd ) );
  return ret;
}

Value Value::opUnary( GE5::Position const& position, value::Operators op, Value && value )
{
  value = applySpecial( position, std::move( value ) );
  Value ret;
  ret.mHolder = value::OpUnary::create( position, op, std::move( value.mHolder ) );
  return ret;
}

Value Value::opBinary( GE5::Position const& position, value::Operators op, Value && left, Value && right )
{
  left = applySpecial( position, std::move( left ) );
  right = applySpecial( position, std::move( right ) );

  Value ret;
  ret.mHolder = value::OpBinary::create( position, op, std::move( left.mHolder ), std::move( right.mHolder ) );
  return ret;
}

Value Value::applySpecial( GE5::Position const& position, Value && value )
{
  // calculation of operators <, > and ^ is delayed to the moment if there will be certain that they aren't top most operatots
  // because they are used along with parentheses as a selectors for addressing modes ie < selects direct addressing,
  // > selects absolut addressing and ^ selects long addressing.
  if ( value.directed() )
  {
    value.mType &= ~T_DIRECTED;
    value = opBinary( position, value::OP_BINARY_AND, std::move( value ), intLiteral( position, 0xff ) );
  }
  else if ( value.absoluted() )
  {
    value.mType &= ~T_ABSOLUTED;
    Value temp = opBinary( position, value::OP_BINARY_AND, std::move( value ), intLiteral( position, 0xff00 ) );
    value = opBinary( position, value::OP_SHR, std::move( temp ), intLiteral( position, 8 ) );
  }
  else if ( value.longened() )
  {
    value.mType &= ~T_LONGENED;
    Value temp = opBinary( position, value::OP_BINARY_AND, std::move( value ), intLiteral( position, 0xff0000 ) );
    value = opBinary( position, value::OP_SHR, std::move( temp ), intLiteral( position, 16 ) );
  }

  return std::move( value );
}


Value::Value() : mHolder(), mType( T_NAKEN )
{
}

Value::~Value()
{
}

Value::Value( Value && other ) : mHolder( std::move( other.mHolder ) ), mType( other.mType )
{
}

Value & Value::operator=( Value && other )
{
  if ( &other != this )
  {
    mHolder = std::move( other.mHolder );
    mType = other.mType;
  }
  return *this;
}

Value::Value( Value const& other ) : mHolder( other.mHolder ), mType( other.mType )
{
}

Value & Value::operator=( Value const& other )
{
  if ( &other != this )
  {
    mHolder = other.mHolder;
    mType = other.mType;
  }
  return *this;
}

bool Value::parenthesized() const
{
  return ( mType & T_PARENTHESIZED ) != 0;
}

bool Value::directed() const
{
  return ( mType & T_DIRECTED ) != 0;
}

bool Value::absoluted() const
{
  return ( mType & T_ABSOLUTED ) != 0;
}

bool Value::longened() const
{
  return ( mType & T_LONGENED ) != 0;
}

Value & Value::parenthesize()
{
  mType |= T_PARENTHESIZED;
  return *this;
}

Value & Value::direct()
{
  mType |= T_DIRECTED;
  return *this;
}

Value & Value::absolute()
{
  mType |= T_ABSOLUTED;
  return *this;
}

Value & Value::longen()
{
  mType |= T_LONGENED;
  return *this;
}

Value & Value::prependName( std::vector< std::wstring > name )
{
  if ( auto pHolder = std::dynamic_pointer_cast<value::Fun>( mHolder ) )
  {
    pHolder->prependName( name );
  }
  else
  {
    assert( false );
  }

  return *this;
}

bool Value::literal( int & out ) const
{
  return mHolder->literal( out );
}

value::Host Value::calcValue( CalcContext & context, value::UsageReq req ) const
{
  if ( ( mType & T_DIRECTED ) != 0 )
  {
    assert( ( mType & T_ABSOLUTED ) == 0 );
    assert( ( mType & T_LONGENED ) == 0 );
    auto result = mHolder->calcValue( context, value::D_DIRECTED, req );
    if ( auto seg = result.getSegment() )
      context.appendToEmitList( seg );
    return result;
  }
  if ( ( mType & T_ABSOLUTED ) != 0 )
  {
    assert( ( mType & T_DIRECTED ) == 0 );
    assert( ( mType & T_LONGENED ) == 0 );
    auto result = mHolder->calcValue( context, value::D_ABSOLUTED, req );
    if ( auto seg = result.getSegment() )
      context.appendToEmitList( seg );
    return result;
  }
  if ( ( mType & T_LONGENED ) != 0 )
  {
    assert( ( mType & T_DIRECTED ) == 0 );
    assert( ( mType & T_ABSOLUTED ) == 0 );
    auto result = mHolder->calcValue( context, value::D_LONGENED, req );
    if ( auto seg = result.getSegment() )
      context.appendToEmitList( seg );
    return result;
  }
  auto result = mHolder->calcValue( context, value::D_NONE, req );
  if ( auto seg = result.getSegment() )
    context.appendToEmitList( seg );
  return result;
}

value::Measure Value::measureValue( MeasureContext & context, value::UsageReq req ) const
{
  if ( ( mType & T_DIRECTED ) != 0 )
  {
    assert( ( mType & T_ABSOLUTED ) == 0 );
    assert( ( mType & T_LONGENED ) == 0 );
    return mHolder->measureValue( context, value::D_DIRECTED, req );
  }
  if ( ( mType & T_ABSOLUTED ) != 0 )
  {
    assert( ( mType & T_DIRECTED ) == 0 );
    assert( ( mType & T_LONGENED ) == 0 );
    return mHolder->measureValue( context, value::D_ABSOLUTED, req );
  }
  if ( ( mType & T_LONGENED ) != 0 )
  {
    assert( ( mType & T_DIRECTED ) == 0 );
    assert( ( mType & T_ABSOLUTED ) == 0 );
    return mHolder->measureValue( context, value::D_LONGENED, req );
  }
  return mHolder->measureValue( context, value::D_NONE, req );
}

std::optional< int > Value::retrieveBank( CalcContext & context ) const
{
  return mHolder->retrieveBank( context );
}

} //namespace las

