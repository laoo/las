#include "stdafx.h"
#include "Out.hpp"

namespace las
{

Out::Out( GE5::Position const& position, std::wstring name, std::wstring attr ) : PositionHolder( position ),
  mName( std::move( name ) ), mAttr( std::move( attr ) )
{
}

Out::~Out()
{
}

std::wstring Out::getName() const
{
  return mName;
}


} //namespace las

