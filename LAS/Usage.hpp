#ifndef LAS_USAGE_HPP
#define LAS_USAGE_HPP

#include "SegmentContent.hpp"
#include "Value.hpp"
#include "value/Decoration.hpp"

namespace las
{

class Usage : public SegmentContent
{
public:

  struct Placement
  {
    enum Kind
    {
      POSITIVE_OFFSET,
      NEGATIVE_OFFSET,
      ANCHOR,  //used to anchor virtual segments to specific value
      NONE
    };

    Placement() : kind( NONE ), value() {}
    Placement( Kind k, Value && v ) : kind( k ), value( std::move( v ) ) {}
    Placement( Placement && other ) : kind( other.kind ), value( std::move( other.value ) ) {}
    Placement( Placement const& other ) : kind( other.kind ), value( other.value ) {}

    Kind kind;
    Value value;
  };

  enum Addressing
  {
    A_IMPLICIT,
    A_DIRECT,
    A_ABSOLUTE,
    A_LONG
  };

  std::wstring const& getAlias() const;
  std::vector< std::wstring > const& getName() const;
  Addressing getAddressing() const;
  bool compatibleAddressing( value::Decoration dec ) const;
  value::Decoration compatibleDecoration() const;

  Usage( GE5::Position const& position, Addressing adr, std::vector< std::wstring > name, Placement && placement, std::wstring as );
  ~Usage();

  bool namespacewiseEqual( Usage const& other );

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  std::vector< std::wstring > mName;
  Addressing mAdr;
  Placement mPlacement;
  std::wstring mAs;
};

} //namespace las

#endif //LAS_USAGE_HPP
