#ifndef LAS_USAGESHIFT_HPP
#define LAS_USAGESHIFT_HPP

#include "SegmentContent.hpp"
#include <Value.hpp>

namespace las
{

class UsageShift : public SegmentContent
{
public:
  enum Direction
  {
    POSITIVE,
    NEGATIVE
  };

  UsageShift( GE5::Position const& position, std::vector< std::wstring > name, Direction dir, Value && offset );
  ~UsageShift();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  std::vector< std::wstring > mName;
  Direction mDir;
  Value mOffset;
};

} //namespace las

#endif //LAS_USAGESHIFT_HPP
