#ifndef LAS_EQU_HPP
#define LAS_EQU_HPP

#include "SegmentContent.hpp"
#include "Value.hpp"


namespace las
{

class Equ : public SegmentContent
{
public:
  Equ( GE5::Position const& position, std::wstring name, Value && value );
  ~Equ();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

  virtual wchar_t const* getName();
  std::wstring const& name() const;
  Value const& getValue() const;

private:
  std::wstring mName;
  Value mValue;
};

} //namespace las

#endif //LAS_EQU_HPP
