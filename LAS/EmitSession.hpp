#ifndef LAS_EMITSESSION_HPP
#define LAS_EMITSESSION_HPP

#include "CPUMode.hpp"
#include "CalcContext.hpp"

namespace GE5
{
struct Position;
}

namespace las
{

class SegmentProxy;
class CalcContext;
class Sink;
class Usage;

class EmitSession : public CalcContext
{
public:
  EmitSession( SegmentProxy & parentProxy, int baseAddress, std::shared_ptr<Sink> sink );
  virtual ~EmitSession();

  virtual size_t currentSegmentOffset() const;
  virtual size_t anonymousLabelOffset( GE5::Position const& pos, bool forward, int count );
  virtual SegmentProxy * currentCodeSegment();
  virtual SegmentProxy * currentDataSegment();
  virtual value::Host functionValue( GE5::Position const& pos, std::vector<std::wstring> const& name,
    std::optional<value::Host> arg, value::UsageReq req );
  virtual value::Host calcIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec , value::UsageReq req );
  virtual void appendToEmitList( SegmentProxy * segmentProxyToAppend );

  // returns current size of memory operations
  int memSize() const;

  //returns current size of index operations
  int idxSize() const;

  bool isNative() const;

  void emit( GE5::Position const& pos, unsigned char op );
  void emit( GE5::Position const& pos, unsigned char op, unsigned char data );
  void emit( GE5::Position const& pos, unsigned char op, unsigned short data );
  void emit( GE5::Position const& pos, unsigned char op, unsigned int data );

  void cpuNative( GE5::Position const& pos, int memSize = 0, int idxSize = 0 );
  void cpuEmulation();

  size_t getSize() const;

  void commitUsage( GE5::Position const& position, Usage const& usage );

  void emitReferencedSegments();

private:
   SegmentProxy & mParentProxy;
   int mBaseAddress;
   size_t mCurrentOffset;
   std::optional< CPUMode::Mode > mCPUMode;
   std::optional< int > mMemSize;
   std::optional< int > mIdxSize;
   std::shared_ptr<Sink> mSink;
   std::set<SegmentProxy *> mSegmentsToEmit;
};

} //namespace las

#endif //LAS_EMITSESSION_HPP
