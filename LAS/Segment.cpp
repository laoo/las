#include "stdafx.h"
#include "Segment.hpp"
#include "Command.hpp"
#include "Label.hpp"
#include "Exception.hpp"
#include "Usage.hpp"
#include "UsageShift.hpp"
#include "Equ.hpp"

namespace las
{

Segment::Segment( GE5::Position const& position, std::wstring aName ) : 
  PositionHolder( position ), mName( std::move( aName ) )
{
}

std::wstring const& Segment::getName() const
{
  return mName;
}

void Segment::addCommand( std::shared_ptr<Command> command )
{
  command->setLabel( std::move( mWaitingLabels ) );
  mContent.push_back( std::move( command ) );
}

void Segment::addEqu( std::shared_ptr<Equ> equ )
{
  SymbolValue sv( equ->position() );
  sv.type = ST_EQU;
  sv.equ = equ;

  auto p = mSymbols.insert( std::make_pair( equ->name(), sv ) );
  if ( !p.second )
    throw SemanticException( equ->position(), std::wstring( L"Segment " ) + mName + L" already has a symbol " + equ->name() );
  addContent( std::move( equ ) );
}

void Segment::addLabel( std::shared_ptr<Label> label )
{
  if ( !label->name().empty() )
  {
    SymbolValue sv( label->position() );
    sv.type = ST_LABEL;
    auto p = mSymbols.insert( std::make_pair( label->name(), sv ) );
    if ( !p.second )
      throw SemanticException( label->position(), std::wstring( L"Segment " ) + mName + L" already has a symbol " + label->name() );
  }
  mWaitingLabels.push_back( std::move( label ) );
}

std::optional< Segment::SymbolValue > Segment::getSymbolValue( std::wstring const& name ) const
{
  auto it = mSymbols.find( name );
  if ( it != mSymbols.cend() )
    return it->second;
  else
    return {};
}

std::vector< std::shared_ptr<Label> > const& Segment::waitingLabels() const
{
  return mWaitingLabels;
}

void Segment::addContent( std::shared_ptr<SegmentContent> content )
{
  mContent.push_back( std::move( content ) );
}

void Segment::gather( GatherSession & session ) const
{
  for ( auto it = mContent.cbegin(); it != mContent.cend(); ++it )
  {
    (*it)->gather( session );
  }
}

void Segment::emit( EmitSession & session ) const
{
  for ( auto it = mContent.cbegin(); it != mContent.cend(); ++it )
  {
    (*it)->emit( session );
  }
}

} //namespace las

