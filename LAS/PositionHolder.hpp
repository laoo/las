#ifndef LAS_POSITIONHOLDER_HPP
#define LAS_POSITIONHOLDER_HPP

#include "ge5/include/Position.hpp"

namespace las
{
class PositionHolder
{
public:
  PositionHolder( GE5::Position const& position );
  ~PositionHolder();

  GE5::Position const& position() const;

protected:
  GE5::Position  mPosition;
};

} //namespace las

#endif //LAS_POSITIONHOLDER_HPP
