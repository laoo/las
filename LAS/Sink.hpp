#ifndef LAS_SINK_HPP
#define LAS_SINK_HPP

namespace GE5
{
struct Position;
}

namespace las
{

class Sink
{
public:
  Sink();
  virtual ~Sink();

  virtual void emit( GE5::Position const& pos, int address, unsigned char op ) = 0;
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned char data ) = 0;
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned short data ) = 0;
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned int data ) = 0;

};

} //namespace las

#endif //LAS_SINK_HPP
