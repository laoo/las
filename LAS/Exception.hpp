#ifndef LAS_EXCEPTION_HPP
#define LAS_EXCEPTION_HPP

#include <stdexcept>
#include "ge5/include/Position.hpp"

namespace las
{

class Exception : public std::runtime_error
{
public:
	explicit Exception( std::wstring const& aWhat ) :
    runtime_error( boost::locale::conv::from_utf( aWhat, "UTF-8" ) ),
    mwWhat( aWhat )
	{
	}
	virtual ~Exception() {}

  std::wstring const& wWhat() const
  {
    return mwWhat;
  }

private:
  std::wstring mwWhat;

};

class SemanticException : public Exception
{
public:
	explicit SemanticException( GE5::Position const& position, std::wstring const& aWhat ) : Exception( aWhat ), mPosition( position )
	{
	}
	virtual ~SemanticException() {}

  GE5::Position const& position() const
  {
    return mPosition;
  }

protected:
  GE5::Position mPosition;

};

class NYIException : public SemanticException
{
public:
	explicit NYIException( GE5::Position const& position, std::wstring const& aWhat ) :
    SemanticException( position, std::wstring( L"Not Yet Immplemented: " ) + aWhat )
	{
	}
	virtual ~NYIException() {}

};


class StopParsingException : public Exception
{
public:
	explicit StopParsingException( GE5::Position const& position ) : Exception( std::wstring() ),  mPosition( position )
	{
	}
	virtual ~StopParsingException() {}

  GE5::Position const& position() const
  {
    return mPosition;
  }

protected:
  GE5::Position mPosition;
};

} //namespace las

#endif //LAS_EXCEPTION_HPP
