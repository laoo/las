#include "stdafx.h"
#include "Module.hpp"
#include "Namespace.hpp"
#include "Exception.hpp"
#include "Out.hpp"

namespace las
{

Module::Module( std::filesystem::path const& path ) :
  mRootNamespace( std::make_shared< Namespace >( L"" ) ),
  mNamespaceStack()
{
  mNamespaceStack.push_back( mRootNamespace );
}

Module::~Module()
{
}

std::shared_ptr<Namespace> Module::getRootNamespace() const
{
  return mRootNamespace;
}

void Module::eachOutput( std::function<void(std::shared_ptr<Out>)> fun )
{
  std::for_each( mOutputs.cbegin(), mOutputs.cend(), fun );
}


void Module::pushNamespace( GE5::Position const& position, std::wstring name )
{
  mNamespaceStack.emplace_back( mNamespaceStack.back()->addNamespace( position, name ) );
}

void Module::addSegment( GE5::Position const& position, std::wstring name )
{
  mNamespaceStack.back()->openSegment( position, std::move( name ) );
}

void Module::closeObject( GE5::Position const& position, std::wstring const& name )
{
  if ( !mNamespaceStack.back()->closeSegment( position, name ) )
  {
    if ( mNamespaceStack.size() < 2 )
    {
      if ( name.empty() )
        throw StopParsingException( position );
      else
        throw SemanticException( position, L"Can't close global namespane as " + name );
    }

    if ( !name.empty() && mNamespaceStack.back()->getName() != name )
    {
      throw SemanticException( position, L"Current namespace name is " + mNamespaceStack.back()->getName() + L" but closing it as " + name );
    }
    mNamespaceStack.pop_back();
  }
}

void Module::addOut( std::shared_ptr<Out> out )
{
  if ( mNamespaceStack.size() > 1 || mNamespaceStack.back()->hasOpenSegment() )
    throw SemanticException( out->position() , L"Output can be defined only in global namespace" );

  mOutputs.push_back( std::move( out ) );
}

void Module::addCommand( std::shared_ptr<Command> cmd )
{
  mNamespaceStack.back()->addCommand( std::move( cmd ) );
}

void Module::addLabel( std::shared_ptr<Label> label )
{
  mNamespaceStack.back()->addLabel( std::move( label ) );
}

void Module::addUsage( std::shared_ptr<Usage> usage )
{
  mNamespaceStack.back()->addUsage( std::move( usage ) );
}

void Module::shiftUsage( std::shared_ptr<UsageShift> usage )
{
  mNamespaceStack.back()->shiftUsage( std::move( usage ) );
}

void Module::addAttribute( std::shared_ptr<Attribute> attr )
{
}

void Module::addEqu( std::shared_ptr<Equ> equ )
{
  mNamespaceStack.back()->addEqu( std::move( equ ) );
}

void Module::addCPUMode( std::shared_ptr<CPUMode> cpuMode )
{
  mNamespaceStack.back()->addCPUMode( std::move( cpuMode ) );
}

void Module::addStackDeclaration( std::shared_ptr<StackDeclaration> stackDeclaration )
{
}

} //namespace las

