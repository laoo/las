#include "stdafx.h"
#include "Addressing.hpp"
#include "cmd/CmdMove.hpp"
#include "cmd/CmdImplied.hpp"
#include "cmd/CmdIndexed.hpp"
#include "cmd/CmdAbsolute.hpp"
#include "cmd/CmdIndirect.hpp"
#include "cmd/CmdImmediate.hpp"
#include "cmd/CmdIndexedIndirect.hpp"
#include "cmd/CmdDirectIndirectLong.hpp"
#include "cmd/CmdDirectIndirectIndexed.hpp"
#include "cmd/CmdDirectIndirectLongIndexed.hpp"
#include "cmd/CmdStackRelativeIndirectIndexed.hpp"

namespace las
{


Addressing::~Addressing()
{
}

AddressingImplied::AddressingImplied()
{
}

AddressingImplied::~AddressingImplied()
{
}

std::shared_ptr<Command> AddressingImplied::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdImplied>( position, mnemonic );
}

AddressingImmediate::AddressingImmediate( Value v )  : value( std::move( v ) )
{
}

AddressingImmediate::~AddressingImmediate()
{
}

std::shared_ptr<Command> AddressingImmediate::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdImmediate>( position, mnemonic, std::move( value ) );
}

AddressingAbsolute::AddressingAbsolute( Value v ) : value( std::move( v ) )
{
}

AddressingAbsolute::~AddressingAbsolute()
{
}

std::shared_ptr<Command> AddressingAbsolute::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdAbsolute>( position, mnemonic, std::move( value ) );
}

AddressingIndirect::AddressingIndirect( Value v ) : value( std::move( v ) )
{
}

AddressingIndirect::~AddressingIndirect()
{
}

std::shared_ptr<Command> AddressingIndirect::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdIndirect>( position, mnemonic, std::move( value ) );
}

AddressingDirectIndirectIndexed::AddressingDirectIndirectIndexed( Value v ) : value( std::move( v ) )
{
}

AddressingDirectIndirectIndexed::~AddressingDirectIndirectIndexed()
{
}

std::shared_ptr<Command> AddressingDirectIndirectIndexed::applyAddressing( GE5::Position const& position,  cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdDirectIndirectIndexed>( position, mnemonic, std::move( value ) );
}

AddressingDirectIndirectLong::AddressingDirectIndirectLong( Value v ) : value( std::move( v ) )
{
}

AddressingDirectIndirectLong::~AddressingDirectIndirectLong()
{
}

std::shared_ptr<Command> AddressingDirectIndirectLong::applyAddressing( GE5::Position const& position,  cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdDirectIndirectLong>( position, mnemonic, std::move( value ) );
}

AddressingDirectIndirectLongIndexed::AddressingDirectIndirectLongIndexed( Value v ) : value( std::move( v ) )
{
}

AddressingDirectIndirectLongIndexed::~AddressingDirectIndirectLongIndexed()
{
}

std::shared_ptr<Command> AddressingDirectIndirectLongIndexed::applyAddressing( GE5::Position const& position,  cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdDirectIndirectLongIndexed>( position, mnemonic, std::move( value ) );
}

AddressingStackRelativeIndirectIndexed::AddressingStackRelativeIndirectIndexed( Value v ) : value( std::move( v ) )
{
}

AddressingStackRelativeIndirectIndexed::~AddressingStackRelativeIndirectIndexed()
{
}

std::shared_ptr<Command> AddressingStackRelativeIndirectIndexed::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdStackRelativeIndirectIndexed>( position, mnemonic, std::move( value ) );
}

AddressingIndexed::AddressingIndexed( Value v, Index i ) : value ( std::move( v ) ), index( i )
{
}

AddressingIndexed::~AddressingIndexed()
{
}

std::shared_ptr<Command> AddressingIndexed::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  cmd::CmdIndexed::Register r = index == X ? cmd::CmdIndexed::X : ( index == Y ? cmd::CmdIndexed::Y : cmd::CmdIndexed::S );
  return std::make_shared<cmd::CmdIndexed>( position, mnemonic, std::move( value ), r );
}

AddressingIndexedIndirect::AddressingIndexedIndirect( Value v ) : value( std::move( v ) )
{
}

AddressingIndexedIndirect::~AddressingIndexedIndirect()
{
}

std::shared_ptr<Command> AddressingIndexedIndirect::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdIndexedIndirect>( position, mnemonic, std::move( value ) );
}

AddressingMove::AddressingMove( Value src, Value dst ) : source( std::move( src ) ), dest( std::move( dst ) )
{
}

AddressingMove::~AddressingMove()
{
}

std::shared_ptr<Command> AddressingMove::applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic )
{
  return std::make_shared<cmd::CmdMove>( position, mnemonic, std::move( source ), std::move( dest ) );
}

} //namespace las

