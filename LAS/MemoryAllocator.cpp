#include "stdafx.h"
#include "MemoryAllocator.hpp"
#include "EmptyCalcContext.hpp"
#include "SegmentProxy.hpp"
#include "Segment.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{

MemoryAllocator::MemoryAllocator()
{
}

MemoryAllocator::~MemoryAllocator()
{
}

void MemoryAllocator::registerRange( NamespaceProxy const* ptr, int address,  std::optional<size_t> size )
{
  if ( size )
  {
    auto p = mRanges.insert( std::make_pair( ptr, Range{ address, address + *size } ) );
    assert( p.second );
  }
  else
  {
    // by default the allocation block spans to the end of the bank if not narrowed
    auto p = mRanges.insert( std::make_pair( ptr, Range{ address, (size_t)( address | 0xffff ) + 1 } ) );
    assert( p.second );
  }
}

int MemoryAllocator::allocate( SegmentProxy const* seg )
{
  preallocate();
  auto it = mPreallocated.find( seg );
  assert( it != mPreallocated.cend() );
  return it->second;
}

std::optional<int> MemoryAllocator::allocate( GE5::Position const& position, NamespaceProxy const* p, size_t size )
{
  preallocate();
  auto it = mRanges.find( p );
  if ( it == mRanges.end() )
    return {};
  else
  {
    int result = it->second.address;
    if ( result + size > 0xffff )
    {
      throw SemanticException( position, std::wstring( L"Allocating memory for segment in namespace specified range exceeds bank boundary" ) );
    }

    it->second.address += (int)size;
    return result;
  }
}

void MemoryAllocator::preallocate( SegmentProxy * seg, Value org, size_t size )
{
  mToPreallocate.push_back( Prealloc( seg, std::move( org ), size ) );
}

void MemoryAllocator::preallocate()
{
  for ( auto it = mToPreallocate.cbegin(); it != mToPreallocate.cend(); ++it )
  {
    assert( mPreallocated.find( it->seg ) == mPreallocated.cend() );

    int org = it->org.calcValue( EmptyCalcContext() ).getValue();
    Bank & bank = getBank( org );
    int bankAddr = org & 0xffff;
    size_t blockSize = it->size;
    if ( bankAddr + blockSize > 0xffff )
      throw SemanticException( it->seg->segment()->position(), std::wstring( L"Segment " ) + it->seg->segment()->getName() + L" exceeds bank boundary" );
    //find if the area is empty
    auto found = std::find_if( bank.memory.cbegin() + bankAddr, bank.memory.cbegin() + bankAddr + blockSize, []( SegmentProxy const* s )
    {
      return s != nullptr;
    } );
    size_t foff = ( size_t )std::distance( bank.memory.cbegin(), found );
    if ( foff - bankAddr < blockSize )
    {
      std::wstring other;
      if ( it->seg->segment()->position().sourceProxy != (*found)->segment()->position().sourceProxy )  //different source file
      {
        other = (*found)->segment()->position().wstring();
      }
      else
      {
        other = (*found)->segment()->position().range();
      }

      throw SemanticException( it->seg->segment()->position(),
        ( boost::wformat( L"While allocating memory for segment %s. Memory beginning from address %x already allocated for segment %s defined at %s" )
          % it->seg->segment()->getName()
          % ( found - bank.memory.cbegin() )
          % (*found)->segment()->getName()
          % other
        ).str() );
    }
    std::fill_n( bank.memory.begin() + bankAddr, blockSize, it->seg );
    mPreallocated.insert( std::make_pair( it->seg, org ) );
  }

  mToPreallocate.clear();
}


MemoryAllocator::Bank & MemoryAllocator::getBank( int address )
{
  assert( address >= 0 && address <= 0xffffff );
  return mBanks[ address >> 16 ];
}

MemoryAllocator::Bank::Bank() : memory( 65536, nullptr )
{
}

MemoryAllocator::Prealloc::Prealloc( SegmentProxy * aSeg, Value && aOrg, size_t aSize ) :
  seg( aSeg ), org( std::move( aOrg ) ), size( aSize )
{
}

} //namespace las

