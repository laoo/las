#include "stdafx.h"
#include "Assembler.hpp"
#include "Namespace.hpp"
#include "Module.hpp"
#include "Out.hpp"
#include "SinkLog.hpp"
#include "Exception.hpp"
#include "NamespaceProxy.hpp"
#include "MemoryAllocator.hpp"

namespace las
{

Assembler::Assembler() :
  mModules( 0 ), mRootNamespace( std::make_shared<Namespace>( L"" ) )
{
}

Assembler::~Assembler()
{
}

void Assembler::addModule( std::shared_ptr<Module> module )
{
  mRootNamespace->merge( module->getRootNamespace() );
  module->eachOutput( [this]( std::shared_ptr<Out> out )
  {
    mOutputs.push_back( out );
  } );
  mModules += 1;
}

void Assembler::process()
{
  if ( mModules == 0 )  //must have been error
    return;             //return silently

  bool failed = true;
  if ( mRootNamespace->hasMainSegment() )
  {
    assemble( mRootNamespace, createDefaultSink() );
    failed = false;
  }

  for ( auto it = mOutputs.begin(); it != mOutputs.end(); ++it )
  {
    assemble( mRootNamespace, createOutSink( &**it ), *it );
    failed = false;
  }

  if ( failed )
    throw Exception( L"No main segment found nor any output specified" );
}


void Assembler::assemble( std::shared_ptr<Namespace> pRoot, std::shared_ptr<Sink> pSink )
{
  NamespaceProxy root( pRoot, std::move( pSink ), std::make_shared<MemoryAllocator>() );
  root.emitSegment( L"main" );
}

void Assembler::assemble( std::shared_ptr<Namespace> pRoot, std::shared_ptr<Sink> pSink, std::shared_ptr<Out> pOut )
{
  assert( false );  //NYI
}

std::shared_ptr<Sink> Assembler::createDefaultSink() const
{
  return std::make_shared<SinkLog>();
}

std::shared_ptr<Sink> Assembler::createOutSink( Out const* pOut ) const
{
  assert( false );
  return std::shared_ptr<Sink>();
}

} //namespace las

