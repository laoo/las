#include "stdafx.h"
#include "Command.hpp"
#include "GatherSession.hpp"
#include "cmd/Org.hpp"
#include "cmd/DataDeclaration.hpp"
#include "cmd/SpaceDeclaration.hpp"
namespace las
{

Command::Command( GE5::Position const& position ) : SegmentContent( position )
{
}

Command::~Command()
{
}

std::shared_ptr<Command> Command::createOrg( GE5::Position const& position, Value && v )
{
  return std::make_shared<cmd::Org>( position, std::move( v ) );
}

std::shared_ptr<Command> Command::createDataDeclaration( GE5::Position const& position,
    DataDeclarationType type, std::vector< Value > && data )
{
  return std::make_shared<cmd::DataDeclaration>( position, type, std::move( data ) );
}


std::shared_ptr<Command> Command::createSpaceDeclaration( GE5::Position const& position, Value && size )
{
  return std::make_shared<cmd::SpaceDeclaration>( position, std::move( size ) );
}

void Command::setLabel( std::vector< std::shared_ptr<Label> > labels )
{
  mLabels = std::move( labels );
}

wchar_t const* Command::getName()
{
  return L"Command";
}

void Command::gather( GatherSession & session ) const
{
  for ( auto it = mLabels.cbegin(); it != mLabels.cend(); ++it )
  {
    session.commitLabel( *it );
  }
}

} //namespace las

