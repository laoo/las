#ifndef LAS_OUT_HPP
#define LAS_OUT_HPP

#include "PositionHolder.hpp"

namespace las
{

class Out : public PositionHolder
{
public:
  Out( GE5::Position const& position, std::wstring name, std::wstring attr );
  ~Out();

  std::wstring getName() const;

private:
  std::wstring mName;
  std::wstring mAttr;
};

} //namespace las

#endif //LAS_OUT_HPP
