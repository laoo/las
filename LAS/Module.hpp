#ifndef LAS_MODULE_HPP
#define LAS_MODULE_HPP

#include "Logger.hpp"
#include "ge5/include/Position.hpp"

namespace las
{

class Namespace;
class Command;
class Label;
class Usage;
class UsageShift;
class Attribute;
class Out;
class Equ;
class CPUMode;
class StackDeclaration;

class Module
{
public:
  Module( std::filesystem::path const& path );
  ~Module();

  void pushNamespace( GE5::Position const& position, std::wstring name ); 
  void addSegment( GE5::Position const& position, std::wstring name ); 
  void closeObject( GE5::Position const& position, std::wstring const& name ); 

  void addOut( std::shared_ptr<Out> attr );

  void addCommand( std::shared_ptr<Command> cmd );
  void addLabel( std::shared_ptr<Label> label );
  void addUsage( std::shared_ptr<Usage> usage );
  void shiftUsage( std::shared_ptr<UsageShift> usage );
  void addAttribute( std::shared_ptr<Attribute> attr );
  void addEqu( std::shared_ptr<Equ> equ );
  void addCPUMode( std::shared_ptr<CPUMode> cpuMode );
  void addStackDeclaration( std::shared_ptr<StackDeclaration> stackDeclaration );

  std::shared_ptr<Namespace> getRootNamespace() const;
  void eachOutput( std::function<void(std::shared_ptr<Out>)> fun );


private:
  std::shared_ptr<Namespace> mRootNamespace;
  std::vector< std::shared_ptr<Namespace> > mNamespaceStack;
  std::vector< std::shared_ptr<Out> > mOutputs;
};

} //namespace las

#endif //LAS_MODULE_HPP

