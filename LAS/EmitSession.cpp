#include "stdafx.h"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "Sink.hpp"
#include "Equ.hpp"
#include "Usage.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"
#include "NameUtils.hpp"

namespace las
{

EmitSession::EmitSession( SegmentProxy & parentProxy, int baseAddress, std::shared_ptr<Sink> sink ) :
  mParentProxy( parentProxy ), mBaseAddress( baseAddress ), mCurrentOffset( 0 ),
  mCPUMode(), mMemSize(), mIdxSize(),
  mSink( std::move( sink ) )
{
}

EmitSession::~EmitSession()
{
}

size_t EmitSession::currentSegmentOffset() const
{
  return mCurrentOffset;
}


SegmentProxy * EmitSession::currentCodeSegment()
{
  return &mParentProxy;
}

SegmentProxy * EmitSession::currentDataSegment() 
{
  return &mParentProxy;
}

size_t EmitSession::anonymousLabelOffset( GE5::Position const& pos, bool forward, int count )
{
  return mParentProxy.anonymousLabelOffset( pos, mCurrentOffset, forward, count );
}

value::Host EmitSession::functionValue( GE5::Position const& pos, std::vector<std::wstring> const& name,
  std::optional<value::Host> arg, value::UsageReq req )
{
  //nullary
  if ( !arg )
  {
    //global
    if ( name.size() == 1 )
    {
      if ( name[0] == L"origin" )
      {
        return value::Host( pos, 0, &mParentProxy );
      }
      if ( name[0] == L"size" )
      {
        return value::Host( pos, (int)mParentProxy.getSize() );
      }
    }
  }

  std::wstring ex( L"Unknow function " );
  ex += name[0];
  for ( size_t i = 1; i < name.size(); ++i )
  {
    ex += L".";
    ex += name[i];
  }

  throw SemanticException( pos, ex );
}

value::Host EmitSession::calcIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
  value::Decoration dec , value::UsageReq req )
{
  std::optional<SegmentProxy::SymbolWithUsage> sym = mParentProxy.findMachingSymbol( pos, name, dec, req );
  if ( sym )
  {
    if ( sym->symbolValue.type == Segment::ST_EQU )
    {
      value::Host h = sym->symbolValue.equ->getValue().calcValue( *this, req );
      h.decorate( dec );
      return h;
    }
    else
    {
      value::Host h( pos, (int)sym->usage->segment->getLabelValue( name[ name.size() - 1 ] ), sym->usage->segment.get() );
      h.decorate( sym->usage->usage.compatibleDecoration() );
      return h;
    }
  }
  else
  {
    throw SemanticException( pos, std::wstring( L"Symbol " ) + wstring( name ) + L" not found" );
  }
}

void EmitSession::appendToEmitList( SegmentProxy * segmentProxyToAppend )
{
  assert( segmentProxyToAppend );
  mSegmentsToEmit.insert( segmentProxyToAppend );
}

void EmitSession::emitReferencedSegments()
{
  for ( auto it = mSegmentsToEmit.begin(); it != mSegmentsToEmit.end(); ++it )
    (*it)->emit();
}

int EmitSession::memSize() const
{
  if ( mMemSize )
    return *mMemSize;
  else
  {
    auto parentMemSize = mParentProxy.memSize();
    assert( parentMemSize );
    return *parentMemSize;
  }
}

int EmitSession::idxSize() const
{
  if ( mIdxSize )
    return *mIdxSize;
  else
  {
    auto parentIdxSize = mParentProxy.idxSize();
    assert( parentIdxSize );
    return *parentIdxSize;
  }
}

void EmitSession::cpuNative( GE5::Position const& pos, int mSize, int xSize )
{
  if ( mCPUMode )
  {
    if ( *mCPUMode == CPUMode::NATIVE )
    {
      if ( mSize )
        mMemSize = mSize;
      if ( xSize )
        mIdxSize = xSize;
    }
    else
    {
      if ( mSize )
        mMemSize = mSize;
      else
        mMemSize = 8;
      if ( xSize )
        mIdxSize = xSize;
      else
        mMemSize = 8;
    }
  }
  else
  {
    if ( mSize )
      mMemSize = mSize;
    else
      mMemSize = memSize();
    if ( xSize )
      mIdxSize = xSize;
    else
      mIdxSize = idxSize();
  }

  mCPUMode = CPUMode::NATIVE;
}

void EmitSession::cpuEmulation()
{
  mCPUMode = CPUMode::EMULATION;
  mMemSize = 8;
  mIdxSize = 8;
}

void EmitSession::emit( GE5::Position const& pos, unsigned char op )
{
  mSink->emit( pos, mBaseAddress + (int)mCurrentOffset, op );
  mCurrentOffset += 1;
}

void EmitSession::emit( GE5::Position const& pos, unsigned char op, unsigned char data )
{
  mSink->emit( pos, mBaseAddress + (int)mCurrentOffset, op, data );
  mCurrentOffset += 2;
}

void EmitSession::emit( GE5::Position const& pos, unsigned char op, unsigned short data )
{
  mSink->emit( pos, mBaseAddress + (int)mCurrentOffset, op, data );
  mCurrentOffset += 3;
}

void EmitSession::emit( GE5::Position const& pos, unsigned char op, unsigned int data )
{
  mSink->emit( pos, mBaseAddress + (int)mCurrentOffset, op, data );
  mCurrentOffset += 4;
}

size_t EmitSession::getSize() const
{
  return mCurrentOffset;
}

void EmitSession::commitUsage( GE5::Position const& position, Usage const& usage )
{
  mParentProxy.commitUsage( position, usage );
}

} //namespace las

