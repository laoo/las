#include "stdafx.h"
#include "Usage.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "Exception.hpp"


namespace las
{

Usage::Usage( GE5::Position const& position, Addressing adr, std::vector< std::wstring > name, Placement && placement, std::wstring as ) :
  SegmentContent( position ),
  mName( std::move( name ) ),
  mAdr( adr ),
  mPlacement( std::move( placement ) ),
  mAs( std::move( as ) )
{
}

Usage::~Usage()
{
}

void Usage::gather( GatherSession & session ) const
{
  session.commitUsage( position(), *this );
}

void Usage::emit( EmitSession & session ) const
{
  session.commitUsage( position(), *this );
}


std::wstring const& Usage::getAlias() const
{
  return mAs;
}

std::vector< std::wstring > const& Usage::getName() const
{
  return mName;
}

bool Usage::namespacewiseEqual( Usage const& other )
{
  return !mAs.empty() && mAs == other.mAs || mName == other.mName && mAdr == other.mAdr;
}


Usage::Addressing Usage::getAddressing() const
{
  return mAdr;
}

bool Usage::compatibleAddressing( value::Decoration dec ) const
{
  switch ( mAdr )
  {
  case A_IMPLICIT:
    return dec == value::D_NONE || dec == value::D_ABSOLUTED || dec == value::D_LONGENED;
  case A_DIRECT:
    return dec == value::D_NONE || dec == value::D_DIRECTED;
  case A_ABSOLUTE:
    return dec == value::D_NONE || dec == value::D_ABSOLUTED;
  case A_LONG:
    return dec == value::D_NONE || dec == value::D_LONGENED;
  default:
    assert( false );
    return false;
  }
}

value::Decoration Usage::compatibleDecoration() const
{
  switch ( mAdr )
  {
  case A_IMPLICIT:
    return value::D_NONE;
  case A_DIRECT:
    return value::D_DIRECTED;
  case A_ABSOLUTE:
    return value::D_ABSOLUTED;
  case A_LONG:
    return value::D_LONGENED;
  default:
    assert( false );
    return value::D_NONE;
  }
}

} //namespace las

