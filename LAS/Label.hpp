#ifndef LAS_LABEL_HPP
#define LAS_LABEL_HPP

#include "PositionHolder.hpp"

namespace GE5
{
struct Position;
}

namespace las
{

class Label : public PositionHolder
{
public:
  Label( GE5::Position const& position );
  Label( GE5::Position const& position, std::wstring name );
  ~Label();

  std::wstring const& name() const;

private:
  std::wstring mName;
};

} //namespace las

#endif //LAS_LABEL_HPP
