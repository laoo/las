#include "stdafx.h"
#include "Label.hpp"

namespace las
{

Label::Label( GE5::Position const& position ) : PositionHolder( position )
{
}

Label::Label( GE5::Position const& position, std::wstring name ) : PositionHolder( position ), mName( std::move( name ) )
{
}

Label::~Label()
{
}

std::wstring const& Label::name() const
{
  return mName;
}


} //namespace las

