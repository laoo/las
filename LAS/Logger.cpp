#include "stdafx.h"
#include "Logger.hpp"

namespace las
{

void Logger::log( Logger::Level level, std::wstring const& message )
{
  switch ( level )
  {
  case LEVEL_TRACE:
    break;
  case LEVEL_NOTICE:
    std::wcerr << L"notice: ";
    break;
  case LEVEL_WARNING:
    std::wcerr << L"warning: ";
    break;
  case LEVEL_ERROR:
    std::wcerr << L"error: ";
    break;
  }
  std::wcerr << message << std::endl;
}

void Logger::log( Logger::Level level, GE5::Position const& pos, std::wstring const& message )
{
  std::wcerr << pos.wstring();
  
  switch ( level )
  {
  case LEVEL_TRACE:
    std::wcerr << L": ";
    break;
  case LEVEL_NOTICE:
    std::wcerr << L": notice: ";
    break;
  case LEVEL_WARNING:
    std::wcerr << L": warning: ";
    break;
  case LEVEL_ERROR:
    std::wcerr << L": error: ";
    break;
  }
  std::wcerr << message << std::endl;
}

void Logger::trace( std::wstring const& message )
{
  log( LEVEL_TRACE, message );
}

void Logger::notice( std::wstring const& message )
{
  log( LEVEL_NOTICE, message );
}

void Logger::notice( GE5::Position const& pos, std::wstring const& message )
{
  log( LEVEL_NOTICE, pos, message );
}

void Logger::warn( std::wstring const& message )
{
  log( LEVEL_WARNING, message );
}

void Logger::warn( GE5::Position const& pos, std::wstring const& message )
{
  log( LEVEL_WARNING, pos, message );
}

void Logger::err( GE5::Position const& pos, std::wstring const& message )
{
  log( LEVEL_ERROR, pos, message );
}

void Logger::err( std::wstring const& message )
{
  log( LEVEL_ERROR, message );
}

} //namespace las

