#ifndef LAS_VALUE_ID_HPP
#define LAS_VALUE_ID_HPP

#include "Holder.hpp"

namespace las
{
namespace value
{

class Id : public Holder
{
public:
  Id( GE5::Position const& pos, std::vector<std::wstring> name );
  virtual ~Id();

  virtual boost::tribool address() const;

  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  std::vector<std::wstring> mName;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_ID_HPP
