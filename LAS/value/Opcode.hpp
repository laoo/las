#ifndef LAS_VALUE_OPCODE_HPP
#define LAS_VALUE_OPCODE_HPP

#include "Holder.hpp"

namespace las
{
class Command;

namespace value
{

class Opcode : public Holder
{
public:
  Opcode( GE5::Position const& pos,  std::shared_ptr<Command> cmd );
  virtual ~Opcode();

  virtual boost::tribool address() const;
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  std::shared_ptr<Command> mCmd;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_OPCODE_HPP
