#ifndef LAS_VALUE_OPERATORS_HPP
#define LAS_VALUE_OPERATORS_HPP

namespace las
{
namespace value
{

enum Operators
{
  OP_ARITHMETIC_NEGATE,
  OP_LOGIC_NEGATE,
  OP_BINARY_NEGATE,
  OP_MULTIPLY,
  OP_DIVIDE,
  OP_MODULO,
  OP_PLUS,
  OP_MINUS,
  OP_SHL,
  OP_SHR,
  OP_L,
  OP_G,
  OP_LE,
  OP_GE,
  OP_EQ,
  OP_NE,
  OP_BINARY_AND,
  OP_BINARY_XOR,
  OP_BINARY_OR,
  OP_LOGICAL_AND,
  OP_LOGICAL_OR,
  OP_DIRECT,
  OP_ABSOLUTE,
  OP_LONG
};

enum CharType
{
  CT_ATASCII,
  CT_INTERNAL
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_OPERATORS_HPP
