#include "stdafx.h"
#include "Id.hpp"
#include "CalcContext.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

Id::Id( GE5::Position const& pos, std::vector<std::wstring> name ) : Holder( pos ), mName( std::move( name ) )
{
}

Id::~Id()
{
}

boost::tribool Id::address() const
{
  return boost::indeterminate;
}

Host Id::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  return context.calcIdentifier( position(), mName, dec, req );
}

Measure Id::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  return context.measureIdentifier( position(), mName, dec, req );
}

std::optional<int> Id::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las

