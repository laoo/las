#include "stdafx.h"
#include "Fun.hpp"
#include "Exception.hpp"
#include "CalcContext.hpp"
#include "MeasureContext.hpp"

namespace las
{
namespace value
{

Fun::Fun( GE5::Position const& pos, std::wstring name, std::shared_ptr<Holder> arg ) : Holder( pos ),
  mName(), mArg( std::move( arg ) )
{
  mName.push_back( std::move( name ) );
}

void Fun::prependName( std::vector<std::wstring> name )
{
  std::swap( name, mName );
  mName.reserve( mName.size() + name.size() ); 
  mName.insert( mName.end(), name.begin(), name.end() );
}

Fun::~Fun()
{
}

boost::tribool Fun::address() const
{
  return boost::indeterminate;
}

Host Fun::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  std::optional<Host> arg;
  if ( mArg )
    arg = mArg->calcValue( context, dec, req );

  return context.functionValue( position(), mName, arg, req );
}

Measure Fun::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  throw NYIException( position(), L"measureValue" );
}

std::optional<int> Fun::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}



} //namespace value
} //namespace las

