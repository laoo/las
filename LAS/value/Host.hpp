#ifndef LAS_VALUE_HOST_HPP
#define LAS_VALUE_HOST_HPP

#include "PositionHolder.hpp"
#include "Decoration.hpp"

namespace las
{
class SegmentProxy;
namespace value
{

class Host : public las::PositionHolder
{
public:
  enum Kind
  {
    HK_VALUE,
    HK_ADDRESS
  };

  Host( GE5::Position const& pos, int value );
  Host( GE5::Position const& pos, int value, SegmentProxy * seg );
  ~Host();

  int getValue() const;

  Kind getKind() const;

  SegmentProxy * getSegment();

  Decoration getDecoration() const;
  void decorate( Decoration dec );
  bool isAddress() const;

private:
  Kind mKind;
  Decoration mDec;
  SegmentProxy * mSegmentOfTheAddress;
  int mValue;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_HOST_HPP
