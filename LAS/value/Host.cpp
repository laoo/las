#include "stdafx.h"
#include "Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

Host::Host( GE5::Position const& pos, int value ) : PositionHolder( pos ),
  mKind( HK_VALUE ), mDec( D_NONE ), mSegmentOfTheAddress( nullptr ), mValue( value )
{
}

Host::Host( GE5::Position const& pos, int value, SegmentProxy * seg ) : PositionHolder( pos ),
  mKind( HK_ADDRESS ), mDec( D_NONE ), mSegmentOfTheAddress( seg ), mValue( value )
{
  assert( mSegmentOfTheAddress != nullptr );
}

Host::~Host()
{
}

Host::Kind Host::getKind() const
{
  return mKind;
}


int Host::getValue() const
{
  return mValue;
}

SegmentProxy * Host::getSegment()
{
  return mSegmentOfTheAddress;
}

Decoration Host::getDecoration() const
{
  return mDec;
}

void Host::decorate( Decoration dec )
{
  mDec = dec;
  if ( mDec == value::D_DIRECTED && ( mValue < -128 || mValue > 255 ) )
  {
    throw SemanticException( position(), ( boost::wformat( L"Value %d ($%x) too big" ) % mValue % mValue ).str() );
  }
}

bool Host::isAddress() const
{
  return mKind == HK_ADDRESS;
}


} //namespace value
} //namespace las

