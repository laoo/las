#ifndef LAS_VALUE_STRING_HPP
#define LAS_VALUE_STRING_HPP

#include "Holder.hpp"
#include "value/Operators.hpp"

namespace las
{
namespace value
{

class String : public Holder
{
public:

  String( GE5::Position const& pos, std::wstring s, value::CharType type );
  virtual ~String();

  virtual bool literal( int & out ) const;
  virtual boost::tribool address() const;

  virtual void updateLiteral( GE5::Position const& pos, int value );
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

protected:
  unsigned char atasciiToInternal( unsigned char input ) const;
  unsigned char internalToAtascii( unsigned char input ) const;
  unsigned char conv( unsigned char input, unsigned char tab[4] ) const;

private:

  std::wstring mString;
  value::CharType mType;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_STRING_HPP
