#include "stdafx.h"
#include "Holder.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

Holder::Holder( GE5::Position const& pos ) :
  las::PositionHolder( pos )
{
}

Holder::~Holder()
{
}

bool Holder::literal( int & out ) const
{
  return false;
}

void Holder::updateLiteral( GE5::Position const& pos, int value )
{
  assert( false );
}

void Holder::assertValue( UsageReq req ) const
{
  if ( req != U_VALUE )
    throw SemanticException( position(), L"Address required" );
}

} //namespace value
} //namespace las

