#ifndef LAS_VALUE_INT_HPP
#define LAS_VALUE_INT_HPP

#include "Holder.hpp"

namespace las
{
namespace value
{

class Int : public Holder
{
public:
  Int( GE5::Position const& pos, int v );
  virtual ~Int();

  virtual bool literal( int & out ) const;
  virtual boost::tribool address() const;

  virtual void updateLiteral( GE5::Position const& pos, int value );
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  int mValue;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_INT_HPP
