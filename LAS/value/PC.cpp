#include "stdafx.h"
#include "PC.hpp"
#include "CalcContext.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

PC::PC( GE5::Position const& pos ) : Holder( pos )
{
}

PC::~PC()
{
}

boost::tribool PC::address() const
{
  return true;
}

Host PC::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  return Host( position(), (int)context.currentSegmentOffset(), context.currentCodeSegment() );
}

Measure PC::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  return Measure( position(), context.currentCodeSegment() );
}

std::optional<int> PC::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las
