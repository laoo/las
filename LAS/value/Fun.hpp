#ifndef LAS_VALUE_FUN_HPP
#define LAS_VALUE_FUN_HPP

#include "Holder.hpp"

namespace las
{
namespace value
{

class Fun : public Holder
{
public:
  Fun( GE5::Position const& pos, std::wstring name, std::shared_ptr<Holder> arg );
  virtual ~Fun();

  void prependName( std::vector<std::wstring> name );

  virtual boost::tribool address() const;
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  std::vector<std::wstring> mName;
  std::shared_ptr<Holder> mArg;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_FUN_HPP
