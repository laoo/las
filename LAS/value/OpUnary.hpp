#ifndef LAS_VALUE_OPUNARY_HPP
#define LAS_VALUE_OPUNARY_HPP

#include "Holder.hpp"
#include "Operators.hpp"

namespace las
{
namespace value
{

class OpUnary : public Holder
{
public:

  static std::shared_ptr<Holder> create( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && data );

  OpUnary( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && data );
  virtual ~OpUnary();

  virtual boost::tribool address() const;
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  Operators mOp;
  std::shared_ptr<Holder> mData;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_OPUNARY_HPP
