#include "stdafx.h"
#include "OpUnary.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

std::shared_ptr<Holder> OpUnary::create( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && data )
{
  if ( data->address() )
  {
    throw SemanticException( pos, L"Can't negate an address" );
  }

  int value;
  if ( data->literal( value ) )
  {
    switch ( op )
    {
    case OP_ARITHMETIC_NEGATE:
      data->updateLiteral( pos, -value );
      break;
    case OP_LOGIC_NEGATE:
      data->updateLiteral( pos, !value );
      break;
    case OP_BINARY_NEGATE:
      data->updateLiteral( pos, ~value );
      break;
    default:
      assert( false );
    }
    return data;
  }
  else
  {
    return std::make_shared<OpUnary>( pos, op, std::move( data ) );
  }
}

OpUnary::OpUnary( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && data ) : Holder( pos ), mOp( op ), mData( std::move( data ) )
{
}

OpUnary::~OpUnary()
{
}

boost::tribool OpUnary::address() const
{
  return false;
}

Host OpUnary::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  Host h = mData->calcValue( context, dec, req );
  if ( h.isAddress() )
  {
    throw SemanticException( position(), L"Can't negate an address" );
  }

  assertValue( req );

  switch ( mOp )
  {
  case OP_ARITHMETIC_NEGATE:
    return Host( position(), -h.getValue() );
  case OP_LOGIC_NEGATE:
    return Host( position(), !h.getValue() );
  case OP_BINARY_NEGATE:
    return Host( position(), ~h.getValue() );
  default:
    throw Exception( L"Unexpected" );
  }
}

Measure OpUnary::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  return mData->measureValue( context, dec, req );
}

std::optional<int> OpUnary::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las

