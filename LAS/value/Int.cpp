#include "stdafx.h"
#include "Int.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

Int::Int( GE5::Position const& pos, int v ) : Holder( pos ), mValue( v )
{
}

Int::~Int()
{
}

bool Int::literal( int & out ) const
{
  out = mValue;
  return true;
}

boost::tribool Int::address() const
{
  return false;
}

void Int::updateLiteral( GE5::Position const& pos, int value )
{
  mPosition = pos;
  mValue = value;
}

Host Int::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  return Host( position(), mValue );
}

Measure Int::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  return Measure( position() );
}

std::optional<int> Int::retrieveBank( CalcContext & context ) const
{
  return mValue >> 16;
}

} //namespace value
} //namespace las

