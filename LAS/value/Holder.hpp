#ifndef LAS_VALUE_HOLDER_HPP
#define LAS_VALUE_HOLDER_HPP

#include "PositionHolder.hpp"
#include "Host.hpp"
#include "Measure.hpp"
#include "CalcContext.hpp"
#include "MeasureContext.hpp"
#include "Decoration.hpp"

namespace las
{
namespace value
{

class Holder : public las::PositionHolder
{
public:
  Holder( GE5::Position const& pos );
  virtual ~Holder();

  virtual boost::tribool address() const = 0;
  virtual bool literal( int & out ) const;
  virtual void updateLiteral( GE5::Position const& pos, int value );

  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const = 0;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const = 0;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const = 0;

protected:
  void assertValue( UsageReq req ) const;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_HOLDER_HPP
