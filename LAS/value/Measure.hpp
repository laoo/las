#ifndef LAS_VALUE_MEASURE_HPP
#define LAS_VALUE_MEASURE_HPP

#include "PositionHolder.hpp"
#include "Decoration.hpp"

namespace las
{
class SegmentProxy;
namespace value
{

class Measure : public las::PositionHolder
{
public:
  enum Kind
  {
    M_VALUE,
    M_ADDRESS
  };

  Measure( GE5::Position const& pos );
  Measure( GE5::Position const& pos, SegmentProxy * seg );
  ~Measure();

  Kind getKind() const;

  SegmentProxy * getSegment();

  Decoration getDecoration() const;
  void decorate( Decoration dec );
  bool isAddress() const;

private:
  Kind mKind;
  Decoration mDec;
  SegmentProxy * mSegmentOfTheAddress;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_MEASURE_HPP
