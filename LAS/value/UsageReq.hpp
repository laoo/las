#ifndef LAS_VALUE_USAGEREQ_HPP
#define LAS_VALUE_USAGEREQ_HPP

namespace las
{
namespace value
{

enum UsageReq
{
  U_VALUE,
  U_BRANCH,
  U_ACCESS
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_USAGEREQ_HPP
