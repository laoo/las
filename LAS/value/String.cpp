#include "stdafx.h"
#include "String.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

String::String( GE5::Position const& pos, std::wstring s, value::CharType type ) : Holder( pos ), mString( s ), mType( type )
{
}

String::~String()
{
}

bool String::literal( int & out ) const
{
  if ( mString.size() == 1 )
  {
    out = atasciiToInternal( static_cast<unsigned char>( mString[0] ) );
    return true;
  }
  else
  {
    return false;
  }
}

boost::tribool String::address() const
{
  return false;
}

void String::updateLiteral( GE5::Position const& pos, int value )
{
  assert( mString.size() == 1 );
  mString[0] = static_cast<wchar_t>( internalToAtascii( value & 0xff ) );
  mPosition = pos;
}


unsigned char String::atasciiToInternal( unsigned char input ) const
{
  //table steald from atari rom
  static unsigned char AINCC[] = { 0x40, 0x00, 0x20, 0x60 };
  return conv( input, AINCC );
}

unsigned char String::internalToAtascii( unsigned char input ) const
{
  //table steald from atari rom
  static unsigned char INACC[] = { 0x20, 0x40, 0x00, 0x60 };
  return conv( input, INACC );
}

unsigned char String::conv( unsigned char input, unsigned char tab[4] ) const
{
  if ( mType == value::CT_INTERNAL )
  {
    return input & 0x9f | tab[ input >> 5 ];
  }
  else
  {
    return input;
  }
}

Host String::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  assertValue( req );
  throw NYIException( position(), L"calcValue" );
}

Measure String::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  throw NYIException( position(), L"measureValue" );
}

std::optional<int> String::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las

