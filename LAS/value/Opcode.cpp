#include "stdafx.h"
#include "Opcode.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

Opcode::Opcode( GE5::Position const& pos, std::shared_ptr<Command> cmd ) : Holder( pos ), mCmd( std::move( cmd ) )
{
}

Opcode::~Opcode()
{
}

boost::tribool Opcode::address() const
{
  return false;
}

Host Opcode::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  assertValue( req );
  throw NYIException( position(), L"calcValue" );
}

Measure Opcode::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  throw NYIException( position(), L"measureValue" );
}

std::optional<int> Opcode::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las

