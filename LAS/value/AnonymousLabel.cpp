#include "stdafx.h"
#include "AnonymousLabel.hpp"
#include "CalcContext.hpp"
#include "MeasureContext.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

AnonymousLabel::AnonymousLabel( GE5::Position const& pos, Direction dir, int skip ) : Holder( pos ),
  mDir( dir ), mSkip( skip )
{
}

AnonymousLabel::~AnonymousLabel()
{
}

boost::tribool AnonymousLabel::address() const
{
  return true;
}

Host AnonymousLabel::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  return Host( position(), (int)context.anonymousLabelOffset( position(), mDir == FORWARD, mSkip ), context.currentCodeSegment() );
}

Measure AnonymousLabel::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  return Measure( position(), context.currentCodeSegment() );
}

std::optional<int> AnonymousLabel::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las
