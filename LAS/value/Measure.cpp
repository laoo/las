#include "stdafx.h"
#include "Measure.hpp"

namespace las
{
namespace value
{

Measure::Measure( GE5::Position const& pos ) : PositionHolder( pos ),
  mKind( M_VALUE ), mDec( D_NONE ), mSegmentOfTheAddress( nullptr )
{
}

Measure::Measure( GE5::Position const& pos, SegmentProxy * seg ) : PositionHolder( pos ),
  mKind( M_ADDRESS ), mDec( D_NONE ), mSegmentOfTheAddress( seg )
{
  assert( mSegmentOfTheAddress != nullptr );
}

Measure::~Measure()
{
}

Measure::Kind Measure::getKind() const
{
  return mKind;
}


SegmentProxy * Measure::getSegment()
{
  return mSegmentOfTheAddress;
}

Decoration Measure::getDecoration() const
{
  return mDec;
}

void Measure::decorate( Decoration dec )
{
  mDec = dec;
}

bool Measure::isAddress() const
{
  return mKind == M_ADDRESS;
}

} //namespace value
} //namespace las

