#ifndef LAS_VALUE_PC_HPP
#define LAS_VALUE_PC_HPP

#include "Holder.hpp"

namespace las
{
namespace value
{

class PC : public Holder
{
public:

  PC( GE5::Position const& pos );
  virtual ~PC();

  virtual boost::tribool address() const;
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

};

} //namespace value
} //namespace las

#endif //LAS_VALUE_PC_HPP
