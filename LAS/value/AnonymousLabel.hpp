#ifndef LAS_VALUE_ANONYMOUSLABEL_HPP
#define LAS_VALUE_ANONYMOUSLABEL_HPP

#include "Holder.hpp"

namespace las
{
namespace value
{

class AnonymousLabel : public Holder
{
public:
  enum Direction
  {
    BACKWARD,
    FORWARD
  };

  AnonymousLabel( GE5::Position const& pos, Direction dir, int skip );
  virtual ~AnonymousLabel();

  virtual boost::tribool address() const;
  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

private:
  Direction mDir;
  int mSkip;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_ANONYMOUSLABEL_HPP
