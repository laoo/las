#include "stdafx.h"
#include "OpBinary.hpp"
#include "Exception.hpp"

namespace las
{
namespace value
{

std::shared_ptr<Holder> OpBinary::create( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && left, std::shared_ptr<Holder> && right )
{
  switch ( op )
  {
  case OP_PLUS:
    if ( left->address() && right->address() )
    {
      throw SemanticException( pos, L"A sum of two addresses does not have a meaning" );
    }
    break;
  case OP_MINUS:
    if ( !left->address() && right->address() )
    {
      throw SemanticException( pos, L"Subtracting address form a value does not have a meaning" );
    }
    break;
  case OP_MULTIPLY:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't multiply an address" );
    }
    break;
  case OP_DIVIDE:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't divide an address" );
    }
    break;
  case OP_MODULO:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't modulo an address" );
    }
    break;
  case OP_SHL:
  case OP_SHR:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't shift an address" );
    }
    break;
  case OP_L:
  case OP_G:
  case OP_LE:
  case OP_GE:
  case OP_EQ:
  case OP_NE:
    break;
  case OP_BINARY_AND:
    if ( left->address() && right->address() )
    {
      throw SemanticException( pos, L"Can't binary AND two addresses" );
    }
    break;
  case OP_BINARY_XOR:
    if ( left->address() && right->address() )
    {
      throw SemanticException( pos, L"Can't binary XOR two addresses" );
    }
    break;
  case OP_BINARY_OR:
    if ( left->address() && right->address() )
    {
      throw SemanticException( pos, L"Can't binary OR two addresses" );
    }
    break;
  case OP_LOGICAL_AND:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't logically AND an address" );
    }
    break;
  case OP_LOGICAL_OR:
    if ( left->address() || right->address() )
    {
      throw SemanticException( pos, L"Can't logically OR an address" );
    }
    break;
  default:
    assert(false);
  }

  int l, r;
  if ( left->literal( l ) && right->literal( r ) )
  {
    switch ( op )
    {
    case OP_PLUS:
      left->updateLiteral( pos, l + r );
      break;
    case OP_MINUS:
      left->updateLiteral( pos, l - r );
      break;
    case OP_MULTIPLY:
      left->updateLiteral( pos, l * r );
      break;
    case OP_DIVIDE:
      if ( r == 0 )
        throw SemanticException( pos, L"Division by 0" );
      left->updateLiteral( pos, l / r );
      break;
    case OP_MODULO:
        throw SemanticException( pos, L"Modulo by 0" );
      left->updateLiteral( pos, l % r );
      break;
    case OP_SHL:
      left->updateLiteral( pos, l << r );
      break;
    case OP_SHR:
      left->updateLiteral( pos, l >> r );
      break;
    case OP_L:
      left->updateLiteral( pos, l < r );
      break;
    case OP_G:
      left->updateLiteral( pos, l > r );
      break;
    case OP_LE:
      left->updateLiteral( pos, l <= r );
      break;
    case OP_GE:
      left->updateLiteral( pos, l >= r );
      break;
    case OP_EQ:
      left->updateLiteral( pos, l == r );
      break;
    case OP_NE:
      left->updateLiteral( pos, l != r );
      break;
    case OP_BINARY_AND:
      left->updateLiteral( pos, l & r );
      break;
    case OP_BINARY_XOR:
      left->updateLiteral( pos, l ^ r );
      break;
    case OP_BINARY_OR:
      left->updateLiteral( pos, l | r );
      break;
    case OP_LOGICAL_AND:
      left->updateLiteral( pos, l && r );
      break;
    case OP_LOGICAL_OR:
      left->updateLiteral( pos, l || r );
      break;
    default:
      assert(false);
    }
    return left;
  }
  else
  {
    return std::make_shared<OpBinary>( pos, op, std::move( left ), std::move( right ) );
  }
}

OpBinary::OpBinary( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && left, std::shared_ptr<Holder> && right ) : Holder( pos ),
  mOp( op ), mLeft( std::move( left ) ), mRight( std::move( right ) )
{
}

OpBinary::~OpBinary()
{
}

boost::tribool OpBinary::address() const
{
  switch ( mOp )
  {
  case OP_PLUS:
  case OP_BINARY_AND:
  case OP_BINARY_XOR:
  case OP_BINARY_OR:
    return mLeft->address() || mRight->address();
  case OP_MINUS:
    if ( mLeft->address() )
      return !mRight->address();
    else if ( !mLeft->address() )
      return false;
    else
      return true;
  case OP_MULTIPLY:
  case OP_DIVIDE:
  case OP_MODULO:
  case OP_SHL:
  case OP_SHR:
  case OP_L:
  case OP_G:
  case OP_LE:
  case OP_GE:
  case OP_EQ:
  case OP_NE:
  case OP_LOGICAL_AND:
  case OP_LOGICAL_OR:
    return false;
  default:
    assert(false);
    return false;
  }
}

Host OpBinary::calcValue( CalcContext & context, Decoration dec, UsageReq req ) const
{
  Host hLeft = mLeft->calcValue( context, dec, req );
  Host hRight = mRight->calcValue( context, dec, req );

  switch ( mOp )
  {
  case OP_PLUS:
    if ( hLeft.isAddress() )
    {
      if ( hRight.isAddress() )
      {
        throw SemanticException( position(), L"Can't sum two addresses" );
      }
      else
      {
        return Host( position(), hLeft.getValue() + hRight.getValue(), hLeft.getSegment() );
      }
    }
    else if ( hRight.isAddress() )
    {
      return Host( position(), hLeft.getValue() + hRight.getValue(), hRight.getSegment() );
    }
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() + hRight.getValue() );
    }
  case OP_BINARY_AND:
    if ( hLeft.isAddress() )
    {
      if ( hRight.isAddress() )
      {
        throw SemanticException( position(), L"Can't AND two addresses" );
      }
      else
      {
        return Host( position(), hLeft.getValue() & hRight.getValue(), hLeft.getSegment() );
      }
    }
    else if ( hRight.isAddress() )
    {
      return Host( position(), hLeft.getValue() & hRight.getValue(), hRight.getSegment() );
    }
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() & hRight.getValue() );
    }
  case OP_BINARY_XOR:
    if ( hLeft.isAddress() )
    {
      if ( hRight.isAddress() )
      {
        throw SemanticException( position(), L"Can't XOR two addresses" );
      }
      else
      {
        return Host( position(), hLeft.getValue() ^ hRight.getValue(), hLeft.getSegment() );
      }
    }
    else if ( hRight.isAddress() )
    {
      return Host( position(), hLeft.getValue() ^ hRight.getValue(), hRight.getSegment() );
    }
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() ^ hRight.getValue() );
    }
  case OP_BINARY_OR:
    if ( hLeft.isAddress() )
    {
      if ( hRight.isAddress() )
      {
        throw SemanticException( position(), L"Can't OR two addresses" );
      }
      else
      {
        return Host( position(), hLeft.getValue() | hRight.getValue(), hLeft.getSegment() );
      }
    }
    else if ( hRight.isAddress() )
    {
      return Host( position(), hLeft.getValue() | hRight.getValue(), hRight.getSegment() );
    }
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() | hRight.getValue() );
    }
  case OP_MINUS:
    if ( hLeft.isAddress() )
    {
      if ( hRight.isAddress() )
      {
        assertValue( req );
        return Host( position(), hLeft.getValue() - hRight.getValue() );  //difference of two addresses is not an address
      }
      else
      {
        return Host( position(), hLeft.getValue() - hRight.getValue(), hLeft.getSegment() );
      }
    }
    else if ( hRight.isAddress() )
    {
      throw SemanticException( position(), L"Can't subtract address from value" );
    }
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() - hRight.getValue() );
    }
  case OP_MULTIPLY:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't multiply an address" );
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() * hRight.getValue() );
    }
  case OP_DIVIDE:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't divide an address" );
    else
    {
      if ( hRight.getValue() == 0 )
        throw SemanticException( position(), L"Division by 0" );
      else
      {
        assertValue( req );
        return Host( position(), hLeft.getValue() / hRight.getValue() );
      }
    }
  case OP_MODULO:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't modulo an address" );
    else
    {
      if ( hRight.getValue() == 0 )
        throw SemanticException( position(), L"Modulo by 0" );
      else
      {
        assertValue( req );
        return Host( position(), hLeft.getValue() % hRight.getValue() );
      }
    }
  case OP_SHL:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't shift an address" );
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() << hRight.getValue() );
    }
  case OP_SHR:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't shift an address" );
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() >> hRight.getValue() );
    }
  case OP_L:
      assertValue( req );
      return Host( position(), hLeft.getValue() < hRight.getValue() );
  case OP_G:
      assertValue( req );
      return Host( position(), hLeft.getValue() > hRight.getValue() );
  case OP_LE:
      assertValue( req );
      return Host( position(), hLeft.getValue() <= hRight.getValue() );
  case OP_GE:
      assertValue( req );
      return Host( position(), hLeft.getValue() >= hRight.getValue() );
  case OP_EQ:
      assertValue( req );
      return Host( position(), hLeft.getValue() == hRight.getValue() );
  case OP_NE:
      assertValue( req );
      return Host( position(), hLeft.getValue() != hRight.getValue() );
  case OP_LOGICAL_AND:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't logically AND an address" );
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() && hRight.getValue() );
    }
  case OP_LOGICAL_OR:
    if ( hLeft.isAddress() || hRight.isAddress() )
      throw SemanticException( position(), L"Can't logically OR an address" );
    else
    {
      assertValue( req );
      return Host( position(), hLeft.getValue() || hRight.getValue() );
    }
  default:
    throw Exception( L"Unexpected" );
  }
}

Measure OpBinary::measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const
{
  Measure left = mLeft->measureValue( context, dec, req );
  Measure right = mRight->measureValue( context, dec, req );

  switch ( mOp )
  {
  case OP_PLUS:
    if ( left.isAddress() )
    {
      if ( right.isAddress() )
      {
        throw SemanticException( position(), L"Can't sum two addresses" );
      }
      else
      {
        return Measure( position(), left.getSegment() );
      }
    }
    else if ( right.isAddress() )
    {
      return Measure( position(), right.getSegment() );
    }
    else
    {
      return Measure( position() );
    }
  case OP_BINARY_AND:
    if ( left.isAddress() )
    {
      if ( right.isAddress() )
      {
        throw SemanticException( position(), L"Can't AND two addresses" );
      }
      else
      {
        return Measure( position(), left.getSegment() );
      }
    }
    else if ( right.isAddress() )
    {
      return Measure( position(), right.getSegment() );
    }
    else
    {
      return Measure( position() );
    }
  case OP_BINARY_XOR:
    if ( left.isAddress() )
    {
      if ( right.isAddress() )
      {
        throw SemanticException( position(), L"Can't XOR two addresses" );
      }
      else
      {
        return Measure( position(), left.getSegment() );
      }
    }
    else if ( right.isAddress() )
    {
      return Measure( position(), right.getSegment() );
    }
    else
    {
      return Measure( position() );
    }
  case OP_BINARY_OR:
    if ( left.isAddress() )
    {
      if ( right.isAddress() )
      {
        throw SemanticException( position(), L"Can't OR two addresses" );
      }
      else
      {
        return Measure( position(), left.getSegment() );
      }
    }
    else if ( right.isAddress() )
    {
      return Measure( position(), right.getSegment() );
    }
    else
    {
      return Measure( position() );
    }
  case OP_MINUS:
    if ( left.isAddress() )
    {
      if ( right.isAddress() )
      {
        return Measure( position() );  //difference of two addresses is not an address
      }
      else
      {
        return Measure( position(), left.getSegment() );
      }
    }
    else if ( right.isAddress() )
    {
      throw SemanticException( position(), L"Can't subtract address from value" );
    }
    else
    {
      return Measure( position() );
    }
  case OP_MULTIPLY:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't multiply an address" );
    else
      return Measure( position() );
  case OP_DIVIDE:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't divide an address" );
    else
      return Measure( position() );
  case OP_MODULO:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't modulo an address" );
    else
      return Measure( position() );
  case OP_SHL:
  case OP_SHR:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't shift an address" );
    else
      return Measure( position() );
  case OP_L:
  case OP_G:
  case OP_LE:
  case OP_GE:
  case OP_EQ:
  case OP_NE:
      return Measure( position() );
  case OP_LOGICAL_AND:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't logically AND an address" );
    else
      return Measure( position() );
  case OP_LOGICAL_OR:
    if ( left.isAddress() || right.isAddress() )
      throw SemanticException( position(), L"Can't logically OR an address" );
    else
      return Measure( position() );
  default:
    throw Exception( L"Unexpected" );
  }
}

std::optional<int> OpBinary::retrieveBank( CalcContext & context ) const
{
  throw NYIException( position(), L"retrieveBank" );
}

} //namespace value
} //namespace las

