#ifndef LAS_VALUE_DECORATION_HPP
#define LAS_VALUE_DECORATION_HPP

namespace las
{
namespace value
{

enum Decoration
{
  D_NONE,
  D_DIRECTED,
  D_ABSOLUTED,
  D_LONGENED
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_DECORATION_HPP
