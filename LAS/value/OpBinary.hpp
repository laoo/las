#ifndef LAS_VALUE_OPBINARY_HPP
#define LAS_VALUE_OPBINARY_HPP

#include "Holder.hpp"
#include "Operators.hpp"

namespace las
{
namespace value
{

class OpBinary : public Holder
{
public:

  static std::shared_ptr<Holder> create( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && left, std::shared_ptr<Holder> && right );

  virtual ~OpBinary();

  virtual boost::tribool address() const;

  OpBinary( GE5::Position const& pos, Operators op, std::shared_ptr<Holder> && left, std::shared_ptr<Holder> && right );

  virtual Host calcValue( CalcContext & context, Decoration dec, UsageReq req ) const;
  virtual Measure measureValue( MeasureContext & context, Decoration dec, UsageReq req ) const;
  virtual std::optional<int> retrieveBank( CalcContext & context ) const;

protected:


private:
  Operators mOp;
  std::shared_ptr<Holder> mLeft;
  std::shared_ptr<Holder> mRight;
};

} //namespace value
} //namespace las

#endif //LAS_VALUE_OPBINARY_HPP
