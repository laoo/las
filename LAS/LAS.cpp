
#include "stdafx.h"

#include "ge5/include/IParser.hpp"
#include "ge5/include/IParserServer.hpp"
#include "ge5/include/Exception.hpp"
#include "Exception.hpp"
#include "Reductor.hpp"
#include "Module.hpp"
#include "Logger.hpp"
#include "Assembler.hpp"
#include "LAS.h"



std::shared_ptr<las::Module> parseModule( las::Reductor & reductor, std::filesystem::path path )
{
  int id;
  std::shared_ptr<las::Module> pModule = reductor.creteModule( path, id );

  try
  {
    if ( !std::filesystem::exists( path ) )
      throw las::Exception( std::wstring( L"'" ) + path.wstring() + L"'" + std::wstring( L" Does not exist" ) );

    std::ifstream fin( path.string().c_str(), std::ios::binary );
    if ( fin.good() )
    {
      auto parser = reductor.getServer().open( path.wstring(), fin, id, true );
      parser->parse();
    }
    else throw las::Exception( std::wstring( L"Can't open" ) );
  }
  catch ( GE5::ParseError const& e )
  {
    las::Logger::err( e.position(), e.wWhat() );
    return std::shared_ptr<las::Module>();
  }
  catch ( las::SemanticException const& e )
  {
    las::Logger::err( e.position(), e.wWhat() );
    return std::shared_ptr<las::Module>();
  }
  catch ( las::StopParsingException const& e )
  {
    las::Logger::notice( e.position(), L"Forcing end of compilation. It's OK not doing it ;)" );
  }
  catch ( las::Exception const& e )
  {
    las::Logger::err( e.wWhat() );
    return std::shared_ptr<las::Module>();
  }

  return pModule;
}

int wmain(int argc, wchar_t* argv[])
{
  if ( argc != 2 )
    return 1;

  try
  {
    las::Reductor reductor{ GE5::createServer( (char*)g_Grammar, (char*)g_Grammar + g_Grammar_size ) };
    las::Assembler assembler{};

    try
    {

      for ( int i = 1; i < argc; ++i )
      {
        std::filesystem::path p{ argv[i] };
        if ( auto pModule = parseModule( reductor, p ) )
          assembler.addModule( std::move( pModule ) );
      }

      assembler.process();
    }
    catch ( las::SemanticException const& e )
    {
      las::Logger::err( e.position(), e.wWhat() );
      return 1;
    }
    catch ( las::Exception const& e )
    {
      las::Logger::err( e.wWhat() );
      return 1;
    }
  }
  catch ( GE5::Exception const& e )
  {
    std::string what = e.what();
    std::cerr << what;
  }

  return 0;
}
