#include "stdafx.h"
#include "EmptyCalcContext.hpp"
#include "value/Host.hpp"
#include "value/Measure.hpp"
#include "Exception.hpp"

namespace las
{

const wchar_t EmptyCalcContext::mMessage[] = L"Unavailable";


EmptyCalcContext::EmptyCalcContext()
{
}

EmptyCalcContext::~EmptyCalcContext()
{
}

size_t EmptyCalcContext::currentSegmentOffset() const
{
  throw Exception( mMessage );
}

SegmentProxy * EmptyCalcContext::currentCodeSegment()
{
  throw Exception( mMessage );
}

SegmentProxy * EmptyCalcContext::currentDataSegment()
{
  throw Exception( mMessage );
}

size_t EmptyCalcContext::anonymousLabelOffset( GE5::Position const& pos, bool forward, int count )
{
  throw Exception( mMessage );
}

value::Host EmptyCalcContext::functionValue( GE5::Position const& pos, std::vector<std::wstring> const& name,
  std::optional<value::Host> arg, value::UsageReq req )
{
  throw Exception( mMessage );
}

value::Host EmptyCalcContext::calcIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
  value::Decoration dec , value::UsageReq req )
{
  throw Exception( mMessage );
}

void EmptyCalcContext::appendToEmitList( SegmentProxy * segmentProxyToAppend )
{
  throw Exception( mMessage );
}


} //namespace las

