#include "stdafx.h"
#include "Equ.hpp"
#include "GatherSession.hpp"
#include "Exception.hpp"

namespace las
{

Equ::Equ( GE5::Position const& position, std::wstring name, Value && value ) : SegmentContent( position ),
  mName( std::move( name ) ), mValue( std::move( value ) )
{
}

Equ::~Equ()
{
}

void Equ::gather( GatherSession & session ) const
{
  // nothing as it's remembered in Segment's symbol map
}

void Equ::emit( EmitSession & session ) const
{
  // nothing as it's remembered in Segment's symbol map
}

Value const& Equ::getValue() const
{
  return mValue;
}


wchar_t const* Equ::getName()
{
  return L"equ";
}

std::wstring const& Equ::name() const
{
  return mName;
}


} //namespace las

