#ifndef LAS_CPUMODE_HPP
#define LAS_CPUMODE_HPP

#include "SegmentContent.hpp"

namespace las
{

class CPUMode : public SegmentContent
{
public:
  enum Mode
  {
    NATIVE,
    EMULATION
  };

  CPUMode( GE5::Position const& position, Mode m, int accumSize, int indexSize );
  CPUMode( GE5::Position const& position );
  virtual ~CPUMode();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

  // 
  static std::shared_ptr<CPUMode> native( GE5::Position const& position, int accumSize, int indexSize );
  static std::shared_ptr<CPUMode> emulation( GE5::Position const& position );

private:

  Mode mMode;
  //any size of zero indicate no change in size. default after switch from emulation is 8.
  int mAccumSize;
  int mIndexSize;


  friend bool operator==( CPUMode const& left, CPUMode const& right );
  friend bool operator!=( CPUMode const& left, CPUMode const& right );
};

} //namespace las

#endif //LAS_CPUMODE_HPP
