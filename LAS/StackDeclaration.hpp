#ifndef LAS_STACKDECLARATION_HPP
#define LAS_STACKDECLARATION_HPP

#include "SegmentContent.hpp"

namespace las
{

class StackDeclaration : public SegmentContent
{
public:
  enum StackElem
  {
    POP,
    BYTE,
    WORD,
    LONG
  };

  typedef std::pair< StackElem, std::wstring > ElemType;

  typedef std::vector< ElemType > DataType;

  StackDeclaration( GE5::Position const& position, DataType data );
  ~StackDeclaration();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  DataType mData;
};

} //namespace las

#endif //LAS_STACKDECLARATION_HPP
