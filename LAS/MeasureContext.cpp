#include "stdafx.h"
#include "MeasureContext.hpp"
#include "value/Host.hpp"
#include "value/Measure.hpp"
#include "Exception.hpp"

namespace las
{

const wchar_t MeasureContext::mMessage[] = L"Unavailable";


MeasureContext::~MeasureContext()
{
}

SegmentProxy * MeasureContext::currentCodeSegment()
{
  throw Exception( mMessage );
}

SegmentProxy * MeasureContext::currentDataSegment()
{
  throw Exception( mMessage );
}

value::Measure MeasureContext::measureIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
  value::Decoration dec, value::UsageReq req )
{
  throw Exception( mMessage );
}

} //namespace las

