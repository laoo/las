#ifndef LAS_CALCCONTEXT_HPP
#define LAS_CALCCONTEXT_HPP

#include "value/Decoration.hpp"
#include "value/UsageReq.hpp"

namespace GE5
{
struct Position;
}

namespace las
{
namespace value
{
class Host;
class Measure;
}

class SegmentProxy;


class CalcContext
{
public:

  virtual ~CalcContext() {};

  virtual size_t currentSegmentOffset() const = 0;
  virtual size_t anonymousLabelOffset( GE5::Position const& pos, bool forward, int count ) = 0;
  virtual SegmentProxy * currentCodeSegment() = 0;
  virtual SegmentProxy * currentDataSegment() = 0;
  virtual value::Host functionValue( GE5::Position const& pos, std::vector<std::wstring> const& name,
    std::optional<value::Host> arg, value::UsageReq req ) = 0;

  virtual value::Host calcIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec , value::UsageReq req ) = 0;

  virtual void appendToEmitList( SegmentProxy * segmentProxyToAppend ) = 0;
};

} //namespace las

#endif //LAS_CALCCONTEXT_HPP
