#ifndef LAS_LOGGER_HPP
#define LAS_LOGGER_HPP

#include <boost/filesystem.hpp>
#include "ge5/include/Position.hpp"

namespace las
{
class Logger
{
public:
  enum Level
  {
    LEVEL_TRACE,
    LEVEL_NOTICE,
    LEVEL_WARNING,
    LEVEL_ERROR
  };

public:
  static void log( Logger::Level level, std::wstring const& message );
  static void log( Logger::Level level, GE5::Position const& pos, std::wstring const& message );

  static void trace( std::wstring const& message );
  static void notice( std::wstring const& message );
  static void notice( GE5::Position const& pos, std::wstring const& message );
  static void warn( std::wstring const& message );
  static void warn( GE5::Position const& pos, std::wstring const& message );
  static void err( GE5::Position const& pos, std::wstring const& message );
  static void err( std::wstring const& message );

};

} //namespace las

#endif //LAS_VALUE_HPP
