#include "stdafx.h"
#include "NameUtils.hpp"

namespace las
{

std::wstring wstring( std::vector< std::wstring > const& name )
{
  if ( name.empty() )
    return std::wstring();

  std::wstring ret( name[0] );

  for ( size_t i = 1; i < name.size(); ++i )
  {
    ret += L".";
    ret += name[1];
  }

  return ret;
}

} //namespace las

