#ifndef LAS_COMMAND_HPP
#define LAS_COMMAND_HPP

#include "SegmentContent.hpp"
#include "Value.hpp"

namespace las
{
class Label;

class Command : public SegmentContent
{
public:

  enum DataDeclarationType
  {
    VARIABLE,
    BYTE,
    WORD,
    LONG
  };

  Command( GE5::Position const& position );
  virtual ~Command();

  static std::shared_ptr<Command> createOrg( GE5::Position const& position, Value && v );
  static std::shared_ptr<Command> createDataDeclaration( GE5::Position const& position,
    DataDeclarationType type, std::vector< Value > && data );
  static std::shared_ptr<Command> createSpaceDeclaration( GE5::Position const& position, Value && size );

  void setLabel( std::vector< std::shared_ptr<Label> > labels );

  virtual void gather( GatherSession & session ) const;

  virtual wchar_t const* getName();

protected:
  std::vector< std::shared_ptr<Label> > mLabels;
};

} //namespace las

#endif //LAS_COMMAND_HPP
