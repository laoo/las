#include "stdafx.h"
#include "Reductor.hpp"
#include "ge5/include/IParserServer.hpp"
#include "ge5/include/Exception.hpp"
#include "ge5/include/any.hpp"
#include "Module.hpp"
#include "Logger.hpp"
#include "Exception.hpp"
#include "Command.hpp"
#include "Addressing.hpp"
#include "Label.hpp"
#include "Usage.hpp"
#include "UsageShift.hpp"
#include "Attribute.hpp"
#include "Out.hpp"
#include "Equ.hpp"
#include "CPUMode.hpp"
#include "StackDeclaration.hpp"
#include "value/Operators.hpp"

namespace las
{
namespace
{
int parseDec( std::wstring s )
{
  unsigned int x;
  std::wstringstream ss;
  ss << s;
  ss >> x;
  return x;
}
}

std::shared_ptr<Module> Reductor::creteModule( std::filesystem::path path, int & outId )
{
  auto it = std::find( mModules.begin(), mModules.end(), nullptr );
  if ( it == mModules.end() )
  {
    mModules.push_back( nullptr );
    it = mModules.begin() + mModules.size() - 1;
  }

  std::shared_ptr<Module> pModule = std::shared_ptr<Module>( new Module( path ), [this]( Module * p )
  {
    delete p;

    auto foundIt = std::find( mModules.begin(), mModules.end(), p );
    if ( foundIt != mModules.end() )
    {
      *foundIt = nullptr;
    }
    else
    {
      assert( false );
    }
  } );

  *it = pModule.get();

  outId = (int)std::distance( mModules.begin(), it );

  return pModule;
}

GE5::IParserServer & Reductor::getServer()
{
  assert( mServer );
  return *mServer;
}

Module & Reductor::module( int id )
{
  assert( id >= 0 && id < (int)mModules.size() );
  return *mModules[ id ];
}

Reductor::~Reductor()
{
}

Reductor::Reductor( std::shared_ptr<GE5::IParserServer> server ) : mServer( std::move( server ) )
{
  auto forward = []( GE5::ReductorArg & r ) -> GE5::any
  {
    GE5::any result( std::move( r[0] ) );
    return result;
  };

  auto empty = []( GE5::ReductorArg & r ) -> GE5::any
  {
    return GE5::any();
  };

  mServer->addReductor( L"<nl> ::= NewLine <nl>", empty );
  mServer->addReductor( L"<nl> ::= NewLine", empty );
  mServer->addReductor( L"<nl Opt> ::= NewLine <nl Opt>", empty);
  mServer->addReductor( L"<nl Opt> ::= <>", empty );
  mServer->addReductor( L"<Program> ::= <nl Opt> <Statements>", empty );
  mServer->addReductor( L"<Statements> ::= <Statement> <Statements>", empty );
  mServer->addReductor( L"<Statements> ::= <Statement>", empty );
  mServer->addReductor( L"<Statement> ::= <Namespace> <nl>", empty );
  mServer->addReductor( L"<Statement> ::= <Segment> <nl>", empty );
  mServer->addReductor( L"<Statement> ::= <Dir> <nl>", empty );
  mServer->addReductor( L"<Statement> ::= <Gen> <nl>", empty );
  mServer->addReductor( L"<Statement> ::= <End> <nl>", empty );
  mServer->addReductor( L"<Statement> ::= <Out> <nl>", empty );

  mServer->addReductor( L"<Namespace> ::= ns <NSName>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v = r.move< std::vector< std::wstring > >( 1 );

    for ( auto it = v.cbegin(); it != v.cend(); ++it )
    {
      module( r.id() ).pushNamespace( r.pos(), *it );
    }
    
    return GE5::any();
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Segment> ::= seg <NSName>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v = r.move< std::vector< std::wstring > >( 1 );

    for ( size_t i = 0; i < v.size() - 1; ++i )
    {
      module( r.id() ).pushNamespace( r.pos( 1 ), v[i] );
    }
    module( r.id() ).addSegment( r.pos(), v.back() );

    return GE5::any();
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<End> ::= end <NSName Opt>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v = r.move< std::vector< std::wstring > >( 1 );

    if ( v.empty() )
    {
        module( r.id() ).closeObject( r.pos(), std::wstring() );
    }
    else
    {
      for ( auto it = v.crbegin(); it != v.crend(); ++it )
      {
        module( r.id() ).closeObject( r.pos(), *it );
      }
    }

    return GE5::any();
  } );

  mServer->addReductor( L"<Out> ::= out InternalString <Id Opt>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addOut( std::make_shared<Out>( r.pos(), r.move<std::wstring>( 1 ), r.move<std::wstring>( 2 ) ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= attr <Attr>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addAttribute( std::make_shared<Attribute>( r.pos(), r.move<std::vector<Value>>( 1 ) ) );
    return GE5::any();
  } );

  //mServer->addReductor( L"<Dir> ::= con <Expr List>", []( GE5::ReductorArg & r ) -> GE5::any
  //{
  //  throw NYIException( r.pos(), L"Constraints" );
  //  return GE5::any();
  //} );

  mServer->addReductor( L"<Dir> ::= use <Usage>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addUsage( r.move<std::shared_ptr<Usage>>( 1 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= use <Usage Shift>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).shiftUsage( r.move<std::shared_ptr<UsageShift>>( 1 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= emu", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addCPUMode( CPUMode::emulation( r.pos() ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= nat <Reg Size>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    auto pair = r.move<std::pair<int,int>>( 1 );
    module( r.id() ).addCPUMode( CPUMode::native( r.pos(), pair.first, pair.second ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= Id equ <Expr>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addEqu( std::make_shared<Equ>( r.pos(), r.move<std::wstring>( 0 ), r.move<Value>( 2 ) ) );
    return GE5::any();
  } );
  
  mServer->addReductor( L"<Dir> ::= Id = <Expr>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addEqu( std::make_shared<Equ>( r.pos(), r.move<std::wstring>( 0 ), r.move<Value>( 2 ) ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Dir> ::= stack <Stack List>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addStackDeclaration( std::make_shared<StackDeclaration>( r.pos(), r.move< StackDeclaration::DataType >( 1 ) ) ); 
    return GE5::any();
  } );

  mServer->addReductor( L"<Attr> ::= <Attr> , <NSValue>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< Value > values( r.move<std::vector< Value >>( 0 ) );
    values.push_back( r.move<Value>( 2 ) );
    return values;
  } );

  mServer->addReductor( L"<Attr> ::= <NSValue>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< Value > values;
    values.push_back( r.move<Value>( 0 ) );
    return values;
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage> ::= <NSName> <Usage Off> <Usage As>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Usage>( r.pos(), Usage::A_IMPLICIT, r.move< std::vector< std::wstring > >( 0 ),
      r.move<Usage::Placement>( 1 ), r.move<std::wstring>( 2 ) );
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage> ::= < <NSName> <Usage Off> <Usage As>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Usage>( r.pos(), Usage::A_DIRECT, r.move< std::vector< std::wstring > >( 1 ),
      r.move<Usage::Placement>( 2 ), r.move<std::wstring>( 3 ) );
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage> ::= > <NSName> <Usage Off> <Usage As>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Usage>( r.pos(), Usage::A_ABSOLUTE, r.move< std::vector< std::wstring > >( 1 ),
      r.move<Usage::Placement>( 2 ), r.move<std::wstring>( 3 ) );
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage> ::= ^ <NSName> <Usage Off> <Usage As>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Usage>( r.pos(), Usage::A_LONG, r.move< std::vector< std::wstring > >( 1 ),
      r.move<Usage::Placement>( 2 ), r.move<std::wstring>( 3 ) );
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage Shift> ::= <NSName> += <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<UsageShift>( r.pos(), r.move< std::vector< std::wstring > >( 1 ),
      UsageShift::POSITIVE, r.move<Value>( 2 ) );
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<Usage Shift> ::= <NSName> -= <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<UsageShift>( r.pos(), r.move< std::vector< std::wstring > >( 1 ),
      UsageShift::NEGATIVE, r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Usage Off> ::= + <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Usage::Placement( Usage::Placement::POSITIVE_OFFSET, r.move<Value>( 1 ) );
  } );

  mServer->addReductor( L"<Usage Off> ::= - <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Usage::Placement( Usage::Placement::NEGATIVE_OFFSET, r.move<Value>( 1 ) );
  } );

  mServer->addReductor( L"<Usage Off> ::= = <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Usage::Placement( Usage::Placement::ANCHOR, r.move<Value>( 1 ) );
  } );

  mServer->addReductor( L"<Usage Off> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Usage::Placement();
  } );

  mServer->addReductor( L"<Usage As> ::= as Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<std::wstring>( 1 );
  } );

  mServer->addReductor( L"<Usage As> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::wstring();
  } );

  mServer->addReductor( L"<Reg Size> ::= Reg = DecLiteral , Reg = DecLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    auto result = std::make_pair( 0, 0 );

    std::wstring reg1 = r.move< std::wstring >( 0 );
    std::wstring reg2 = r.move< std::wstring >( 4 );
    wchar_t idx1 = std::toupper( reg1[0], std::locale::classic() );
    wchar_t idx2 = std::toupper( reg2[0], std::locale::classic() );

    if ( idx1 == 'S' || idx2 == 'S' )
      throw SemanticException( r.pos(), L"You can specify register size only for A (accumulator) or for X/Y (index) registers" );


    if ( idx1 == 'A' )
    {
      result.first = parseDec( r.move<std::wstring>( 2 ) );
      if ( result.first != 8 && result.first != 16 )
      {
        throw SemanticException( r.pos( 2 ), L"The valid accumulator sizes are 8 or 16 bits" );
      }

      if ( idx2 == 'X' || idx2 == 'Y' )
      {
        result.second = parseDec( r.move<std::wstring>( 6 ) );
        if ( result.second != 8 && result.second != 16 )
        {
          throw SemanticException( r.pos( 6 ), L"The valid index registers sizes are 8 or 16 bits" );
        }
      }
      else
      {
        throw SemanticException( r.pos( 4 ), L"Invalid register for size specification. Here could be only X/Y" );
      }
    }
    else
    {
      assert( idx1 == 'X' || idx1 == 'Y' );

      result.second = parseDec( r.move<std::wstring>( 2 ) );
      if ( result.second != 8 && result.second != 16 )
      {
        throw SemanticException( r.pos( 2 ), L"The valid index registers sizes are 8 or 16 bits" );
      }

      if ( idx2 == 'A' )
      {
        result.first = parseDec( r.move<std::wstring>( 6 ) );
        if ( result.first != 8 && result.first != 16 )
        {
          throw SemanticException( r.pos( 6 ), L"The valid accumulator sizes are 8 or 16 bits" );
        }
      }
      else
      {
        throw SemanticException( r.pos( 4 ), L"Invalid register for size specification. Here could be only A" );
      }
    }

    return result;
  } );

  mServer->addReductor( L"<Reg Size> ::= Reg = DecLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    auto result = std::make_pair( 0, 0 );

    std::wstring reg = r.move< std::wstring >( 0 );
    wchar_t idx = std::toupper( reg[0], std::locale::classic() );

    if ( idx == 'S' )
      throw SemanticException( r.pos(), L"You can specify register size only for A (accumulator) or for X/Y (index) registers" );

    if ( idx == 'A' )
    {
      result.first = parseDec( r.move<std::wstring>( 2 ) );
      if ( result.first != 8 && result.first != 16 )
      {
        throw SemanticException( r.pos( 2 ), L"The valid accumulator sizes are 8 or 16 bits" );
      }
    }
    else
    {
      assert( idx == 'X' || idx == 'Y' );

      result.second = parseDec( r.move<std::wstring>( 2 ) );
      if ( result.second != 8 && result.second != 16 )
      {
        throw SemanticException( r.pos( 2 ), L"The valid index registers sizes are 8 or 16 bits" );
      }
    }

    return result;
  } );

  mServer->addReductor( L"<Reg Size> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_pair( 0, 0 );
  } );

  // std::vector< std::wstring > NSName
  // std::wstring Id
  mServer->addReductor( L"<NSName> ::= <NSName> . Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v = r.move< std::vector< std::wstring > >( 0 );
    v.push_back( r.move< std::wstring >( 2 ) );
    return v;
  } );

  // std::wstring Id 
  mServer->addReductor( L"<NSName> ::= Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v;
    v.push_back( r.move<std::wstring>( 0 ) );
    return v;
  } );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<NSName Opt> ::= <NSName>", forward );

  // std::vector< std::wstring > NSName
  mServer->addReductor( L"<NSName Opt> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::vector< std::wstring >();
  } );

  mServer->addReductor( L"<Id Opt> ::= Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<std::wstring>( 0 );
  } );

  mServer->addReductor( L"<Id Opt> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::wstring();
  } );

  mServer->addReductor( L"<Gen> ::= <Cmd>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addCommand( r.move<std::shared_ptr<Command>>( 0 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Gen> ::= <Label> <Cmd>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addLabel( r.move<std::shared_ptr<Label>>( 0 ) );
    module( r.id() ).addCommand( r.move<std::shared_ptr<Command>>( 1 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Gen> ::= <Label> : <Cmd>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addLabel( r.move<std::shared_ptr<Label>>( 0 ) );
    module( r.id() ).addCommand( r.move<std::shared_ptr<Command>>( 2 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Gen> ::= <Label> :", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    module( r.id() ).addLabel( r.move<std::shared_ptr<Label>>( 0 ) );
    return GE5::any();
  } );

  mServer->addReductor( L"<Label> ::= Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Label>( r.pos(), r.move<std::wstring>( 0 ) );
  } );

  mServer->addReductor( L"<Label> ::= @", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::make_shared<Label>( r.pos() );
  } );

  mServer->addReductor( L"<Cmd> ::= <Mnemonic> <Addressing Mode>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    cmd::Mnemonic m = r.move<cmd::Mnemonic>( 0 );
    return r.move<std::shared_ptr<Addressing>>( 1 )->applyAddressing( r.pos(), m );
  } );

  mServer->addReductor( L"<Cmd> ::= <Data Size> <Expr List>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    int size = r.move<int>( 0 );
    auto dt = size == 1 ? Command::BYTE : ( size == 2 ?  Command::WORD : Command::LONG );
    return Command::createDataDeclaration( r.pos(), dt, r.move< std::vector< Value > >( 1 ) );
  } );

  mServer->addReductor( L"<Cmd> ::= data <Expr List>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Command::createDataDeclaration( r.pos(), Command::VARIABLE, r.move< std::vector< Value > >( 1 ) );
  } );

  mServer->addReductor( L"<Cmd> ::= org <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );
    return Command::createOrg( r.pos(), std::move( v ) );
  } );

  mServer->addReductor( L"<Cmd> ::= ds <Expr>", [this]( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );
    return Command::createSpaceDeclaration( r.pos(), std::move( v ) );
  } );

  mServer->addReductor( L"<Data Size> ::= byte", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return 1;
  } );

  mServer->addReductor( L"<Data Size> ::= word", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return 2;
  } );

  mServer->addReductor( L"<Data Size> ::= long", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return 3;
  } );

  mServer->addReductor( L"<Stack List> ::= <Stack List> , <Stack Data>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    auto vec( r.move< StackDeclaration::DataType >( 0 ) );
    vec.push_back( r.move< StackDeclaration::ElemType >( 2 ) );
    return vec;
  } );

  mServer->addReductor( L"<Stack List> ::= <Stack Data>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    StackDeclaration::DataType vec;
    vec.push_back( r.move< StackDeclaration::ElemType >( 0 ) );
    return vec;
  } );

  mServer->addReductor( L"<Stack Data> ::= <Data Size> Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    int size = r.move<int>( 0 );
    StackDeclaration::StackElem se = size == 1 ? StackDeclaration::BYTE : ( size == 2 ? StackDeclaration::WORD : StackDeclaration::LONG );
    return StackDeclaration::ElemType( se, r.move<std::wstring>( 1 ) );
  } );

  mServer->addReductor( L"<Stack Data> ::= Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    auto id = r.move<std::wstring>( 0 );
    std::transform( id.begin(), id.end(), id.begin(), tolower );
    if ( id == L"pop" )
      return StackDeclaration::ElemType( StackDeclaration::POP, std::wstring() );
    else
      throw SemanticException( r.pos(), L"Expecting stack variable specifier or 'pop' to declare element removal" );
  } );

  mServer->addReductor( L"<Expr List> ::= <Expr List> , <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< Value > vec( r.move< std::vector< Value > >( 0 ) );
    vec.push_back( r.move<Value>( 2 ) );
    return vec;
  } );

  mServer->addReductor( L"<Expr List> ::= <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< Value > vec;
    vec.push_back( r.move<Value>( 0 ) );
    return vec;
  } );

  mServer->addReductor( L"<Mnemonic> ::= LDA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::LDA; } );
  mServer->addReductor( L"<Mnemonic> ::= LDX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::LDX; } );
  mServer->addReductor( L"<Mnemonic> ::= LDY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::LDY; } );
  mServer->addReductor( L"<Mnemonic> ::= STA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::STA; } );
  mServer->addReductor( L"<Mnemonic> ::= STX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::STX; } );
  mServer->addReductor( L"<Mnemonic> ::= STY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::STY; } );
  mServer->addReductor( L"<Mnemonic> ::= ADC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::ADC; } );
  mServer->addReductor( L"<Mnemonic> ::= AND", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::AND; } );
  mServer->addReductor( L"<Mnemonic> ::= ASL", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::ASL; } );
  mServer->addReductor( L"<Mnemonic> ::= SBC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::SBC; } );
  mServer->addReductor( L"<Mnemonic> ::= JSR", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::JSR; } );
  mServer->addReductor( L"<Mnemonic> ::= JMP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::JMP; } );
  mServer->addReductor( L"<Mnemonic> ::= LSR", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::LSR; } );
  mServer->addReductor( L"<Mnemonic> ::= ORA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::ORA; } );
  mServer->addReductor( L"<Mnemonic> ::= CMP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CMP; } );
  mServer->addReductor( L"<Mnemonic> ::= CPY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CPY; } );
  mServer->addReductor( L"<Mnemonic> ::= CPX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CPX; } );
  mServer->addReductor( L"<Mnemonic> ::= DEC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::DEC; } );
  mServer->addReductor( L"<Mnemonic> ::= INC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::INC; } );
  mServer->addReductor( L"<Mnemonic> ::= EOR", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::EOR; } );
  mServer->addReductor( L"<Mnemonic> ::= ROL", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::ROL; } );
  mServer->addReductor( L"<Mnemonic> ::= ROR", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::ROR; } );
  mServer->addReductor( L"<Mnemonic> ::= BRK", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BRK; } );
  mServer->addReductor( L"<Mnemonic> ::= CLC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CLC; } );
  mServer->addReductor( L"<Mnemonic> ::= CLI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CLI; } );
  mServer->addReductor( L"<Mnemonic> ::= CLV", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CLV; } );
  mServer->addReductor( L"<Mnemonic> ::= CLD", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::CLD; } );
  mServer->addReductor( L"<Mnemonic> ::= PHP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHP; } );
  mServer->addReductor( L"<Mnemonic> ::= PLP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLP; } );
  mServer->addReductor( L"<Mnemonic> ::= PHA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHA; } );
  mServer->addReductor( L"<Mnemonic> ::= PLA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLA; } );
  mServer->addReductor( L"<Mnemonic> ::= RTI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::RTI; } );
  mServer->addReductor( L"<Mnemonic> ::= RTS", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::RTS; } );
  mServer->addReductor( L"<Mnemonic> ::= SEC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::SEC; } );
  mServer->addReductor( L"<Mnemonic> ::= SEI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::SEI; } );
  mServer->addReductor( L"<Mnemonic> ::= SED", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::SED; } );
  mServer->addReductor( L"<Mnemonic> ::= INY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::INY; } );
  mServer->addReductor( L"<Mnemonic> ::= INX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::INX; } );
  mServer->addReductor( L"<Mnemonic> ::= DEY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::DEY; } );
  mServer->addReductor( L"<Mnemonic> ::= DEX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::DEX; } );
  mServer->addReductor( L"<Mnemonic> ::= TXA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TXA; } );
  mServer->addReductor( L"<Mnemonic> ::= TYA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TYA; } );
  mServer->addReductor( L"<Mnemonic> ::= TXS", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TXS; } );
  mServer->addReductor( L"<Mnemonic> ::= TAY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TAY; } );
  mServer->addReductor( L"<Mnemonic> ::= TAX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TAX; } );
  mServer->addReductor( L"<Mnemonic> ::= TSX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TSX; } );
  mServer->addReductor( L"<Mnemonic> ::= NOP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::NOP; } );
  mServer->addReductor( L"<Mnemonic> ::= BPL", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BPL; } );
  mServer->addReductor( L"<Mnemonic> ::= BMI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BMI; } );
  mServer->addReductor( L"<Mnemonic> ::= BNE", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BNE; } );
  mServer->addReductor( L"<Mnemonic> ::= BCC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BCC; } );
  mServer->addReductor( L"<Mnemonic> ::= BCS", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BCS; } );
  mServer->addReductor( L"<Mnemonic> ::= BEQ", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BEQ; } );
  mServer->addReductor( L"<Mnemonic> ::= BVC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BVC; } );
  mServer->addReductor( L"<Mnemonic> ::= BVS", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BVS; } );
  mServer->addReductor( L"<Mnemonic> ::= BIT", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BIT; } );
  mServer->addReductor( L"<Mnemonic> ::= STZ", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::STZ; } );
  mServer->addReductor( L"<Mnemonic> ::= SEP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::SEP; } );
  mServer->addReductor( L"<Mnemonic> ::= REP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::REP; } );
  mServer->addReductor( L"<Mnemonic> ::= TRB", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TRB; } );
  mServer->addReductor( L"<Mnemonic> ::= TSB", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TSB; } );
  mServer->addReductor( L"<Mnemonic> ::= BRA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BRA; } );
  mServer->addReductor( L"<Mnemonic> ::= BRL", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::BRL; } );
  mServer->addReductor( L"<Mnemonic> ::= COP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::COP; } );
  mServer->addReductor( L"<Mnemonic> ::= MVN", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::MVN; } );
  mServer->addReductor( L"<Mnemonic> ::= MVP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::MVP; } );
  mServer->addReductor( L"<Mnemonic> ::= PEA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PEA; } );
  mServer->addReductor( L"<Mnemonic> ::= PEI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PEI; } );
  mServer->addReductor( L"<Mnemonic> ::= PER", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PER; } );
  mServer->addReductor( L"<Mnemonic> ::= PHB", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHB; } );
  mServer->addReductor( L"<Mnemonic> ::= PHD", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHD; } );
  mServer->addReductor( L"<Mnemonic> ::= PHK", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHK; } );
  mServer->addReductor( L"<Mnemonic> ::= PHX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHX; } );
  mServer->addReductor( L"<Mnemonic> ::= PHY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PHY; } );
  mServer->addReductor( L"<Mnemonic> ::= PLB", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLB; } );
  mServer->addReductor( L"<Mnemonic> ::= PLD", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLD; } );
  mServer->addReductor( L"<Mnemonic> ::= PLX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLX; } );
  mServer->addReductor( L"<Mnemonic> ::= PLY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::PLY; } );
  mServer->addReductor( L"<Mnemonic> ::= RTL", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::RTL; } );
  mServer->addReductor( L"<Mnemonic> ::= STP", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::STP; } );
  mServer->addReductor( L"<Mnemonic> ::= TCD", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TCD; } );
  mServer->addReductor( L"<Mnemonic> ::= TCS", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TCS; } );
  mServer->addReductor( L"<Mnemonic> ::= TDC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TDC; } );
  mServer->addReductor( L"<Mnemonic> ::= TSC", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TSC; } );
  mServer->addReductor( L"<Mnemonic> ::= TXY", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TXY; } );
  mServer->addReductor( L"<Mnemonic> ::= TYX", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::TYX; } );
  mServer->addReductor( L"<Mnemonic> ::= WAI", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::WAI; } );
  mServer->addReductor( L"<Mnemonic> ::= WDM", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::WDM; } );
  mServer->addReductor( L"<Mnemonic> ::= XBA", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::XBA; } );
  mServer->addReductor( L"<Mnemonic> ::= XCE", []( GE5::ReductorArg & r ) -> GE5::any { return cmd::XCE; } );

  mServer->addReductor( L"<Addressing Mode> ::= <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 0 );

    if ( v.parenthesized() )
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingIndirect>( std::move( v ) ) );
    else
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingAbsolute>( std::move( v ) ) );

  } );

  mServer->addReductor( L"<Addressing Mode> ::= <Expr> , Reg", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 0 );
    std::wstring reg = r.move< std::wstring >( 2 );
    wchar_t idx = std::toupper( reg[0], std::locale::classic() ) ;

    if ( v.parenthesized() )
    {
      if ( idx != 'Y' )
        throw SemanticException( r.pos( 2 ), L"There is no addressing mode indirect indexed with register " + reg );
      else
        return std::static_pointer_cast<Addressing>( std::make_shared<AddressingDirectIndirectIndexed>( std::move( v ) ) );
    }

    switch ( idx )
    {
    case 'X':
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingIndexed>( std::move( v ), Addressing::X ) );
    case 'Y':
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingIndexed>( std::move( v ), Addressing::Y ) );
    case 'S':
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingIndexed>( std::move( v ), Addressing::S ) );
    default:
      assert( false );
    }

    return GE5::any();
  } );

  mServer->addReductor( L"<Addressing Mode> ::= <Expr> , <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value s = r.move<Value>( 0 );
    Value d = r.move<Value>( 2 );

    if ( s.parenthesized() || d.parenthesized())
    {
      Logger::warn( r.pos(), L"Redundant parentheses in move addressing mode" );
    }
    return std::static_pointer_cast<Addressing>( std::make_shared<AddressingMove>( std::move( s ), std::move( d ) ) );
  } );

  mServer->addReductor( L"<Addressing Mode> ::= ( <Expr> , Reg )", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );

    if ( v.parenthesized() )
    {
      Logger::warn( r.pos(), L"Redundant parentheses in indexed indirect addressing mode" );
    }

    std::wstring reg = r.move< std::wstring >( 3 );

    wchar_t idx = std::toupper( reg[0], std::locale::classic() ) ;

    if ( idx != 'X' )
      throw SemanticException( r.pos( 3 ), L"There is no addressing mode indexed indirect with register " + reg );
    else
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingIndexedIndirect>( std::move( v ) ) );

  } );

  mServer->addReductor( L"<Addressing Mode> ::= [ <Expr> ]", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );
    if ( v.parenthesized() )
    {
      Logger::warn( r.pos(), L"Redundant parentheses in direct indirect long addressing mode" );
    }

    return std::static_pointer_cast<Addressing>( std::make_shared<AddressingDirectIndirectLong>( std::move( v ) ) );
  } );

  mServer->addReductor( L"<Addressing Mode> ::= [ <Expr> ] , Reg", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );

    if ( v.parenthesized() )
    {
      Logger::warn( r.pos(), L"Redundant parentheses in direct indirect long indexed addressing mode" );
    }

    std::wstring reg = r.move< std::wstring >( 4 );
    wchar_t idx = std::toupper( reg[0], std::locale::classic() ) ;

    if ( idx != 'Y' )
    {
      throw SemanticException( r.pos( 4 ), L"There is no addressing mode direct indirect long indexed with register "
          + reg );
    }
    else
      return std::static_pointer_cast<Addressing>( std::make_shared<AddressingDirectIndirectLongIndexed>( std::move( v ) ) );
  } );

  mServer->addReductor( L"<Addressing Mode> ::= ( <Expr> , Reg ) , Reg", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );

    if ( v.parenthesized() )
    {
      Logger::warn( r.pos(), L"Redundant parentheses in stack relative indirect indexed addressing mode" );
    }

    std::wstring reg1 = r.move< std::wstring >( 3 );
    std::wstring reg2 = r.move< std::wstring >( 6 );

    wchar_t idx1 = std::toupper( reg1[0], std::locale::classic() ) ;
    wchar_t idx2 = std::toupper( reg2[0], std::locale::classic() ) ;

    if ( idx1 != 'S' )
      throw SemanticException( r.pos( 3 ), L"Preindexing in stack relative indirect indexed addressing mode must be done with S register" );

    if ( idx2 != 'Y' )
      throw SemanticException( r.pos( 6 ), L"Postindexing in stack relative indirect indexed addressing mode must be done with Y register" );

    return std::static_pointer_cast<Addressing>( std::make_shared<AddressingStackRelativeIndirectIndexed>( std::move( v ) ) );
  } );

  mServer->addReductor( L"<Addressing Mode> ::= # <Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    Value v = r.move<Value>( 1 );

    if ( v.parenthesized() )
    {
      Logger::warn( r.pos(), L"Redundant parentheses in immediate addressing mode" );
    }

    return std::static_pointer_cast<Addressing>( std::make_shared<AddressingImmediate>( std::move( v ) ) );
  } );

  mServer->addReductor( L"<Addressing Mode> ::= <>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return std::static_pointer_cast<Addressing>( std::make_shared<AddressingImplied>() );
  } );

  mServer->addReductor( L"<Expr> ::= < <Spec Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<Value>( 1 ).direct();
  } );

  mServer->addReductor( L"<Expr> ::= > <Spec Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<Value>( 1 ).absolute();
  } );

  mServer->addReductor( L"<Expr> ::= ^ <Spec Expr>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<Value>( 1 ).longen();
  } );

  mServer->addReductor( L"<Expr> ::= <Spec Expr>", forward );

  mServer->addReductor( L"<Spec Expr> ::= <Op Or>", forward );

  mServer->addReductor( L"<Op Or> ::= <Op Or> || <Op And>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_LOGICAL_OR, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Or> ::= <Op And>", forward );

  mServer->addReductor( L"<Op And> ::= <Op And> && <Op BinOR>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_LOGICAL_AND, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op And> ::= <Op BinOR>", forward );

  mServer->addReductor( L"<Op BinOR> ::= <Op BinOR> | <Op BinXOR>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_BINARY_OR, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op BinOR> ::= <Op BinXOR>", forward );

  mServer->addReductor( L"<Op BinXOR> ::= <Op BinXOR> ^ <Op BinAND>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_BINARY_XOR, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op BinXOR> ::= <Op BinAND>", forward );

  mServer->addReductor( L"<Op BinAND> ::= <Op BinAND> & <Op Equate>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_BINARY_AND, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op BinAND> ::= <Op Equate>", forward );

  mServer->addReductor( L"<Op Equate> ::= <Op Equate> == <Op Compare>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_EQ, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Equate> ::= <Op Equate> != <Op Compare>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_NE, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Equate> ::= <Op Compare>", forward );

  mServer->addReductor( L"<Op Compare> ::= <Op Compare> < <Op Shift>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_L, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Compare> ::= <Op Compare> > <Op Shift>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_G, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Compare> ::= <Op Compare> <= <Op Shift>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_LE, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Compare> ::= <Op Compare> >= <Op Shift>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_GE, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Compare> ::= <Op Shift>", forward );

  mServer->addReductor( L"<Op Shift> ::= <Op Shift> << <Op Add>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_SHL, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Shift> ::= <Op Shift> >> <Op Add>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_SHR, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Shift> ::= <Op Add>", forward );

  mServer->addReductor( L"<Op Add> ::= <Op Add> + <Op Mult>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_PLUS, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Add> ::= <Op Add> - <Op Mult>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_MINUS, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Add> ::= <Op Mult>", forward );

  mServer->addReductor( L"<Op Mult> ::= <Op Mult> * <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_MULTIPLY, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Mult> ::= <Op Mult> / <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_DIVIDE, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Mult> ::= <Op Mult> % <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opBinary( r.pos(), value::OP_MODULO, r.move<Value>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Op Mult> ::= <Op Unary>", forward );

  mServer->addReductor( L"<Op Unary> ::= ! <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opUnary( r.pos(), value::OP_LOGIC_NEGATE, r.move<Value>( 1 ) );
  } );

  mServer->addReductor( L"<Op Unary> ::= ~ <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opUnary( r.pos(), value::OP_BINARY_NEGATE, r.move<Value>( 1 ) );
  } );
  mServer->addReductor( L"<Op Unary> ::= - <Op Unary>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::opUnary( r.pos(), value::OP_ARITHMETIC_NEGATE, r.move<Value>( 1 ) );
  } );

  mServer->addReductor( L"<Op Unary> ::= <NSValue>", forward );
  mServer->addReductor( L"<Op Unary> ::= <LitValue>", forward );

  mServer->addReductor( L"<NSValue> ::= <NSName> . <Fun>", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > v = r.move< std::vector< std::wstring > >( 0 );
    return r.move<Value>( 2 ).prependName( v );
  } );

  mServer->addReductor( L"<NSValue> ::= <NSName> . Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > name = r.move< std::vector< std::wstring > >( 0 );
    name.push_back( r.move<std::wstring>( 2 ) );
    return Value::id( r.pos(), std::move( name ) );
  } );

  mServer->addReductor( L"<NSValue> ::= <Fun>", forward );

  mServer->addReductor( L"<NSValue> ::= Id", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::vector< std::wstring > name;
    name.push_back( r.move<std::wstring>( 0 ) );
    return Value::id( r.pos(), std::move( name ) );
  } );

  mServer->addReductor( L"<Fun> ::= Id ( <Expr> )", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::fun( r.pos(), r.move<std::wstring>( 0 ), r.move<Value>( 2 ) );
  } );

  mServer->addReductor( L"<Fun> ::= Id ( )", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::fun( r.pos(), r.move<std::wstring>( 0 ) );
  } );

  mServer->addReductor( L"<LitValue> ::= BinLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::wstring lit = r.move<std::wstring>( 0 );

    int x = 0;
    for ( size_t i = 1; i < lit.size(); ++i )
    {
      x <<= 1;
      x |= lit[i] - L'0';
    }

    return Value::intLiteral( r.pos(), x );
  } );

  // std::wstring HexLiteral 
  mServer->addReductor( L"<LitValue> ::= HexLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    std::wstring lit = r.move<std::wstring>( 0 );

    unsigned int x;
    std::wstringstream ss;
    ss << std::hex << ( lit.c_str() + 1 );
    ss >> x;
    return Value::intLiteral( r.pos(), x );
  } );

  mServer->addReductor( L"<LitValue> ::= DecLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    int x = parseDec( r.move<std::wstring>( 0 ) );
    return Value::intLiteral( r.pos(), x );
  } );

  mServer->addReductor( L"<LitValue> ::= InternalString", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::string( r.pos(), r.move<std::wstring>( 0 ), value::CT_INTERNAL );
  } );

  mServer->addReductor( L"<LitValue> ::= AtasciString", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::string( r.pos(), r.move<std::wstring>( 0 ), value::CT_ATASCII );
  } );

  mServer->addReductor( L"<LitValue> ::= ( <Expr> )", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return r.move<Value>( 1 ).parenthesize();
  } );

  mServer->addReductor( L"<LitValue> ::= { <Mnemonic> <Addressing Mode> }", []( GE5::ReductorArg & r ) -> GE5::any
  {
    cmd::Mnemonic m = r.move<cmd::Mnemonic>( 1 );
    auto cmd = r.move<std::shared_ptr<Addressing>>( 2 )->applyAddressing( r.pos(), m );

    return Value::opcode( r.pos(), cmd );
  } );

  mServer->addReductor( L"<LitValue> ::= *", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::pc( r.pos() );
  } );

  mServer->addReductor( L"<LitValue> ::= @ + DecLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    int skip = parseDec( r.move<std::wstring>( 2 ) );
    return Value::anonymousLabel( r.pos(), true, skip );
  } );

  mServer->addReductor( L"<LitValue> ::= @ +", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::anonymousLabel( r.pos(), true, 0 ); 
  } );

  mServer->addReductor( L"<LitValue> ::= @ - DecLiteral", []( GE5::ReductorArg & r ) -> GE5::any
  {
    int skip = parseDec( r.move<std::wstring>( 2 ) );
    return Value::anonymousLabel( r.pos(), false, skip );
  } );

  mServer->addReductor( L"<LitValue> ::= @ -", []( GE5::ReductorArg & r ) -> GE5::any
  {
    return Value::anonymousLabel( r.pos(), false, 0 );
  } );

}

} //namespace las

