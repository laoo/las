#pragma once

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <tuple>
#include <vector>
#include <filesystem>
#include <optional>

#include <boost/lexical_cast.hpp>
#include <boost/locale.hpp>
#include <boost/format.hpp>
#include <boost/logic/tribool.hpp>
