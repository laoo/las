#include "stdafx.h"
#include "UsageShift.hpp"
#include "Exception.hpp"

namespace las
{


UsageShift::UsageShift( GE5::Position const& position, std::vector< std::wstring > name, Direction dir, Value && offset ) :
  SegmentContent( position ),
  mName( std::move( name ) ),
  mDir( dir ),
  mOffset( std::move( offset ) )
{
}

UsageShift::~UsageShift()
{
}

void UsageShift::gather( GatherSession & session ) const
{
  throw NYIException( position(), L"Gather" );
}

void UsageShift::emit( EmitSession & session ) const
{
  throw NYIException( position(), L"Emit" );
}

} //namespace las
