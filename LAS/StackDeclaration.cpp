#include "stdafx.h"
#include "StackDeclaration.hpp"
#include "Exception.hpp"

namespace las
{

StackDeclaration::StackDeclaration( GE5::Position const& position, DataType data ) :
  SegmentContent( position ),
  mData( std::move( data ) )
{
}

StackDeclaration::~StackDeclaration()
{
}

void StackDeclaration::gather( GatherSession & session ) const
{
  throw NYIException( position(), L"Gather" );
}

void StackDeclaration::emit( EmitSession & session ) const
{
  throw NYIException( position(), L"Emit" );
}


} //namespace las

