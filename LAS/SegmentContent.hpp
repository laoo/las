#ifndef LAS_SEGMENTCONTENT_HPP
#define LAS_SEGMENTCONTENT_HPP

#include "PositionHolder.hpp"

namespace las
{
class GatherSession;
class EmitSession;

class SegmentContent : public PositionHolder
{
public:
  SegmentContent( GE5::Position const& position );

  virtual ~SegmentContent();

  virtual wchar_t const* getName();

  virtual void gather( GatherSession & session ) const = 0;
  virtual void emit( EmitSession & session ) const = 0;

};

} //namespace las

#endif //LAS_SEGMENTCONTENT_HPP
