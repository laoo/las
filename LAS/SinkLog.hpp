#ifndef LAS_SINKLOG_HPP
#define LAS_SINKLOG_HPP

#include "Sink.hpp"

namespace GE5
{
class ISourceProxy;
}

namespace las
{

class SinkLog : public Sink
{
public:
  SinkLog();
  virtual ~SinkLog();

  virtual void emit( GE5::Position const& pos, int address, unsigned char op );
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned char data );
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned short data );
  virtual void emit( GE5::Position const& pos, int address, unsigned char op, unsigned int data );

private:

private:
  void emit( GE5::Position const& pos, int address, unsigned char * begin, unsigned char * end );
  int fillUntil( int until );

private:
  std::filesystem::path mPath;
  std::wofstream mFout;

  std::shared_ptr<GE5::ISourceProxy> mSrcProxy;
  int mLineNumberDigits;
  int mCurrentLine;

  static const int mWorkSize;
  static const int mLinesAround;
};

} //namespace las

#endif //LAS_SINKLOG_HPP
