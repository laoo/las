#include "stdafx.h"
#include "Namespace.hpp"
#include "Exception.hpp"
#include "Segment.hpp"
#include "Command.hpp"
#include "Label.hpp"
#include "Usage.hpp"
#include "UsageShift.hpp"
#include "CPUMode.hpp"
#include "Equ.hpp"
#include "cmd/Org.hpp"
#include "ge5/include/Position.hpp"

namespace las
{

Namespace::Namespace( std::wstring aName ) :
  mName( std::move( aName ) ),
  mFullName()
{
}

std::wstring const& Namespace::getName() const
{
  return mName;
}

std::wstring Namespace::fullName() const
{
  if ( mFullName.empty() )
    return L"global namespace";
  else
    return std::wstring( L"namespace " ) + mFullName;
}



std::shared_ptr<Namespace> Namespace::addNamespace( GE5::Position const& position, std::wstring aName )
{
  if ( mOpenSegment )
    throw SemanticException( position, L"Cannot open a namespace " + aName + L" inside a segment " + mOpenSegment->getName() );

  if ( mChildSegments.find( aName ) != mChildSegments.cend() )
  {
    throw SemanticException( position, L"Cannot open a namespace " + aName + L" inside a " + fullName() + L" because there is already a segment with that name opened at "
      + mChildSegments[ aName ]->position().wstring() );
  }

  if ( mEquates.find( aName ) != mEquates.cend() )
  {
    throw SemanticException( position, L"Cannot open a namespace " + aName + L" inside a " + fullName() + L" because there is already an equate with that name declared at "
      + mEquates[ aName ]->position().wstring() );
  }

  auto it = mChildNamespaces.find( aName );

  if ( it == mChildNamespaces.end() )
  {
    it = mChildNamespaces.insert( std::make_pair( aName, std::make_shared<Namespace>( aName ) ) ).first;

    it->second->mFullName = mFullName + aName;
  }

  return it->second;
}


void Namespace::openSegment( GE5::Position const& position, std::wstring aName )
{
  if ( mOpenSegment )
    throw SemanticException( position, L"Cannot nest segment " + aName + L" inside a segment " + mOpenSegment->getName() );

  if ( mChildNamespaces.find( aName ) != mChildNamespaces.cend() )
  {
    throw SemanticException( position, L"Cannot open a segment " + aName + L" inside a " + fullName() + L" because there is already a namespace with that name" );
  }

  if ( mEquates.find( aName ) != mEquates.cend() )
  {
    throw SemanticException( position, L"Cannot open a segment " + aName + L" inside a " + fullName() + L" because there is already an equate with that name declared at "
      + mEquates[ aName ]->position().wstring() );
  }

  assertUniqueSegment( position, aName );

  mOpenSegment = std::make_shared<Segment>( position, aName );
  mChildSegments.insert( std::make_pair( aName, mOpenSegment ) );
}

bool Namespace::hasMainSegment() const
{
  return mChildSegments.find( L"main" ) != mChildSegments.cend();
}


bool Namespace::closeSegment( GE5::Position const& position, std::wstring const& aName )
{
  if ( mOpenSegment )
  {
    if ( !aName.empty() && mOpenSegment->getName() != aName )
    {
      throw SemanticException( position, L"Current segment name is " + mOpenSegment->getName() + L" but closing it as " + aName );
    }

    if ( !mOpenSegment->waitingLabels().empty() )
    {
      throw SemanticException( mOpenSegment->waitingLabels()[0]->position(), L"Can't place label at the end of segment. Use origin() + size() instead" );
    }

    mOpenSegment.reset();
    return true;
  }
  else
  {
    return false;
  }
}


void Namespace::addEqu( std::shared_ptr<Equ> equ )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addEqu( std::move( equ ) );
  }
  else
  {

    if ( mChildNamespaces.find( equ->name() ) != mChildNamespaces.cend() )
    {
      throw SemanticException( equ->position(), L"Cannot add an equate " + equ->name() + L" inside a " + fullName() +
        L" because there is already a namespace with that name" );
    }

    if ( mChildSegments.find( equ->name() ) != mChildSegments.cend() )
    {
      throw SemanticException( equ->position(), L"Cannot add an equate " + equ->name() + L" inside a " + fullName() +
        L" because there is already a segment with that name opened at " + mChildSegments[  equ->name() ]->position().wstring() );
    }

    auto p = mEquates.insert( std::make_pair( equ->name(), equ ) );
    if ( !p.second )
    {
      std::wstring fn = fullName();
      std::toupper( fn[0], std::locale::classic() );
      throw SemanticException( equ->position(), fn + L" already has an equate " + equ->name() );
    }
  }
}

void Namespace::addCommand( std::shared_ptr<Command> cmd )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addCommand( std::move( cmd ) );
  }
  else
  {
    // A special case for 'org'. It'll be done in more general way with constraints.
    if ( auto org = std::dynamic_pointer_cast<cmd::Org>( cmd ) )
    {
      if ( mOrg )
        throw SemanticException( cmd->position(), std::wstring( L"Namespace can only have one org (the other defined at " ) + mOrg->position().range() + L")" );

      mOrg = std::move( org );
    }
    else
    {
      throw SemanticException( cmd->position(), std::wstring( cmd->getName() ) + L" cannot be placed outside a segment" );
    }
  }

}

bool Namespace::hasOpenSegment() const
{
  return mOpenSegment != nullptr;
}

void Namespace::addLabel( std::shared_ptr<Label> label )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addLabel( std::move( label ) );
  }
  else
  {
    throw SemanticException( label->position(), L"Labels can be only added to segments" );
  }
}

void Namespace::addUsage( std::shared_ptr<Usage> usage )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addContent( std::move( usage ) );
  }
  else
  {
    auto it = std::find_if( mUsages.cbegin(), mUsages.cend(), [usage]( std::shared_ptr<Usage> p )
    {
      return p->namespacewiseEqual( *usage );
    } );

    if ( it != mUsages.cend() )
    {
      throw SemanticException( usage->position(), std::wstring( L"Equivalent usage is already present in namespace " ) +
        fullName() + L" at " + (*it)->position().wstring() );
    }

    mUsages.push_back( std::move( usage ) );
  }
}

void Namespace::shiftUsage( std::shared_ptr<UsageShift> usage )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addContent( std::move( usage ) );
  }
  else
  {
    throw SemanticException( usage->position(), L"Usage can be shifted only in segment" );
  }
}

void Namespace::addCPUMode( std::shared_ptr<CPUMode> cpuMode )
{
  if ( mOpenSegment )
  {
    mOpenSegment->addContent( std::move( cpuMode ) );
  }
  else
  {
    if ( mCPUMode )
      throw SemanticException( cpuMode->position(), std::wstring( L"CPU mode already declared at " ) + mCPUMode->position().range() );
    else
      mCPUMode = std::move( cpuMode );
  }
}

void Namespace::assertUniqueSegment( GE5::Position const& position, std::wstring const& name ) const
{
  auto it = mChildSegments.find( name );

  if ( it != mChildSegments.cend() )
  {
    std::wstring fn = fullName();
    std::toupper( fn[0], std::locale::classic() );
    throw SemanticException( position, std::wstring( L"Segment " ) + name + L" redefinition in namespace " + fn );
  }
}


void Namespace::merge( std::shared_ptr<Namespace> other )
{
  assert( fullName() == other->fullName() );

  if ( mOrg && other->mOrg )
    throw SemanticException( other->mOrg->position(), std::wstring( L"Origin of the namespace " ) + fullName() +
      L" set more than once. Previous set is at " + mOrg->position().wstring() );

  mOrg = other->mOrg;

  if ( mCPUMode && other->mCPUMode && *mCPUMode != *other->mCPUMode )
    throw SemanticException( other->mOrg->position(), std::wstring( L"Origin of the namespace " ) + fullName() +
      L" set more than once. Previous set is at " + mOrg->position().wstring() );

  mCPUMode = other->mCPUMode;

  for ( auto it = other->mUsages.cbegin(); it != other->mUsages.cend(); ++it )
    addUsage( *it );

  for ( auto it = other->mChildSegments.begin(); it != other->mChildSegments.end(); ++it )
  {
    std::shared_ptr<Segment> segment = it->second;
    assertUniqueSegment( segment->position(), it->first );
    mChildSegments.insert( std::make_pair( it->first, segment ) );
  }

  for ( auto it = other->mChildNamespaces.begin(); it != other->mChildNamespaces.end(); ++it )
  {
    auto jt = mChildNamespaces.find( it->first );
    if ( jt != mChildNamespaces.end() )
    {
      jt->second->merge( it->second );
    }
    else
    {
      mChildNamespaces.insert( std::make_pair( it->first, it->second ) );
    }
  }
}

} //namespace las

