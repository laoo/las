#ifndef LAS_EMPTYCALCCONTEXT_HPP
#define LAS_EMPTYCALCCONTEXT_HPP

#include "CalcContext.hpp"


namespace las
{

class EmptyCalcContext : public CalcContext
{
public:

  EmptyCalcContext();
  virtual ~EmptyCalcContext();

  virtual size_t currentSegmentOffset() const;
  virtual size_t anonymousLabelOffset( GE5::Position const& pos, bool forward, int count );
  virtual SegmentProxy * currentCodeSegment();
  virtual SegmentProxy * currentDataSegment();
  virtual value::Host functionValue( GE5::Position const& pos, std::vector<std::wstring> const& name,
    std::optional<value::Host> arg, value::UsageReq req );

  virtual value::Host calcIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec, value::UsageReq req );

  virtual void appendToEmitList( SegmentProxy * segmentProxyToAppend );

private:
  static const wchar_t mMessage[];

};

} //namespace las

#endif //LAS_EMPTYCALCCONTEXT_HPP
