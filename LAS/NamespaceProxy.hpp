#ifndef LAS_NAMESPACEPROXY_HPP
#define LAS_NAMESPACEPROXY_HPP

#include "CPUMode.hpp"
#include "value/Decoration.hpp"
#include "Value.hpp"

namespace las
{
class Namespace;
class SegmentProxy;
class Sink;
class MemoryAllocator;
class Usage;

class NamespaceProxy
{
public:

  struct UsageList
  {
    UsageList( Usage const& aUsage ) : usage( aUsage ), pPrevious( nullptr ) {}

    std::shared_ptr<SegmentProxy> segment;
    Usage const& usage;
    UsageList const* pPrevious;

    /**
     * Checks whether this UsageList node is valid in context of previous nodes.
     *
     * @param head A head of the list againts which to perform validation.
     * @return Colliding node if any (nullptr otherwise).
     */
    UsageList const* validate( UsageList const* head ) const;
  };

public:
  NamespaceProxy( std::shared_ptr<Namespace const> pNamespace, std::shared_ptr<Sink> pSink,
    std::shared_ptr<MemoryAllocator> pAllocator, NamespaceProxy * pParent = nullptr );
  virtual ~NamespaceProxy();

  void emitSegment( std::wstring const& name );

  std::optional< int > memSize() const;
  std::optional< int > idxSize() const;
  std::optional< CPUMode::Mode > cpuMode() const;

  void preallocate( SegmentProxy * segment, Value org, size_t size );
  int allocate( SegmentProxy const* segment );
  int allocate( GE5::Position const& position, size_t size );

  std::optional< int > bank();

  UsageList const* getUsageList() const;

  std::shared_ptr<SegmentProxy> findSegment( std::vector<std::wstring>::const_iterator beg, std::vector<std::wstring>::const_iterator end );

private:

  std::shared_ptr<SegmentProxy> getSegmentProxy( std::wstring const& name );

  void setDefaultState();

private:
  std::shared_ptr<Namespace const> mNamespace;

  std::map< std::wstring, std::shared_ptr<NamespaceProxy> > mChildNamespaces;
  std::map< std::wstring, std::shared_ptr<SegmentProxy> > mChildSegments;

  NamespaceProxy * mParent;

  std::shared_ptr<Sink> mSink;
  std::shared_ptr<MemoryAllocator> mAllocator;

  std::optional< int > mMemSize;
  std::optional< int > mIdxSize;
  std::optional< CPUMode::Mode > mCpuMode;

  std::vector< UsageList > mUsages;
};

} //namespace las

#endif //LAS_NAMESPACEPROXY_HPP

