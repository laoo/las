#ifndef LAS_GAHTERSESSION_HPP
#define LAS_GAHTERSESSION_HPP

#include "CPUMode.hpp"
#include "Value.hpp"
#include "MeasureContext.hpp"

namespace GE5
{
struct Position;
}

namespace las
{

class SegmentProxy;
class CalcContext;
class Label;
class Usage;
class Equ;

class GatherSession : public MeasureContext
{
public:
  GatherSession( SegmentProxy & parentProxy );
  virtual ~GatherSession();

  virtual SegmentProxy * currentCodeSegment();
  virtual SegmentProxy * currentDataSegment();

  virtual value::Measure measureIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec , value::UsageReq req );

  // returns current size of memory operations
  int memSize() const;

  //returns current size of index operations
  int idxSize() const;

  bool isNative() const;

  bool isBss() const;

  // advance pc
  void cmdAdvance( int bytes );
  void bssAdvance( int bytes );
  void bssAdvance( Value const& value );

  void commitLabel( std::shared_ptr<Label> const& label );
  void commitUsage( GE5::Position const& position, Usage const& usage );

  void cpuNative( GE5::Position const& pos, int memSize = 0, int idxSize = 0 );
  void cpuEmulation();
  void setOrigin( GE5::Position const& pos, Value value );

  size_t getSize() const;

private:
   SegmentProxy & mParentProxy;
   bool mBegun;
   bool mBss;
   size_t mCurrentOffset;
   std::optional< CPUMode::Mode > mCPUMode;
   std::optional< int > mMemSize;
   std::optional< int > mIdxSize;
   std::optional< Value > mOrg;
};

} //namespace las

#endif //LAS_GAHTERSESSION_HPP
