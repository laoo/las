#include "stdafx.h"
#include "GatherSession.hpp"
#include "EmptyCalcContext.hpp"
#include "SegmentProxy.hpp"
#include "Value.hpp"
#include "value/Host.hpp"
#include "value/Measure.hpp"
#include "Label.hpp"
#include "Equ.hpp"
#include "Exception.hpp"
#include "Usage.hpp"
#include "NameUtils.hpp"

namespace las
{

GatherSession::GatherSession( SegmentProxy & parentProxy ) : mParentProxy( parentProxy ),
  mBegun( false ), mBss( false ), mCurrentOffset( 0 ),
  mCPUMode(), mMemSize(), mIdxSize()
{
}

GatherSession::~GatherSession()
{
}

SegmentProxy * GatherSession::currentCodeSegment()
{
  return &mParentProxy;
}

SegmentProxy * GatherSession::currentDataSegment()
{
  return &mParentProxy;
}

value::Measure GatherSession::measureIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec, value::UsageReq req )
{
  std::optional<SegmentProxy::SymbolWithUsage> sym = mParentProxy.findMachingSymbol( pos, name, dec, req );
  if ( sym )
  {
    if ( sym->symbolValue.type == Segment::ST_EQU )
    {
      value::Measure m = sym->symbolValue.equ->getValue().measureValue( *this, req );
      m.decorate( dec );
      return m;
    }
    else
    {
      value::Measure m( pos, sym->usage->segment.get() );
      m.decorate( sym->usage->usage.compatibleDecoration() );
      return m;
    }
  }
  else
  {
    throw SemanticException( pos, std::wstring( L"Symbol " ) + wstring( name ) + L" not found" );
  }
}

int GatherSession::memSize() const
{
  if ( mMemSize )
    return *mMemSize;
  else
  {
    auto parentMemSize = mParentProxy.memSize();
    assert( parentMemSize );
    return *parentMemSize;
  }
}

int GatherSession::idxSize() const
{
  if ( mIdxSize )
    return *mIdxSize;
  else
  {
    auto parentIdxSize = mParentProxy.idxSize();
    assert( parentIdxSize );
    return *parentIdxSize;
  }
}

void GatherSession::cpuNative( GE5::Position const& pos, int mSize, int xSize )
{
  if ( mCPUMode )
  {
    if ( *mCPUMode == CPUMode::NATIVE )
    {
      if ( mSize )
        mMemSize = mSize;
      if ( xSize )
        mIdxSize = xSize;
    }
    else
    {
      if ( mSize )
        mMemSize = mSize;
      else
        mMemSize = 8;
      if ( xSize )
        mIdxSize = xSize;
      else
        mMemSize = 8;
    }
  }
  else
  {
    if ( mSize )
      mMemSize = mSize;
    else
      mMemSize = memSize();
    if ( xSize )
      mIdxSize = xSize;
    else
      mIdxSize = idxSize();
  }

  mCPUMode = CPUMode::NATIVE;
}

void GatherSession::cpuEmulation()
{
  mCPUMode = CPUMode::EMULATION;
  mMemSize = 8;
  mIdxSize = 8;
}

void GatherSession::cmdAdvance( int bytes )
{
  if ( mBegun )
  {
    mCurrentOffset += bytes;
  }
  else
  {
    mBegun = true;
    mBss = false;
    mParentProxy.setOrigin( mOrg );
    mCurrentOffset = bytes;
  }
}

void GatherSession::bssAdvance( int bytes )
{
  if ( mBegun )
  {
    mCurrentOffset += bytes;
  }
  else
  {
    mBegun = true;
    mBss = true;
    mParentProxy.setOrigin( mOrg );
    mCurrentOffset += bytes;
  }
}

void GatherSession::bssAdvance( Value const& value )
{
  int v = value.calcValue( EmptyCalcContext() ).getValue();
  bssAdvance( v );
}


void GatherSession::commitLabel( std::shared_ptr<Label> const& pLabel )
{
  mParentProxy.commitLabel( pLabel->name(), mCurrentOffset );
}

void GatherSession::commitUsage( GE5::Position const& position, Usage const& usage )
{
  mParentProxy.commitUsage( position, usage );
}

void GatherSession::setOrigin( GE5::Position const& pos, Value value )
{

  if ( mBegun )
  {
    if ( mBss )
    {
      int v = value.calcValue( EmptyCalcContext() ).getValue();

      if ( v < 0 || v > 0xffffff )
      {
        throw SemanticException( pos, L"Bad address" );
      }

      unsigned int uvalue = (unsigned int)v;

      if ( uvalue < mCurrentOffset )
      {
        throw SemanticException( pos, ( boost::wformat( L"Cannot move back in bss segment from %04x to %04x" ) % mCurrentOffset % uvalue ).str() );
      }
      if ( ( uvalue >> 16 ) != ( mCurrentOffset >> 16 ) )
      {
        throw SemanticException( pos, ( boost::wformat( L"Cannot change bank in bss segment from %02x to %02x" )
            % ( mCurrentOffset >> 16 ) % ( uvalue >> 16 ) ).str() );
      }
      mCurrentOffset = uvalue;
    }
    else
    {
      throw SemanticException( pos, L"Cannot change address in not bss segment" );
    }
  }
  else
  {
    mOrg = std::move( value );
  }
}

size_t GatherSession::getSize() const
{
  return mCurrentOffset;
}

bool GatherSession::isBss() const
{
  assert( mBegun );
  return mBss;
}

} //namespace las

