#ifndef LAS_NAMESPACE_HPP
#define LAS_NAMESPACE_HPP


namespace GE5
{
struct Position;
};

namespace las
{
namespace cmd
{
class Org;
}

class Segment;
class Command;
class Label;
class Usage;
class UsageShift;
class CPUMode;
class NamespaceProxy;
class Equ;

class Namespace
{
  friend class NamespaceProxy;
public:
  Namespace( std::wstring aName );

  std::shared_ptr<Namespace> addNamespace( GE5::Position const& position, std::wstring aName );
  void openSegment( GE5::Position const& position, std::wstring aName );
  bool closeSegment( GE5::Position const& position, std::wstring const& aName );
  void addCommand( std::shared_ptr<Command> cmd );
  void addLabel( std::shared_ptr<Label> label );
  void addUsage( std::shared_ptr<Usage> usage );
  void shiftUsage( std::shared_ptr<UsageShift> usage );
  void addCPUMode( std::shared_ptr<CPUMode> cpuMode );
  void addEqu( std::shared_ptr<Equ> equ );

  bool hasOpenSegment() const;
  bool hasMainSegment() const;
  std::wstring const& getName() const;

  void merge( std::shared_ptr<Namespace> other );

protected:
  std::wstring fullName() const;

private:
  void assertUniqueSegment( GE5::Position const& position, std::wstring const& name ) const;

private:
  std::wstring const mName;
  std::wstring mFullName;
  std::map< std::wstring, std::shared_ptr<Namespace> > mChildNamespaces;
  std::map< std::wstring, std::shared_ptr<Segment> > mChildSegments;

  std::shared_ptr<cmd::Org> mOrg; //namespace origin.
  std::shared_ptr<CPUMode> mCPUMode;
  std::vector< std::shared_ptr<Usage> > mUsages;
  std::map< std::wstring, std::shared_ptr<Equ> > mEquates;
  std::shared_ptr<Segment> mOpenSegment;

};

} //namespace las

#endif //LAS_NAMESPACE_HPP

