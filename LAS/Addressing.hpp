#ifndef LAS_ADDRESSING_HPP
#define LAS_ADDRESSING_HPP

#include "Value.hpp"
#include "cmd/Opcodes.hpp"
#include "Command.hpp"
namespace las
{

struct Addressing
{
  enum Index
  {
    X,
    Y,
    S
  };

  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic ) = 0;
  virtual ~Addressing();
};

struct AddressingImplied : public Addressing
{
  AddressingImplied();
  virtual ~AddressingImplied();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );
};

struct AddressingImmediate : public Addressing
{
  AddressingImmediate( Value v );
  virtual ~AddressingImmediate();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingAbsolute : public Addressing
{
  AddressingAbsolute( Value v );
  virtual ~AddressingAbsolute();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingIndirect : public Addressing
{
  AddressingIndirect( Value v );
  virtual ~AddressingIndirect();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingDirectIndirectIndexed : public Addressing
{
  AddressingDirectIndirectIndexed( Value v );
  virtual ~AddressingDirectIndirectIndexed();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingDirectIndirectLong : public Addressing
{
  AddressingDirectIndirectLong( Value v );
  virtual ~AddressingDirectIndirectLong();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingDirectIndirectLongIndexed : public Addressing
{
  AddressingDirectIndirectLongIndexed( Value v );
  virtual ~AddressingDirectIndirectLongIndexed();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingStackRelativeIndirectIndexed : public Addressing
{
  AddressingStackRelativeIndirectIndexed( Value v );
  virtual ~AddressingStackRelativeIndirectIndexed();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};

struct AddressingIndexed : public Addressing
{
  AddressingIndexed( Value v, Index i );
  virtual ~AddressingIndexed();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
  Index index;
};

struct AddressingIndexedIndirect : public Addressing
{
  AddressingIndexedIndirect( Value v );
  virtual ~AddressingIndexedIndirect();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value value;
};


struct AddressingMove : public Addressing
{
  AddressingMove( Value src, Value dst );
  virtual ~AddressingMove();
  virtual std::shared_ptr<Command> applyAddressing( GE5::Position const& position, cmd::Mnemonic mnemonic );

  Value source;
  Value dest;
};

} //namespace las

#endif //LAS_ADDRESSING_HPP
