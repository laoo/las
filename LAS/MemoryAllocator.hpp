#ifndef LAS_MEMORYALLOCATOR_HPP
#define LAS_MEMORYALLOCATOR_HPP


#include "Value.hpp"

namespace las
{
class NamespaceProxy;
class SegmentProxy;

class MemoryAllocator
{
public:
  MemoryAllocator();
  ~MemoryAllocator();

  void registerRange( NamespaceProxy const* p, int address,  std::optional<size_t> size );
  /**
   * Preallocates specified block for given segent.
   *
   * @param seg Segment to be allocated block for
   * @param org The beginning of the block
   * @param size The size of the block
   */
  void preallocate( SegmentProxy * seg, Value org, size_t size );

  int allocate( SegmentProxy const* seg );
  std::optional<int> allocate( GE5::Position const& position, NamespaceProxy const* p, size_t size );

private:
  struct Bank
  {
    Bank();
    std::vector<SegmentProxy const*> memory;
  };

  struct Prealloc
  {
    Prealloc( SegmentProxy * aSeg, Value && aOrg, size_t aSize );
    SegmentProxy * seg;
    Value org;
    size_t size;
  };

private:
  void preallocate();
  Bank & getBank( int address );
private:

  struct Range
  {
    int address;
    size_t size;
  };
  
  std::map<NamespaceProxy const*, Range > mRanges;
  std::map<SegmentProxy const*, int> mPreallocated;
  std::map< unsigned char, Bank > mBanks;
  std::vector< Prealloc > mToPreallocate;

};

} //namespace las

#endif //LAS_MEMORYALLOCATOR_HPP
