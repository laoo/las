#ifndef LAS_SEGMENTPROXY_HPP
#define LAS_SEGMENTPROXY_HPP

#include "CPUMode.hpp"
#include "NamespaceProxy.hpp"
#include "Value.hpp"
#include "Segment.hpp"

namespace las
{
class Sink;
class Usage;

class SegmentProxy
{
public:

  struct SymbolWithUsage
  {
    SymbolWithUsage( Segment::SymbolValue const& sv ) : symbolValue( sv ), usage( nullptr ) {}
    Segment::SymbolValue symbolValue;
    NamespaceProxy::UsageList const* usage;
  };

  SegmentProxy( std::shared_ptr<Segment const> pSegment, NamespaceProxy * pParent, std::shared_ptr<Sink> sink );
  virtual ~SegmentProxy();

  std::pair< std::optional<Value>, size_t > gatheredLocation() const;
  Segment const* segment() const;
  size_t anonymousLabelOffset( GE5::Position const& pos, size_t segmentOffset, bool forward, int count );

  void emit();

  std::optional< int > bank();
  boost::tribool sameBank( SegmentProxy * otherSegment );

    std::optional< int > memSize() const;
  std::optional< int > idxSize() const;
  std::optional< CPUMode::Mode > cpuMode() const;
  void setOrigin( std::optional<Value> value );
  int getOrigin();
  size_t getSize() const;
  bool isBound( int address ) const;
  void commitLabel( std::wstring name, size_t offset );
  void commitUsage( GE5::Position const& position, Usage const& usage );
  std::optional<SymbolWithUsage> findMachingSymbol( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec, value::UsageReq req );
  std::optional<SymbolWithUsage> findSymbol( std::wstring const& name );

  size_t getLabelValue( std::wstring const& name ) const;

private:

  enum SegmentState
  {
    INITIAL,
    GATHERING,
    GATHERED,
    EMITTING,
    EMITTED
  };


  void gather();

private:
  std::shared_ptr<Segment const> mSegment;
  NamespaceProxy * mParent;
  std::shared_ptr<Sink> mSink;
  SegmentState mState;
  std::optional<Value> mOrg;
  std::optional<int> mCalculatedOrg;
  size_t mSize;
  bool mBss;  //valid after GATHERED
  std::map< std::wstring, size_t > mLabels;
  std::vector< size_t > mAnonymousLabels;
  std::list< NamespaceProxy::UsageList > mUsages;
  NamespaceProxy::UsageList const* mHeadUsage;

};

} //namespace las

#endif //LAS_SEGMENTPROXY_HPP
