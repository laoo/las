#ifndef LAS_ASSEMBLER_HPP
#define LAS_ASSEMBLER_HPP

namespace las
{
class Module;
class Namespace;
class Out;
class Sink;

class Assembler
{
public:
  Assembler();
  ~Assembler();

  void addModule( std::shared_ptr<Module> module );

  void process();

private:
  void assemble( std::shared_ptr<Namespace> pRoot, std::shared_ptr<Sink> pSink );
  void assemble( std::shared_ptr<Namespace> pRoot, std::shared_ptr<Sink> pSink, std::shared_ptr<Out> pOut );

  std::shared_ptr<Sink> createDefaultSink() const;
  std::shared_ptr<Sink> createOutSink( Out const* pOut ) const;

private:
  size_t mModules;
  std::shared_ptr<Namespace> mRootNamespace;
  std::vector< std::shared_ptr<Out> > mOutputs;

};

} //namespace las

#endif //LAS_MODULE_HPP

