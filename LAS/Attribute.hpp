#ifndef LAS_ATTRIBUTE_HPP
#define LAS_ATTRIBUTE_HPP

#include "ge5/include/Position.hpp"
#include "Value.hpp"

namespace GE5
{
struct Position;
}

namespace las
{

class Attribute
{
public:
  Attribute( GE5::Position const& position, std::vector< Value > && values );
  ~Attribute();

private:
  GE5::Position  mPosition;
   std::vector< Value > mValues;
};

} //namespace las

#endif //LAS_ATTRIBUTE_HPP
