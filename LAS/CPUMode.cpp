#include "stdafx.h"
#include "CPUMode.hpp"
#include "GatherSession.hpp"
#include "Exception.hpp"

namespace las
{

CPUMode::CPUMode( GE5::Position const& position, Mode m, int accumSize, int indexSize ) :
  SegmentContent( position ), mMode( m ), mAccumSize( accumSize ), mIndexSize( indexSize )
{
}

CPUMode::~CPUMode()
{
}

void CPUMode::gather( GatherSession & session ) const
{
  if ( mMode == NATIVE )
    session.cpuNative( position(), mAccumSize, mIndexSize );
  else
    session.cpuEmulation();
}

void CPUMode::emit( EmitSession & session ) const
{
  throw NYIException( position(), L"Emit" );
}

std::shared_ptr<CPUMode> CPUMode::native( GE5::Position const& position, int accumSize, int indexSize )
{
  assert( ( accumSize == 0 || accumSize == 8 || accumSize == 16 ) && ( indexSize == 0 || indexSize == 8 || indexSize == 16 ) ) ;

  return std::make_shared<CPUMode>( position, NATIVE, accumSize, indexSize );
}

std::shared_ptr<CPUMode> CPUMode::emulation( GE5::Position const& position )
{
  return std::make_shared<CPUMode>( position, EMULATION, 8, 8 );
}

bool operator==( CPUMode const& left, CPUMode const& right )
{
  return left.mMode == right.mMode && left.mAccumSize == right.mAccumSize && left.mIndexSize == right.mIndexSize;
}

bool operator!=( CPUMode const& left, CPUMode const& right )
{
  return !( left == right );
}

} //namespace las

