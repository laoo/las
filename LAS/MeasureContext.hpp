#ifndef LAS_MEASURECONTEXT_HPP
#define LAS_MEASURECONTEXT_HPP

#include "value/Decoration.hpp"
#include "value/UsageReq.hpp"

namespace GE5
{
struct Position;
}

namespace las
{
namespace value
{
class Host;
class Measure;
}

class SegmentProxy;

class MeasureContext
{
public:

  virtual ~MeasureContext();

  virtual SegmentProxy * currentCodeSegment();
  virtual SegmentProxy * currentDataSegment();
  virtual value::Measure measureIdentifier( GE5::Position const& pos, std::vector<std::wstring> const& name,
    value::Decoration dec , value::UsageReq req );

private:
  static const wchar_t mMessage[];

};

} //namespace las

#endif //LAS_MEASURECONTEXT_HPP
