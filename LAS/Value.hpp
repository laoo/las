#ifndef LAS_VALUE_HPP
#define LAS_VALUE_HPP

#include "value/Operators.hpp"
#include "value/UsageReq.hpp"

namespace GE5
{
struct Position;
}

namespace las
{
class Command;
class GatherSession;
class EmitSession;
class CalcContext;
class MeasureContext;

namespace value
{
class Holder;
class Host;
class Measure;
}

class Value
{
  typedef int Type;

  static const Type T_NAKEN         = 0;
  static const Type T_PARENTHESIZED = 1;
  static const Type T_DIRECTED      = 4;
  static const Type T_ABSOLUTED     = 8;
  static const Type T_LONGENED      = 16;

public:

  Value();
  ~Value();
  Value( Value const& other );
  Value( Value && other );
  Value & operator=( Value const& other );
  Value & operator=( Value && other );

  bool parenthesized() const;
  bool directed() const;
  bool absoluted() const;
  bool longened() const;
  bool literal( int & out ) const;

  Value & parenthesize();
  Value & direct();
  Value & absolute();
  Value & longen();
  Value & prependName( std::vector< std::wstring > name );

  value::Host calcValue( CalcContext & context, value::UsageReq req = value::U_VALUE ) const;
  value::Measure measureValue( MeasureContext & context, value::UsageReq req ) const;
  std::optional< int > retrieveBank( CalcContext & context ) const;

  static Value intLiteral( GE5::Position const& position, int value );
  static Value anonymousLabel( GE5::Position const& position, bool forward, int skip );
  static Value pc( GE5::Position const& position );
  static Value id( GE5::Position const& position, std::vector< std::wstring > name );
  static Value string( GE5::Position const& position, std::wstring value, value::CharType type );
  static Value fun( GE5::Position const& position, std::wstring name );
  static Value fun( GE5::Position const& position, std::wstring name, Value && arg );
  static Value opcode( GE5::Position const& position, std::shared_ptr<Command> cmd );
  static Value opUnary( GE5::Position const& position, value::Operators op, Value && value );
  static Value opBinary( GE5::Position const& position, value::Operators op, Value && left, Value && right );


protected:
  static Value applySpecial( GE5::Position const& position, Value && value );

private:

  std::shared_ptr<value::Holder> mHolder;
  Type mType;
};

} //namespace las

#endif //LAS_VALUE_HPP
