#include "stdafx.h"
#include "SegmentProxy.hpp"
#include "EmptyCalcContext.hpp"
#include "Segment.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "NamespaceProxy.hpp"
#include "Sink.hpp"
#include "Usage.hpp"
#include "Equ.hpp"
#include "Exception.hpp"

namespace las
{

SegmentProxy::SegmentProxy( std::shared_ptr<Segment const> pSegment, NamespaceProxy * pParent, std::shared_ptr<Sink> sink ) :
  mSegment( std::move( pSegment ) ),
  mParent( pParent ), mSink( std::move( sink ) ),
  mState( INITIAL ), mOrg(), mCalculatedOrg(), mSize( 0 ), mHeadUsage( nullptr )
{
}

SegmentProxy::~SegmentProxy()
{
}

size_t SegmentProxy::anonymousLabelOffset( GE5::Position const& pos, size_t segmentOffset, bool forward, int count )
{
  if ( forward )
  {
    int i = 0;
    for ( ; (size_t)i < mAnonymousLabels.size(); ++i )
    {
      if ( mAnonymousLabels[i] > segmentOffset )
        break;
    }
    if ( (size_t)( i + count ) >= mAnonymousLabels.size() )
      throw SemanticException( pos, L"Not enough anonymous labels in positive direction" );

    return mAnonymousLabels[ i + count ];
  }
  else
  {
    int i = (int)mAnonymousLabels.size() - 1 ;
    for ( ; i >= 0; --i )
    {
      if ( mAnonymousLabels[i] <= segmentOffset )
        break;
    }
    if ( i - count < 0 )
      throw SemanticException( pos, L"Not enough anonymous labels in negative direction" );

    return mAnonymousLabels[ i - count ];
  }
}

std::pair< std::optional<Value>, size_t > SegmentProxy::gatheredLocation() const
{
  return std::make_pair( mOrg, mSize );
}

Segment const* SegmentProxy::segment() const
{
  return mSegment.get();
}

void SegmentProxy::emit()
{
  gather();

  if ( mBss )
    return;

  if ( mState == GATHERED )
  {
    mState = EMITTING;

    mUsages.clear();
    mHeadUsage = nullptr;

    EmitSession session( *this, getOrigin(), mSink );
    mSegment->emit( session );
    mState = EMITTED;
    session.emitReferencedSegments();
  }
}

std::optional< int > SegmentProxy::bank()
{
  if ( mOrg )
    return ( *mOrg->retrieveBank( EmptyCalcContext() ) );
  else
    return mParent->bank();
}

void SegmentProxy::gather()
{
  if ( mState == INITIAL )
  {
    mState = GATHERING;
    GatherSession session( *this );
    mSegment->gather( session );
    mSize = session.getSize();
    mState = GATHERED;
    if ( mOrg )
      mParent->preallocate( this, *mOrg, mSize );
    mBss = session.isBss();
  }
  else if ( mState == GATHERING )
  {
    throw Exception( L"Circular dependency" ); //TODO: be more specific.
  }
}

void SegmentProxy::commitLabel( std::wstring name, size_t offset )
{
  if ( name.empty() )
  {
    mAnonymousLabels.push_back( offset );
  }
  else
  {
    assert( mLabels.find( name ) == mLabels.cend() );
    mLabels.insert( std::make_pair( name, offset ) );
  }
}

size_t SegmentProxy::getLabelValue( std::wstring const& name ) const
{
  auto it = mLabels.find( name );
  assert( it != mLabels.cend() );
  return it->second;
}

void SegmentProxy::commitUsage( GE5::Position const& position, Usage const& usage )
{
  NamespaceProxy::UsageList u( usage );
  u.segment = mParent->findSegment( usage.getName().cbegin(), usage.getName().cend() );
  if ( !u.segment )
    throw SemanticException( position, L"Specified segment not found" );

  if ( mUsages.empty() )
    u.pPrevious = mParent->getUsageList();
  else
    u.pPrevious = &mUsages.back();

  if ( auto colliding = u.validate( u.pPrevious ) )
  {
    throw SemanticException( u.usage.position(), std::wstring( L"Usage collision with " ) + colliding->usage.position().wstring() );
  }

  mUsages.push_back( u );
  mHeadUsage = &mUsages.back();
}

std::optional<SegmentProxy::SymbolWithUsage> SegmentProxy::findSymbol( std::wstring const& name )
{
  std::optional<Segment::SymbolValue> sv = mSegment->getSymbolValue( name );
  if ( sv )
  {
    if ( mState == INITIAL )
      gather();

    SymbolWithUsage psv( *sv );
    return psv;
  }
  else
    return {};
}

std::optional<SegmentProxy::SymbolWithUsage> SegmentProxy::findMachingSymbol( GE5::Position const& pos, std::vector<std::wstring> const& name,
  value::Decoration dec, value::UsageReq req )
{
  std::optional<SymbolWithUsage> resultSymbol;

  auto assignOrThrow = [&resultSymbol,&pos]( std::optional<SymbolWithUsage> sym, NamespaceProxy::UsageList const* usage )
  {
    if ( sym )
    {
      if ( resultSymbol && sym->usage && resultSymbol->usage && sym->usage->segment != resultSymbol->usage->segment )
      {
        throw SemanticException( pos, std::wstring( L"Accessing ambiguous symbol. Might be:\n" ) +
          sym->symbolValue.pos.wstring() + L"\nor\n"  + resultSymbol->symbolValue.pos.wstring() );
      }
      else
      {
        resultSymbol = sym;
        resultSymbol->usage = usage;
      }
    }
  };

  if ( name.size() == 1 )
  {
    //searching in this and in non-aliased usings

    resultSymbol = findSymbol( name[0] );

    for ( NamespaceProxy::UsageList const* usage = mHeadUsage; usage; usage = usage->pPrevious )
    {
      if ( usage->usage.getAlias().empty() )
      {
        if ( req == value::U_BRANCH || usage->usage.compatibleAddressing( dec ) )
          assignOrThrow( usage->segment->findSymbol( name[0] ), usage );
      }
    }
  }
  else
  {
    if ( name.size() == 2 )
    {
      //searching in aliased usings
      for ( NamespaceProxy::UsageList const* usage = mHeadUsage; usage; usage = usage->pPrevious )
      {
        if ( !usage->usage.getAlias().empty() )
        {
          if (name[0] != usage->usage.getAlias() )
            continue;

          if ( req == value::U_BRANCH || usage->usage.compatibleAddressing( dec ) )
            assignOrThrow( usage->segment->findSymbol( name[1] ), usage );
        }
      }
    }

    //searching in parent namespace
    std::shared_ptr<SegmentProxy> seg = mParent->findSegment( name.cbegin(), name.cbegin() + name.size() - 1 );
    if ( seg )
    {
      std::optional<SymbolWithUsage> sym = seg->findSymbol( name.back() );
      if ( sym )
      {
        //ignoring labels outside of using list for accesses.
        if ( req == value::U_BRANCH || sym->symbolValue.type == Segment::ST_EQU )
          assignOrThrow( sym, nullptr ); 
      }
    }
  }



  return resultSymbol;
}

std::optional< int > SegmentProxy::memSize() const
{
  // TODO: local mode
  return mParent->memSize();
}

std::optional< int > SegmentProxy::idxSize() const
{
  // TODO: local mode
  return mParent->idxSize();
}

std::optional< CPUMode::Mode > SegmentProxy::cpuMode() const
{
  // TODO: local mode
  return mParent->cpuMode();
}

void SegmentProxy::setOrigin( std::optional<Value> value )
{
  mOrg = std::move( value );
}

int SegmentProxy::getOrigin()
{
  if ( !mCalculatedOrg )
  {
    if ( mOrg )
      mCalculatedOrg = mParent->allocate( this );
    else
      mCalculatedOrg = mParent->allocate( mSegment->position(), mSize );
  }

  return *mCalculatedOrg;

}

size_t SegmentProxy::getSize() const
{
  return mSize;
}

bool SegmentProxy::isBound( int address ) const
{
  return address >= 0 && address < (int)mSize;
}

boost::tribool SegmentProxy::sameBank( SegmentProxy * otherSegment )
{
  std::optional<int> thisBank = bank();
  otherSegment->gather();
  std::optional<int> otherBank = otherSegment->bank();
  if ( thisBank && otherBank )
  {
    return *thisBank == *otherBank;
  }
  else
  {
    return boost::indeterminate;
  }
}


} //namespace las

