#include "stdafx.h"
#include "NamespaceProxy.hpp"
#include "Namespace.hpp"
#include "EmptyCalcContext.hpp"
#include "SegmentProxy.hpp"
#include "Sink.hpp"
#include "MemoryAllocator.hpp"
#include "Usage.hpp"
#include "Exception.hpp"
#include "Value.hpp"
#include "value/Host.hpp"
#include "cmd/Org.hpp"

namespace las
{

NamespaceProxy::NamespaceProxy( std::shared_ptr<Namespace const> pNamespace, std::shared_ptr<Sink> sink,
  std::shared_ptr<MemoryAllocator> pAllocator, NamespaceProxy * pParent ) :
  mNamespace( std::move( pNamespace ) ),
  mParent( pParent ),
  mSink( std::move( sink ) ),
  mAllocator( std::move( pAllocator ) )
{
  if ( !pParent )
    setDefaultState();
  if ( mNamespace->mOrg )
  {
    mAllocator->registerRange( this, mNamespace->mOrg->getValue().calcValue( EmptyCalcContext() ).getValue(), {} );
  }

  for ( auto it = mNamespace->mUsages.cbegin(); it != mNamespace->mUsages.cend(); ++it )
  {
    UsageList u( **it );
    u.segment = findSegment( (*it)->getName().cbegin(), (*it)->getName().cend() );
    if ( !u.segment )
      throw SemanticException( u.usage.position(), L"Specified segment not found" );
    if ( !mUsages.empty() )
    {
      if ( auto colliding = u.validate( &mUsages.back() ) )
      {
        throw SemanticException( u.usage.position(), std::wstring( L"Usage collision with " ) + colliding->usage.position().wstring() );
      }
      u.pPrevious = &mUsages.back();
    }
    mUsages.push_back( u );
  }
}

NamespaceProxy::~NamespaceProxy()
{
}

void NamespaceProxy::emitSegment( std::wstring const& name )
{
  std::shared_ptr<SegmentProxy> proxy = getSegmentProxy( name );
  if ( proxy )
    proxy->emit();
  else
  {
    throw Exception( mNamespace->fullName() + L" does not have segment " + name );
  }
}

std::shared_ptr<SegmentProxy> NamespaceProxy::getSegmentProxy( std::wstring const& name )
{
  auto it = mChildSegments.find( name );
  if ( it == mChildSegments.end() )
  {
    auto segment = mNamespace->mChildSegments.find( name );
    if ( segment == mNamespace->mChildSegments.end() )
      return std::shared_ptr<SegmentProxy>();

    auto p = mChildSegments.insert( std::make_pair( name, std::make_shared<SegmentProxy>( segment->second, this, mSink ) ) );
    
    return p.first->second;
  }
  else
  {
    return it->second;
  }
}


std::shared_ptr<SegmentProxy> NamespaceProxy::findSegment( std::vector<std::wstring>::const_iterator beg, std::vector<std::wstring>::const_iterator end )
{
  std::shared_ptr<SegmentProxy> result;

  if ( std::distance( beg, end ) == 1 )
  {
    result = getSegmentProxy( *beg );
  }
  else
  {
    auto ns = mChildNamespaces.find( *beg );
    if ( ns != mChildNamespaces.end() )
      result = ns->second->findSegment( beg + 1, end );

    if ( !result )
      result = mParent->findSegment( beg, end );
  }

  return result;
}



std::optional< int > NamespaceProxy::memSize() const
{
  if ( mMemSize )
    return mMemSize;
  else
  {
    assert( mParent );
    return mParent->memSize();
  }
}

std::optional< int > NamespaceProxy::idxSize() const
{
  if ( mIdxSize )
    return mIdxSize;
  else
  {
    assert( mParent );
    return mParent->idxSize();
  }
}

std::optional< CPUMode::Mode > NamespaceProxy::cpuMode() const
{
  if ( mCpuMode )
    return mCpuMode;
  else
  {
    assert( mParent );
    return mParent->cpuMode();
  }
}

void NamespaceProxy::setDefaultState()
{
  mMemSize = 1; //in bytes
  mIdxSize = 1; //default to 8 bit in emulation
  mCpuMode = CPUMode::EMULATION;
}

void NamespaceProxy::preallocate( SegmentProxy * segment, Value org, size_t size )
{
  mAllocator->preallocate( segment, std::move( org ), size );
}

int NamespaceProxy::allocate( SegmentProxy const* segment )
{
  return mAllocator->allocate( segment );
}

int NamespaceProxy::allocate( GE5::Position const& position, size_t size )
{
  auto opt = mAllocator->allocate( position, this, size );
  if( opt )
    return *opt;
  else
  {
    if ( mParent )
    {
      return mParent->allocate( position, size );
    }
    else
    {
      throw SemanticException( position, L"None among segment parents provided origin address" );
    }
  }
}

std::optional< int > NamespaceProxy::bank()
{
  if ( mNamespace->mOrg )
  {
    return mNamespace->mOrg->getValue().retrieveBank( EmptyCalcContext() );
  }
  else if ( mParent )
  {
    return mParent->bank();
  }
  else
    return {};
}

NamespaceProxy::UsageList const* NamespaceProxy::getUsageList() const
{
  if ( mUsages.empty() )
    return nullptr;
  else
    return &mUsages[0];
}

NamespaceProxy::UsageList const* NamespaceProxy::UsageList::validate( UsageList const* head ) const
{
  for ( ; head; head = head->pPrevious )
  {
    if ( segment != head->segment )
      continue;

    if ( usage.getAlias() != head->usage.getAlias() )
      continue;

    if ( usage.getAddressing() != head->usage.getAddressing() )
      continue;

    break;
  }
  return head;
}

} //namespace las

