#include "stdafx.h"
#include "PositionHolder.hpp"

namespace las
{

PositionHolder::PositionHolder( GE5::Position const& position ) : mPosition( position )
{
}

PositionHolder::~PositionHolder()
{
}

GE5::Position const& PositionHolder::position() const
{
  return mPosition;
}

} //namespace las

