#include "stdafx.h"
#include "SinkLog.hpp"
#include "ge5/include/Position.hpp"

namespace las
{

const int SinkLog::mWorkSize = 16;
const int SinkLog::mLinesAround = 2;

SinkLog::SinkLog() : mPath( L"out.log" ), mFout( mPath.string().c_str() )
{
}

SinkLog::~SinkLog()
{
  try
  {
    if ( mSrcProxy )
      fillUntil( mSrcProxy->lines() );
  }
  catch( ... )
  {
  }
}

void SinkLog::emit( GE5::Position const& pos, int address, unsigned char op )
{
  emit( pos, address, &op, &op + 1 );
}

void SinkLog::emit( GE5::Position const& pos, int address, unsigned char op, unsigned char data )
{
  unsigned char buf[2];
  buf[0] = op;
  buf[1] = data;
  emit( pos, address, buf, buf + 2 );
}

void SinkLog::emit( GE5::Position const& pos, int address, unsigned char op, unsigned short data )
{
  unsigned char buf[3];
  buf[0] = op;
  buf[1] = data & 0xff;
  buf[2] = data >> 8;
  emit( pos, address, buf, buf + 3 );
}

void SinkLog::emit( GE5::Position const& pos, int address, unsigned char op, unsigned int data )
{
  unsigned char buf[4];
  buf[0] = op;
  buf[1] = data & 0xff;
  buf[2] = ( data >> 8 ) & 0xff;
  buf[3] = ( data >> 16 ) & 0xff;
  emit( pos, address, buf, buf + 4 );
}

void SinkLog::emit( GE5::Position const& pos, int address, unsigned char * begin, unsigned char * end )
{
  if ( mSrcProxy != pos.sourceProxy )
  {
    if ( mSrcProxy )
      fillUntil( mSrcProxy->lines() );

    mSrcProxy = pos.sourceProxy;
    int numberOfLines = mSrcProxy->lines();
    mLineNumberDigits = 0;
    while ( numberOfLines != 0 )
    {
      numberOfLines /= 10;
      mLineNumberDigits++;
    }

    mCurrentLine = 0;

    mFout << std::setfill( L' ' ) << std::setw( mWorkSize + mLineNumberDigits + 1 ) << std::right << L" " << mSrcProxy->getName() << std::endl << std::endl;
  }

  if ( mCurrentLine > 0 && fillUntil( pos.lineBegin - mLinesAround ) < pos.lineBegin - mLinesAround )
  {
    mFout << std::setfill( L' ' ) << std::setw( mWorkSize ) << L" ";
    mFout << std::setfill( L'.' ) << std::setw( mLineNumberDigits ) << L"." << std::endl;
  }

  for ( mCurrentLine = std::max( 1, std::max( mCurrentLine, pos.lineBegin - mLinesAround ) ); mCurrentLine < pos.lineBegin; ++mCurrentLine )
  {
    mFout << std::setfill( L' ' ) << std::setw( mWorkSize + mLineNumberDigits ) << std::right << std::dec << mCurrentLine << L' ' << mSrcProxy->getLine( mCurrentLine );
  }

  mFout << std::setfill( L'0' ) << std::setw( 6 ) << std::hex << address;
  mFout << L' ';
  for ( unsigned char const* p = begin; p < begin + 4; ++p )
  {
    if ( p < end )
    {
      mFout << std::setfill( L'0' ) << std::setw( 2 ) << (unsigned int)*p;
    }
    else
    {
      mFout << L"  ";
    }
  }
  mFout << std::setfill( L' ' ) << std::setw( mLineNumberDigits + 1 ) << std::right << std::dec << mCurrentLine << L' ' << mSrcProxy->getLine( mCurrentLine ) << std::flush;
  mCurrentLine += 1;
  int cnt = 0;
  for ( unsigned char const* p = begin + 4; p < end; ++p, ++cnt )
  {
    if ( cnt == 0 )
    {
      address += 4;
      mFout << std::setfill( L'0' ) << std::setw( 6 ) << address;
    }

    mFout << std::setfill( L'0' ) << std::setw( 2 ) << (unsigned int)*p;
  }
}

int SinkLog::fillUntil( int until )
{
  if ( mSrcProxy )
  {
    int lines = mSrcProxy->lines();
    int i = mCurrentLine;
    for ( i = mCurrentLine; i < std::min( mCurrentLine + mLinesAround, until ); ++i )
    {
      mFout << std::setfill( L' ' ) << std::setw( mWorkSize + mLineNumberDigits ) << std::right << std::dec << i << L' ' << mSrcProxy->getLine( i ) << std::flush;
    }
    return i;
  }
  return 0;
}

} //namespace las

