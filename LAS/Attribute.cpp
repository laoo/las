#include "stdafx.h"
#include "Attribute.hpp"

namespace las
{

Attribute::Attribute( GE5::Position const& position, std::vector< Value > && values ) : mPosition( position ), mValues( std::move( values ) )
{
}

Attribute::~Attribute()
{
}

} //namespace las

