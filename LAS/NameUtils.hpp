#ifndef LAS_NAMEUTILS_HPP
#define LAS_NAMEUTILS_HPP


namespace las
{

std::wstring wstring( std::vector< std::wstring > const& name );

} //namespace las

#endif //LAS_NAMEUTILS_HPP
