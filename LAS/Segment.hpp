#ifndef LAS_SEGMENT_HPP
#define LAS_SEGMENT_HPP

#include "PositionHolder.hpp"
#include "Value.hpp"

namespace las
{
class Namespace;
class SegmentContent;
class Command;
class Module;
class Label;
class Usage;
class UsageShift;
class CPUMode;
class Equ;

class GatherSession;
class EmitSession;

class Segment : public PositionHolder
{
public:

  enum SymbolType
  {
    ST_EQU,
    ST_LABEL
  };

  struct SymbolValue
  {
    SymbolValue( GE5::Position const& p ) : pos( p ) {}
    SymbolValue( SymbolValue const& other ) : pos( other.pos ), type( other.type ), equ( other.equ ) {}
    SymbolValue & operator=( SymbolValue const& other )
    {
      std::swap( *this, SymbolValue( other ) );
      return *this;
    }

    GE5::Position const& pos;
    SymbolType type;
    std::shared_ptr<Equ const> equ;

  };


  Segment( GE5::Position const& position, std::wstring aName );

  std::wstring const& getName() const;
  void addCommand( std::shared_ptr<Command> content );
  void addLabel( std::shared_ptr<Label> label );
  std::vector< std::shared_ptr<Label> > const& waitingLabels() const;
  void addContent( std::shared_ptr<SegmentContent> content );
  void addEqu( std::shared_ptr<Equ> equ );

  void gather( GatherSession & session ) const;
  void emit( EmitSession & session ) const;

  std::optional< SymbolValue > getSymbolValue( std::wstring const& name ) const;

private:
  std::wstring const mName;
  std::vector< std::shared_ptr<SegmentContent> > mContent;
  std::vector< std::shared_ptr<Label> > mWaitingLabels;
  std::map< std::wstring, SymbolValue > mSymbols;
};

} //namespace las

#endif //LAS_SEGMENT_HPP

