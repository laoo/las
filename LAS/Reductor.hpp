#ifndef LAS_REDUCTOR_HPP
#define LAS_REDUCTOR_HPP

namespace GE5
{
class IParserServer;
}

namespace las
{
class Module;

class Reductor
{
public:
  Reductor( std::shared_ptr<GE5::IParserServer> server );
  ~Reductor();

  std::shared_ptr<Module> creteModule( std::filesystem::path path, int & outId );

  GE5::IParserServer & getServer();

protected:
  Module & module( int id );

private:
  std::shared_ptr<GE5::IParserServer> mServer;
  std::vector< Module* > mModules;
};

} //namespace las

#endif

