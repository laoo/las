#include "stdafx.h"
#include "CmdDirectIndirectLong.hpp"
#include "SegmentProxy.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdDirectIndirectLong::~CmdDirectIndirectLong()
{
}

void CmdDirectIndirectLong::gather( GatherSession & session ) const
{
  Command::gather( session );

  if ( mOp != JMP_AIL )
  {
    session.cmdAdvance( 1 + 1 );
  }
  else
  {
    session.cmdAdvance( 1 + 2 );
  }
}

void CmdDirectIndirectLong::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  if ( mOp != JMP_AIL )
  {
    session.emit( position(), mOp, (unsigned char)adr );
  }
  else
  {
    session.emit( position(), mOp, (unsigned short)adr );
  }
}

CmdDirectIndirectLong::CmdDirectIndirectLong( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp( -1 )
{
  switch ( mnemonic )
  {
  case ADC:
    mOp = ADC_DIL;
    break;
  case AND:
    mOp = AND_DIL;
    break;
  case CMP:
    mOp = CMP_DIL;
    break;
  case EOR:
    mOp = EOR_DIL;
    break;
  case LDA:
    mOp = LDA_DIL;
    break;
  case ORA:
    mOp = ORA_DIL;
    break;
  case SBC:
    mOp = SBC_DIL;
    break;
  case STA:
    mOp = STA_DIL;
    break;
  case JMP:
    mOp = JMP_AIL; //two bytes
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have direct indirect long addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

