#ifndef LAS_CMD_SPACEDECLARATION_HPP
#define LAS_CMD_SPACEDECLARATION_HPP

#include "Command.hpp"

namespace las
{
namespace cmd
{

class SpaceDeclaration : public Command
{
public:
  SpaceDeclaration( GE5::Position const& position, Value && size );
  virtual ~SpaceDeclaration();

  virtual wchar_t const* getName();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:

  Value const mSize;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_SPACEDECLARATION_HPP
