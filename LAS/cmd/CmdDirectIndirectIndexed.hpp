#ifndef LAS_CMD_CMDDIRECTINDIRECTINDEXED_HPP
#define LAS_CMD_CMDDIRECTINDIRECTINDEXED_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdDirectIndirectIndexed : public Command
{
public:
  CmdDirectIndirectIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdDirectIndirectIndexed();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDDIRECTINDIRECTINDEXED_HPP
