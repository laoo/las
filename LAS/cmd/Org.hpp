#ifndef LAS_CMD_ORG_HPP
#define LAS_CMD_ORG_HPP

#include "Command.hpp"

namespace las
{
namespace cmd
{

class Org : public Command
{
public:
  Org( GE5::Position const& position, Value && v );
  virtual ~Org();

  virtual wchar_t const* getName();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

  Value const& getValue() const;

private:
  Value const mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_ORG_HPP
