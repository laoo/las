#include "stdafx.h"
#include "CmdImplied.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdImplied::~CmdImplied()
{
}

void CmdImplied::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.cmdAdvance( 1 );
}

void CmdImplied::emit( EmitSession & session ) const
{
  session.emit( position(), mOp );
}

CmdImplied::CmdImplied( GE5::Position const& position, Mnemonic mnemonic ) :
  Command( position )
{
  switch ( mnemonic )
  {
  case ASL:
    mOp = ASL_IMP;
    break;
  case BRK:
    mOp = BRK_IMP;
    break;
  case CLC:
    mOp = CLC_IMP;
    break;
  case CLD:
    mOp = CLD_IMP;
    break;
  case CLI:
    mOp = CLI_IMP;
    break;
  case CLV:
    mOp = CLV_IMP;
    break;
  case DEC:
    mOp = DEC_IMP;
    break;
  case DEX:
    mOp = DEX_IMP;
    break;
  case DEY:
    mOp = DEY_IMP;
    break;
  case INC:
    mOp = INC_IMP;
    break;
  case INX:
    mOp = INX_IMP;
    break;
  case INY:
    mOp = INY_IMP;
    break;
  case LSR:
    mOp = LSR_IMP;
    break;
  case NOP:
    mOp = NOP_IMP;
    break;
  case PHA:
    mOp = PHA_IMP;
    break;
  case PHB:
    mOp = PHB_IMP;
    break;
  case PHD:
    mOp = PHD_IMP;
    break;
  case PHK:
    mOp = PHK_IMP;
    break;
  case PHP:
    mOp = PHP_IMP;
    break;
  case PHX:
    mOp = PHX_IMP;
    break;
  case PHY:
    mOp = PHY_IMP;
    break;
  case PLA:
    mOp = PLA_IMP;
    break;
  case PLB:
    mOp = PLB_IMP;
    break;
  case PLD:
    mOp = PLD_IMP;
    break;
  case PLP:
    mOp = PLP_IMP;
    break;
  case PLX:
    mOp = PLX_IMP;
    break;
  case PLY:
    mOp = PLY_IMP;
    break;
  case ROL:
    mOp = ROL_IMP;
    break;
  case ROR:
    mOp = ROR_IMP;
    break;
  case RTI:
    mOp = RTI_IMP;
    break;
  case RTL:
    mOp = RTL_IMP;
    break;
  case RTS:
    mOp = RTS_IMP;
    break;
  case SEC:
    mOp = SEC_IMP;
    break;
  case SED:
    mOp = SED_IMP;
    break;
  case SEI:
    mOp = SEI_IMP;
    break;
  case STP:
    mOp = STP_IMP;
    break;
  case TAX:
    mOp = TAX_IMP;
    break;
  case TAY:
    mOp = TAY_IMP;
    break;
  case TCD:
    mOp = TCD_IMP;
    break;
  case TCS:
    mOp = TCS_IMP;
    break;
  case TDC:
    mOp = TDC_IMP;
    break;
  case TSC:
    mOp = TSC_IMP;
    break;
  case TSX:
    mOp = TSX_IMP;
    break;
  case TXA:
    mOp = TXA_IMP;
    break;
  case TXS:
    mOp = TXS_IMP;
    break;
  case TXY:
    mOp = TXY_IMP;
    break;
  case TYA:
    mOp = TYA_IMP;
    break;
  case TYX:
    mOp = TYX_IMP;
    break;
  case WAI:
    mOp = WAI_IMP;
    break;
  case WDM:
    mOp = WDM_IMP;
    break;
  case XBA:
    mOp = XBA_IMP;
    break;
  case XCE:
    mOp = XCE_IMP;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have implied addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

