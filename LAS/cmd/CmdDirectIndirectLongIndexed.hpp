#ifndef LAS_CMD_CMDDIRECTINDIRECTLONGINDEXED_HPP
#define LAS_CMD_CMDDIRECTINDIRECTLONGINDEXED_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdDirectIndirectLongIndexed : public Command
{
public:
  CmdDirectIndirectLongIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdDirectIndirectLongIndexed();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDDIRECTINDIRECTLONGINDEXED_HPP
