#include "stdafx.h"
#include "CmdDirectIndirectLongIndexed.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdDirectIndirectLongIndexed::~CmdDirectIndirectLongIndexed()
{
}

void CmdDirectIndirectLongIndexed::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.cmdAdvance( 1 + 1 );
}

void CmdDirectIndirectLongIndexed::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  session.emit( position(), mOp, (unsigned char)adr );
}

CmdDirectIndirectLongIndexed::CmdDirectIndirectLongIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp( -1 )
{
  switch ( mnemonic )
  {
  case ADC:
    mOp = ADC_DLY;
    break;
  case AND:
    mOp = AND_DLY;
    break;
  case CMP:
    mOp = CMP_DLY;
    break;
  case EOR:
    mOp = EOR_DLY;
    break;
  case LDA:
    mOp = LDA_DLY;
    break;
  case ORA:
    mOp = ORA_DLY;
    break;
  case SBC:
    mOp = SBC_DLY;
    break;
  case STA:
    mOp = STA_DLY;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have direct indirect long indexed addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

