#ifndef LAS_CMD_CMDMOVE_HPP
#define LAS_CMD_CMDMOVE_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdMove : public Command
{
public:
  CmdMove( GE5::Position const& position, Mnemonic mnemonic, Value src, Value dst );
  virtual ~CmdMove();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mSrc;
  Value mDst;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDMOVE_HPP
