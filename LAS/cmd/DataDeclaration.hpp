#ifndef LAS_CMD_DATADECLARATION_HPP
#define LAS_CMD_DATADECLARATION_HPP

#include "Value.hpp"
#include "Command.hpp"

namespace las
{
namespace cmd
{

class DataDeclaration : public Command
{
public:

  DataDeclaration( GE5::Position const& position, DataDeclarationType type, std::vector< Value > && data );
  virtual ~DataDeclaration();

  virtual wchar_t const* getName();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:

  DataDeclarationType mType;
  std::vector< Value > mData;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_DATADECLARATION_HPP
