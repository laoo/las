#include "stdafx.h"
#include "DataDeclaration.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

DataDeclaration::DataDeclaration( GE5::Position const& position, DataDeclarationType type, std::vector< Value > && data ) :
  Command( position ),
  mType( type ),
  mData( std::move( data ) )
{
}

DataDeclaration::~DataDeclaration()
{
}

void DataDeclaration::gather( GatherSession & session ) const
{
  Command::gather( session );
  throw NYIException( position(), L"Gather" );
}

void DataDeclaration::emit( EmitSession & session ) const
{
  throw NYIException( position(), L"Emit" );
}


wchar_t const* DataDeclaration::getName()
{
  switch ( mType )
  {
  case BYTE:
    return L"byte";
  case WORD:
    return L"word";
  case LONG:
    return L"long";
  case VARIABLE:
    return L"data";
  default:
    assert( false );
    return L"";
  }
}

} //namespace cmd
} //namespace las

