#include "stdafx.h"
#include "SpaceDeclaration.hpp"
#include "GatherSession.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

SpaceDeclaration::SpaceDeclaration( GE5::Position const& position, Value && size ) :
  Command( position ),
  mSize( std::move( size ) )
{
}

SpaceDeclaration::~SpaceDeclaration()
{
}

void SpaceDeclaration::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.bssAdvance( mSize );
}

void SpaceDeclaration::emit( EmitSession & session ) const
{
  throw NYIException( position(), L"Emit" );
}

wchar_t const* SpaceDeclaration::getName()
{
  return L"ds";
}

} //namespace cmd
} //namespace las

