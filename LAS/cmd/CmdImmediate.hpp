#ifndef LAS_CMD_CMDIMMEDIATE_HPP
#define LAS_CMD_CMDIMMEDIATE_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdImmediate : public Command
{
public:
  CmdImmediate( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdImmediate();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  unsigned char mOp;
  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDIMMEDIATE_HPP
