#ifndef LAS_CMD_CMDABSOLUTE_HPP
#define LAS_CMD_CMDABSOLUTE_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdAbsolute : public Command
{
public:
  CmdAbsolute( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdAbsolute();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  // sizes of the argument
  int mOp1;
  int mOp2;
  int mOp3;

  typedef int Kind;

  static const Kind K_ABSOLUTE = 0x01;
  static const Kind K_RELATIVE = 0x02;
  static const Kind K_ACCESS   = 0x04;
  static const Kind K_BRANCH   = 0x08;
  static const Kind K_ABSOLUTE_BRANCH = K_ABSOLUTE | K_BRANCH;
  static const Kind K_ABSOLUTE_ACCESS = K_ABSOLUTE | K_ACCESS;
  static const Kind K_RELATIVE_BRANCH = K_RELATIVE | K_BRANCH;
  static const Kind K_RELATIVE_ACCESS = K_RELATIVE | K_ACCESS;

  Kind mKind;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDABSOLUTE_HPP
