#ifndef LAS_CMD_CMDSTACKRELATIVEINDIRECTINDEXED_HPP
#define LAS_CMD_CMDSTACKRELATIVEINDIRECTINDEXED_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdStackRelativeIndirectIndexed : public Command
{
public:
  CmdStackRelativeIndirectIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdStackRelativeIndirectIndexed();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDSTACKRELATIVEINDIRECTINDEXED_HPP
