#include "stdafx.h"
#include "CmdImmediate.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdImmediate::~CmdImmediate()
{
}

void CmdImmediate::gather( GatherSession & session ) const
{
  Command::gather( session );
  switch ( mOp )
  {
  // memory
  case ADC_IMM:
  case AND_IMM:
  case BIT_IMM:
  case CMP_IMM:
  case EOR_IMM:
  case LDA_IMM:
  case ORA_IMM:
  case SBC_IMM:
    session.cmdAdvance( 1 + session.memSize() );
    break;
  // index
  case CPX_IMM:
  case CPY_IMM:
  case LDY_IMM:
  case LDX_IMM:
    session.cmdAdvance( 1 + session.idxSize() );
    break;
  // 8-bit
  case COP_IMM:
  case REP_IMM:
  case SEP_IMM:
    session.cmdAdvance( 1 + 1 );
    break;
  // 16-bit
  case PEA_IMM:
    session.cmdAdvance( 1 + 2 );
    break;
  default:
    assert( false );
  }
}

void CmdImmediate::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session );
  int v = h.getValue();
  switch ( mOp )
  {
  // memory
  case ADC_IMM:
  case AND_IMM:
  case BIT_IMM:
  case CMP_IMM:
  case EOR_IMM:
  case LDA_IMM:
  case ORA_IMM:
  case SBC_IMM:
    if ( session.memSize() == 1 )
    {
      if ( v >= -128 && v < 256 )
      {
        session.emit( position(), mOp, (unsigned char)v );
      }
      else
        throw SemanticException( h.position(), ( boost::wformat( L"Given value of %d ($%x) does not fit on byte" ) % v % v ).str() );
    }
    else
      session.emit( position(), mOp, (unsigned short)v );
    break;
  // index
  case CPX_IMM:
  case CPY_IMM:
  case LDY_IMM:
  case LDX_IMM:
    if ( session.idxSize() == 1 )
    {
      if ( v >= -128 && v < 256 )
      {
        session.emit( position(), mOp, (unsigned char)v );
      }
      else
        throw SemanticException( h.position(), ( boost::wformat( L"Given value of %d ($%x) does not fit on byte" ) % v % v ).str() );
    }
    else
      session.emit( position(), mOp, (unsigned short)v );
    break;
  // 8-bit
  case COP_IMM:
  case REP_IMM:
  case SEP_IMM:
    {
      if ( v >= -128 && v < 256 )
      {
        session.emit( position(), mOp, (unsigned char)v );
      }
      else
        throw SemanticException( h.position(), ( boost::wformat( L"Given value of %d ($%x) does not fit on byte" ) % v % v ).str() );
    }
    break;
  // 16-bit
  case PEA_IMM:
    session.emit( position(), mOp, (unsigned short)v );
    break;
  default:
    assert( false );
  }
}


CmdImmediate::CmdImmediate( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) )
{

  switch ( mnemonic )
  {
  case LDY:
    mOp = LDY_IMM;
    break;
  case CPY:
    mOp = CPY_IMM;
    break;
  case CPX:
    mOp = CPX_IMM;
    break;
  case LDX:
    mOp = LDX_IMM;
    break;
  case ORA:
    mOp = ORA_IMM;
    break;
  case AND:
    mOp = AND_IMM;
    break;
  case EOR:
    mOp = EOR_IMM;
    break;
  case ADC:
    mOp = ADC_IMM;
    break;
  case BIT:
    mOp = BIT_IMM;
    break;
  case LDA:
    mOp = LDA_IMM;
    break;
  case CMP:
    mOp = CMP_IMM;
    break;
  case SBC:
    mOp = SBC_IMM;
    break;
  case REP:
    mOp = REP_IMM;
    break;
  case SEP:
    mOp = SEP_IMM;
    break;
  case PEA:
    mOp = PEA_IMM;
    break;
  case COP:
    mOp = COP_IMM;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have immediate addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

