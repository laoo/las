#include "stdafx.h"
#include "CmdMove.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdMove::~CmdMove()
{
}

void CmdMove::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.cmdAdvance( 1 + 1 + 1 );
}

void CmdMove::emit( EmitSession & session ) const
{
  value::Host hSrc = mSrc.calcValue( session );
  if ( !hSrc.isAddress() )
    throw SemanticException( position(), L"Source argument must be an address which bank will be taken" );
  value::Host hDst = mDst.calcValue( session );
  if ( !hDst.isAddress() )
    throw SemanticException( position(), L"Destination argument must be an address which bank will be taken" );
  int srcBank = *hSrc.getSegment()->bank();
  int dstBank = *hDst.getSegment()->bank();

  session.emit( position(), mOp, (unsigned short)( ( srcBank << 8 ) | dstBank ) );
}


CmdMove::CmdMove( GE5::Position const& position, Mnemonic mnemonic, Value src, Value dst ) :
  Command( position ), mSrc( std::move( src ) ), mDst( std::move( dst ) ), mOp( -1 )
{

  switch ( mnemonic )
  {
  case MVN:
    mOp = MVN_MOV;
    break;
  case MVP:
    mOp = MVP_MOV;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have move addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

