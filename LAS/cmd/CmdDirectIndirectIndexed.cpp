#include "stdafx.h"
#include "CmdDirectIndirectIndexed.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdDirectIndirectIndexed::~CmdDirectIndirectIndexed()
{
}

void CmdDirectIndirectIndexed::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.cmdAdvance( 1 + 1 );
}

void CmdDirectIndirectIndexed::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  session.emit( position(), mOp, (unsigned char)adr );
}


CmdDirectIndirectIndexed::CmdDirectIndirectIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp( -1 )
{

  switch ( mnemonic )
  {
  case ADC:
    mOp = ADC_DIY;
    break;
  case AND:
    mOp = AND_DIY;
    break;
  case CMP:
    mOp = CMP_DIY;
    break;
  case EOR:
    mOp = EOR_DIY;
    break;
  case LDA:
    mOp = LDA_DIY;
    break;
  case ORA:
    mOp = ORA_DIY;
    break;
  case SBC:
    mOp = SBC_DIY;
    break;
  case STA:
    mOp = STA_DIY;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have direct indirect indexed addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

