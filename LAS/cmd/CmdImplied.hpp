#ifndef LAS_CMD_CMDIMPLIED_HPP
#define LAS_CMD_CMDIMPLIED_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdImplied : public Command
{
public:
  CmdImplied( GE5::Position const& position, Mnemonic mnemonic );
  virtual ~CmdImplied();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  unsigned char mOp;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDIMPLIED_HPP
