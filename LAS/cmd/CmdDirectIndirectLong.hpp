#ifndef LAS_CMD_CMDDIRECTINDIRECTLONG_HPP
#define LAS_CMD_CMDDIRECTINDIRECTLONG_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdDirectIndirectLong : public Command
{
public:
  CmdDirectIndirectLong( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdDirectIndirectLong();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDDIRECTINDIRECTLONG_HPP
