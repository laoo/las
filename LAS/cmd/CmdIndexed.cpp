#include "stdafx.h"
#include "CmdIndexed.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "value/Measure.hpp"
#include "value/Host.hpp"
#include "SegmentProxy.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdIndexed::~CmdIndexed()
{
}

void CmdIndexed::gather( GatherSession & session ) const
{
  Command::gather( session );

  if ( mOp1 != -1 && mOp2 == -1 && mOp3 == -1 || mValue.directed() )
    session.cmdAdvance( 1 + 1 );
  else if ( mOp1 == -1 && mOp2 != -1 && mOp3 == -1 || mValue.absoluted() )
    session.cmdAdvance( 1 + 2 );
  else if ( mValue.longened() )
    session.cmdAdvance( 1 + 3 );
  else
  {
    value::Measure m = mValue.measureValue( session, value::U_ACCESS );

    if ( !m.isAddress() )
      throw SemanticException( m.position(), L"Argument must be an address" );
  
    switch ( m.getDecoration() )
    {
    case value::D_DIRECTED:
      session.cmdAdvance( 1 + 1 );
      break;
    case value::D_ABSOLUTED:
      session.cmdAdvance( 1 + 2 );
      break;
    case value::D_LONGENED:
      session.cmdAdvance( 1 + 3 );
      break;
    case value::D_NONE:
      {
        boost::tribool sameBank = session.currentDataSegment()->sameBank( m.getSegment() );

        if ( sameBank )
        {
          session.cmdAdvance( 1 + 2 );
        }
        else if ( !sameBank ) //not same bank
        {
          session.cmdAdvance( 1 + 3 );
        }
        else // don't know
        {
          throw SemanticException( m.position(), L"Can't determine argument's bank" );
        }
      }
      break;
    default:
      assert( false );
      break;
    }
  }
}

void CmdIndexed::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  switch ( h.getDecoration() )
  {
  case value::D_DIRECTED:
    if ( mOp1 != -1 )
    {
      session.emit( position(), mOp1, (unsigned char)adr );
      return;
    }
    break;
  case value::D_ABSOLUTED:
    if ( mOp2 != -1 )
    {
      session.emit( position(), mOp2, (unsigned short)adr );
      return;
    }
    break;
  case value::D_LONGENED:
    if ( mOp3 != -1 )
    {
      session.emit( position(), mOp3, (unsigned int)adr );
      return;
    }
    break;
  case value::D_NONE:
    {
      boost::tribool sameBank = session.currentDataSegment()->sameBank( h.getSegment() );

      if ( sameBank )
      {
        if ( mOp2 != -1 )
        {
          session.emit( position(), mOp2, (unsigned short)adr );
          return;
        }
      }
      else if ( !sameBank ) //not same bank
      {
        if ( mOp3 != -1 )
        {
          session.emit( position(), mOp3, (unsigned int)h.getValue() );
          return;
        }
      }
      else // don't know
      {
        throw SemanticException( h.position(), L"Can't determine argument's bank" );
      }
    }
    break;
  default:
    assert( false );
    break;
  }

  throw SemanticException( position(), L"Bad argument addressing" );
}


CmdIndexed::CmdIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value, Register reg ) :
  Command( position ), mValue( std::move( value ) ), mOp1( -1 ), mOp2( -1 ), mOp3( -1 ), mReg( reg )
{
  switch ( mReg )
  {
  case X:
    switch ( mnemonic )
    {
    case ADC:
      mOp1 = ADC_DPX;
      mOp2 = ADC_ABX;
      mOp3 = ADC_ALX;
      break;
    case AND:
      mOp1 = AND_DPX;
      mOp2 = AND_ABX;
      mOp3 = AND_ALX;
      break;
    case ASL:
      mOp1 = ASL_DPX;
      mOp2 = ASL_ABX;
      break;
    case BIT:
      mOp1 = BIT_DPX;
      mOp2 = BIT_ABX;
      break;
    case CMP:
      mOp1 = CMP_DPX;
      mOp2 = CMP_ABX;
      mOp3 = CMP_ALX;
      break;
    case DEC:
      mOp1 = DEC_DPX;
      mOp2 = DEC_ABX;
      break;
    case EOR:
      mOp1 = EOR_DPX;
      mOp2 = EOR_ABX;
      mOp3 = EOR_ALX;
      break;
    case INC:
      mOp1 = INC_DPX;
      mOp2 = INC_ABX;
      break;
    case LDA:
      mOp1 = LDA_DPX;
      mOp2 = LDA_ABX;
      mOp3 = LDA_ALX;
      break;
    case LDY:
      mOp1 = LDY_DPX;
      mOp2 = LDY_ABX;
      break;
    case LSR:
      mOp1 = LSR_DPX;
      mOp2 = LSR_ABX;
      break;
    case ORA:
      mOp1 = ORA_DPX;
      mOp2 = ORA_ABX;
      mOp3 = ORA_ALX;
      break;
    case ROL:
      mOp1 = ROL_DPX;
      mOp2 = ROL_ABX;
      break;
    case ROR:
      mOp1 = ROR_DPX;
      mOp2 = ROR_ABX;
      break;
    case SBC:
      mOp1 = SBC_DPX;
      mOp2 = SBC_ABX;
      mOp3 = SBC_ALX;
      break;
    case STA:
      mOp1 = STA_DPX;
      mOp2 = STA_ABX;
      mOp3 = STA_ALX;
      break;
    case STY:
      mOp1 = STY_DPX;
      break;
    case STZ:
      mOp1 = STZ_DPX;
      mOp2 = STZ_ABX;
      break;
    default:
      throw SemanticException( position,
        std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have absolute, long, nor direct indexed with X addressing mode" );
      break;
    }
    break;
  case Y:
    switch ( mnemonic )
    {
    case ADC:
      mOp2 = ADC_ABY;
      break;
    case AND:
      mOp2 = AND_ABY;
      break;
    case CMP:
      mOp2 = CMP_ABY;
      break;
    case EOR:
      mOp2 = EOR_ABY;
      break;
    case LDA:
      mOp2 = LDA_ABY;
      break;
    case LDX:
      mOp1 = LDX_DPY;
      mOp2 = LDX_ABY;
      break;
    case ORA:
      mOp2 = ORA_ABY;
      break;
    case SBC:
      mOp2 = SBC_ABY;
      break;
    case STA:
      mOp2 = STA_ABY;
      break;
    case STX:
      mOp1 = STX_DPY;
      break;
    default:
      throw SemanticException( position,
        std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have absolute nor direct indexed with Y addressing mode" );
      break;
    }
    break;
  case S:
    switch ( mnemonic )
    {
    case ADC:
      mOp1 = ADC_SRL;
      break;
    case AND:
      mOp1 = AND_SRL;
      break;
    case CMP:
      mOp1 = CMP_SRL;
      break;
    case EOR:
      mOp1 = EOR_SRL;
      break;
    case LDA:
      mOp1 = LDA_SRL;
      break;
    case ORA:
      mOp1 = ORA_SRL;
      break;
    case SBC:
      mOp1 = SBC_SRL;
      break;
    case STA:
      mOp1 = STA_SRL;
      break;
    default:
      throw SemanticException( position,
        std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have stack relative addressing mode" );
      break;
    }
    break;
  default:
    assert( false );
    break;
  }
}

} //namespace cmd
} //namespace las

