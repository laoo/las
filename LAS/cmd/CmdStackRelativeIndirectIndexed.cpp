#include "stdafx.h"
#include "CmdStackRelativeIndirectIndexed.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdStackRelativeIndirectIndexed::~CmdStackRelativeIndirectIndexed()
{
}

void CmdStackRelativeIndirectIndexed::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.cmdAdvance( 1 + 1 );
}

void CmdStackRelativeIndirectIndexed::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  session.emit( position(), mOp, (unsigned char)adr );
}


CmdStackRelativeIndirectIndexed::CmdStackRelativeIndirectIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp( -1 )
{
  switch ( mnemonic )
  {
  case ADC:
    mOp = ADC_SIY;
    break;
  case AND:
    mOp = AND_SIY;
    break;
  case CMP:
    mOp = CMP_SIY;
    break;
  case EOR:
    mOp = EOR_SIY;
    break;
  case LDA:
    mOp = LDA_SIY;
    break;
  case ORA:
    mOp = ORA_SIY;
    break;
  case SBC:
    mOp = SBC_SIY;
    break;
  case STA:
    mOp = STA_SIY;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have stack relative indirect indexed addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

