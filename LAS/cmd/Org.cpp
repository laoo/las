#include "stdafx.h"
#include "Org.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

Org::Org( GE5::Position const& position, Value && v ) : Command( position ), mValue( std::move( v ) )
{
}

Org::~Org()
{
}

void Org::gather( GatherSession & session ) const
{
  Command::gather( session );
  session.setOrigin( position(), mValue );
}

void Org::emit( EmitSession & session ) const
{
  //nothing
}

wchar_t const* Org::getName()
{
  return L"org";
}

Value const& Org::getValue() const
{
  return mValue;
}



} //namespace cmd
} //namespace las

