#ifndef LAS_CMD_CMDINDIRECT_HPP
#define LAS_CMD_CMDINDIRECT_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdIndirect : public Command
{
public:
  CmdIndirect( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdIndirect();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDINDIRECT_HPP
