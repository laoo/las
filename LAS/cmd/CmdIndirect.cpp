#include "stdafx.h"
#include "CmdIndirect.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdIndirect::~CmdIndirect()
{
}

void CmdIndirect::gather( GatherSession & session ) const
{
  Command::gather( session );
  if ( mOp != JMP_ABI )
  {
    session.cmdAdvance( 1 + 1 );
  }
  else
  {
    session.cmdAdvance( 1 + 2 );
  }
}

void CmdIndirect::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  if ( mOp != JMP_ABI )
  {
    session.emit( position(), mOp, (unsigned char)adr );
  }
  else
  {
    session.emit( position(), mOp, (unsigned short)adr );
  }
}

CmdIndirect::CmdIndirect( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp( -1 )
{

  switch ( mnemonic )
  {
  case ADC:
    mOp = ADC_DIN;
    break;
  case AND:
    mOp = AND_DIN;
    break;
  case CMP:
    mOp = CMP_DIN;
    break;
  case EOR:
    mOp = EOR_DIN;
    break;
  case LDA:
    mOp = LDA_DIN;
    break;
  case ORA:
    mOp = ORA_DIN;
    break;
  case SBC:
    mOp = SBC_DIN;
    break;
  case STA:
    mOp = STA_DIN;
    break;
  case PEI:
  case PEA:
    mOp = PEA_DIN;
    break;
  case JMP:   //two bytes
    mOp = JMP_ABI;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have indirect addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

