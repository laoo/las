#include "stdafx.h"
#include "CmdAbsolute.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "value/Measure.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdAbsolute::~CmdAbsolute()
{
}

void CmdAbsolute::gather( GatherSession & session ) const
{
  Command::gather( session );

  if ( mOp1 != -1 && mOp2 == -1 && mOp3 == -1 || mValue.directed() )
    session.cmdAdvance( 1 + 1 );
  else if ( mOp1 == -1 && mOp2 != -1 && mOp3 == -1 || mValue.absoluted() )
    session.cmdAdvance( 1 + 2 );
  else if ( mValue.longened() )
    session.cmdAdvance( 1 + 3 );
  else
  {
    value::Measure m = mValue.measureValue( session, ( mKind & K_BRANCH ) != 0 ? value::U_BRANCH : value::U_ACCESS );

    if ( !m.isAddress() )
      throw SemanticException( m.position(), L"Argument must be an address" );

    switch ( m.getDecoration() )
    {
    case value::D_DIRECTED:
      session.cmdAdvance( 1 + 1 );
      break;
    case value::D_ABSOLUTED:
      session.cmdAdvance( 1 + 2 );
      break;
    case value::D_LONGENED:
      session.cmdAdvance( 1 + 3 );
      break;
    default:
      {
        boost::tribool sameBank;
        if ( ( mKind & K_BRANCH ) != 0 )
        {
          sameBank = session.currentCodeSegment()->sameBank( m.getSegment() );
        }
        else if ( ( mKind & K_ACCESS ) != 0 )
        {
          sameBank = session.currentDataSegment()->sameBank( m.getSegment() );
        }
        else
        {
          assert( false );
        }

        if ( sameBank )
        {
          session.cmdAdvance( 1 + 2 );
        }
        else if ( !sameBank ) //not same bank
        {
          session.cmdAdvance( 1 + 3 );
        }
        else // don't know
        {
          throw SemanticException( m.position(), L"Can't determine argument's bank" );
        }
      }
      break;
    }
  }
}

void CmdAbsolute::emit( EmitSession & session ) const
{
  if ( mKind == K_RELATIVE_BRANCH || mKind == K_RELATIVE_ACCESS ) //TODO: devise whether relative access needs separate path
  {
    value::Host h = mValue.calcValue( session, mKind == K_RELATIVE_BRANCH ? value::U_BRANCH : value::U_ACCESS );
    assert( h.isAddress() );

    if ( mOp1 != -1 )
    {
      if ( session.currentDataSegment() != h.getSegment() )
      {
        throw SemanticException( position(), L"Cannot do a short-branch to a different segment" );
      }

      int dest = h.getValue();

      if ( !h.getSegment()->isBound( dest ) )
      {
        throw SemanticException( position(), L"Cannot do a short-branch outside of a segment" );
      }

      int off = (int)session.currentSegmentOffset() + 2;
      int diff = dest - off;

      if ( diff < -128 || diff > 127 )
      {
        throw SemanticException( position(), ( boost::wformat( L"Short branch of %d too long" ) % diff ).str() );
      }

      session.emit( position(), mOp1, (unsigned char)diff );
    }
    else if ( mOp2 != -1 )
    {
      int dest = h.getValue();
      int off = (int)session.currentSegmentOffset() + 3;

      if ( session.currentDataSegment() == h.getSegment() && h.getSegment()->isBound( dest ) )  //relative path if bound to "this" segment
      {
        int diff = dest - off;
        session.emit( position(), mOp2, (unsigned short)diff );
      }
      else
      {
        int thisBaseAddress = session.currentDataSegment()->getOrigin();
        int otherBaseAddress = h.getSegment()->getOrigin();

        if ( thisBaseAddress >> 16 != otherBaseAddress >> 16 )
        {
          throw SemanticException( position(), ( boost::wformat( L"Cannot branch from segment %02x to segment %02x" )
            % ( thisBaseAddress >> 16 )
            % ( otherBaseAddress >> 16 ) ).str() );
        }

        int pc = thisBaseAddress + off;
        int diff = otherBaseAddress + dest - pc;

        session.emit( position(), mOp2, (unsigned short)diff );
      }
    }
    else
    {
      assert( false );
    }
  }
  else if ( mKind == K_ABSOLUTE_BRANCH )
  {
    value::Host h = mValue.calcValue( session, value::U_BRANCH );
    assert( h.isAddress() );

    int thisBaseAddress = session.currentCodeSegment()->getOrigin();
    int otherBaseAddress = h.getSegment()->getOrigin();
    bool sameBank = ( thisBaseAddress >> 16 ) == ( otherBaseAddress >> 16 );

    int v = h.getValue();
    switch ( h.getDecoration() )
    {
    case value::D_DIRECTED:
      throw SemanticException( position(), L"Can't jump with direct addressing" );
    case value::D_ABSOLUTED:
      if ( sameBank )
        session.emit( position(), mOp2, (unsigned short)( ( otherBaseAddress & 0xffff ) + v ) );
      else
        throw SemanticException( position(), L"Long jump required" );
       break;
    case value::D_LONGENED:
        session.emit( position(), mOp3, (unsigned int)( otherBaseAddress + v ) );
      break;
    default:
      if ( sameBank )
        session.emit( position(), mOp2, (unsigned short)( ( otherBaseAddress & 0xffff ) + v ) );
      else
        session.emit( position(), mOp3, (unsigned int)( otherBaseAddress + v ) );
      break;
    }
  }
  else if ( mKind == K_ABSOLUTE_ACCESS )
  {
    value::Host h = mValue.calcValue( session, value::U_ACCESS );
    assert( h.isAddress() );

    int otherBaseAddress = h.getSegment()->getOrigin();
    bool sameBank = ( session.currentDataSegment()->getOrigin() >> 16 ) == ( otherBaseAddress >> 16 );

    int v = h.getValue();
    v += sameBank ? otherBaseAddress & 0xffff : otherBaseAddress;
    switch ( h.getDecoration() )
    {
    case value::D_DIRECTED:
      if ( v >= -128 && v < 256 ) //TODO shouldn't the value already be clamped?
        session.emit( position(), mOp1, (unsigned char)v  );
      else
        throw SemanticException( position(), L"Value too big" );
      break;
    case value::D_ABSOLUTED:
      session.emit( position(), mOp2, (unsigned short)v );
      break;
    case value::D_LONGENED:
      assert( v < 256 * 65536 );
      session.emit( position(), mOp3, (unsigned int)v );
      break;
    default:
      if ( sameBank )
        session.emit( position(), mOp2, (unsigned short)v );
      else
        session.emit( position(), mOp3, (unsigned int)v );
      break;
    }
  }
  else
  {
    assert( false );
  }
}

CmdAbsolute::CmdAbsolute( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp1( -1 ), mOp2( -1 ), mOp3( -1 )
{

  switch ( mnemonic )
  {
  case ADC:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = ADC_DPR;
    mOp2 = ADC_ABS;
    mOp3 = ADC_ABL;
    break;
  case AND:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = AND_DPR;
    mOp2 = AND_ABS;
    mOp3 = AND_ABL;
    break;
  case ASL:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = ASL_DPR;
    mOp2 = ASL_ABS;
    break;
  case BIT:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = BIT_DPR;
    mOp2 = BIT_ABS;
    break;
  case CMP:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = CMP_DPR;
    mOp2 = CMP_ABS;
    mOp3 = CMP_ABL;
    break;
  case CPX:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = CPX_DPR;
    mOp2 = CPX_ABS;
    break;
  case CPY:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = CPY_DPR;
    mOp2 = CPY_ABS;
    break;
  case DEC:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = DEC_DPR;
    mOp2 = DEC_ABS;
    break;
  case EOR:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = EOR_DPR;
    mOp2 = EOR_ABS;
    mOp3 = EOR_ABL;
    break;
  case INC:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = INC_DPR;
    mOp2 = INC_ABS;
    break;
  case LDA:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = LDA_DPR;
    mOp2 = LDA_ABS;
    mOp3 = LDA_ABL;
    break;
  case LDX:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = LDX_DPR;
    mOp2 = LDX_ABS;
    break;
  case LDY:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = LDY_DPR;
    mOp2 = LDY_ABS;
    break;
  case LSR:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = LSR_DPR;
    mOp2 = LSR_ABS;
    break;
  case ORA:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = ORA_DPR;
    mOp2 = ORA_ABS;
    mOp3 = ORA_ABL;
    break;
  case ROL:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = ROL_DPR;
    mOp2 = ROL_ABS;
    break;
  case ROR:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = ROR_DPR;
    mOp2 = ROR_ABS;
    break;
  case SBC:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = SBC_DPR;
    mOp2 = SBC_ABS;
    mOp3 = SBC_ABL;
    break;
  case STA:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = STA_DPR;
    mOp2 = STA_ABS;
    mOp3 = STA_ABL;
    break;
  case STX:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = STX_DPR;
    mOp2 = STX_ABS;
    break;
  case STY:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = STY_DPR;
    mOp2 = STY_ABS;
    break;
  case STZ:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = STZ_DPR;
    mOp2 = STZ_ABS;
    break;
  case TRB:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = TRB_DPR;
    mOp2 = TRB_ABS;
    break;
  case TSB:
    mKind = K_ABSOLUTE_ACCESS;
    mOp1 = TSB_DPR;
    mOp2 = TSB_ABS;
    break;
  case BCC:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BCC_REL;
    break;
  case BCS:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BCS_REL;
    break;
  case BEQ:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BEQ_REL;
    break;
  case BNE:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BNE_REL;
    break;
  case BMI:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BMI_REL;
    break;
  case BPL:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BPL_REL;
    break;
  case BRA:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BRA_REL;
    break;
  case BVC:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BVC_REL;
    break;
  case BVS:
    mKind = K_RELATIVE_BRANCH;
    mOp1 = BVS_REL;
    break;
  case PEA:
    mKind = K_ABSOLUTE_ACCESS;
    mOp2 = PEA_IMM;
    break;
  case PER:
    mKind = K_RELATIVE_ACCESS;
    mOp2 = PEA_RLL;
    break;
  case BRL:
    mKind = K_RELATIVE_BRANCH;
    mOp2 = BRL_RLL;
    break;
  case JMP:
    mKind = K_ABSOLUTE_BRANCH;
    mOp2 = JMP_ABS;
    mOp3 = JMP_ABL;
    break;
  case JSR:
    mKind = K_ABSOLUTE_BRANCH;
    mOp2 = JSR_ABS;
    mOp3 = JSR_ABL;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have absolute, direct or relative addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

