#include "stdafx.h"
#include "CmdIndexedIndirect.hpp"
#include "GatherSession.hpp"
#include "EmitSession.hpp"
#include "SegmentProxy.hpp"
#include "value/Host.hpp"
#include "Exception.hpp"

namespace las
{
namespace cmd
{

CmdIndexedIndirect::~CmdIndexedIndirect()
{
}

void CmdIndexedIndirect::gather( GatherSession & session ) const
{
  Command::gather( session );

  if ( mOp1 != -1 && mOp2 == -1 )
  {
    session.cmdAdvance( 1 + 1 );
  }
  else if ( mOp1 == -1 && mOp2 != -1 )
  {
    session.cmdAdvance( 1 + 2 );
  }
  else
  {
    assert( false );
  }
}

void CmdIndexedIndirect::emit( EmitSession & session ) const
{
  value::Host h = mValue.calcValue( session, value::U_ACCESS );
  unsigned int adr = h.getSegment()->getOrigin() + h.getValue();

  if ( mOp1 != -1 && mOp2 == -1 )
  {
    session.emit( position(), mOp1, (unsigned char)adr );
  }
  else if ( mOp1 == -1 && mOp2 != -1 )
  {
    session.emit( position(), mOp2, (unsigned short)adr );
  }
  else
  {
    assert( false );
  }
}

CmdIndexedIndirect::CmdIndexedIndirect( GE5::Position const& position, Mnemonic mnemonic, Value value ) :
  Command( position ), mValue( std::move( value ) ), mOp1( -1 ), mOp2( -1 )
{
  switch ( mnemonic )
  {
  case ADC:
    mOp1 = ADC_DXI;
    break;
  case AND:
    mOp1 = AND_DXI;
    break;
  case CMP:
    mOp1 = CMP_DXI;
    break;
  case EOR:
    mOp1 = EOR_DXI;
    break;
  case LDA:
    mOp1 = LDA_DXI;
    break;
  case ORA:
    mOp1 = ORA_DXI;
    break;
  case SBC:
    mOp1 = SBC_DXI;
    break;
  case STA:
    mOp1 = STA_DXI;
    break;
  case JSR:
    mOp2 = JSR_AXI;
    break;
  case JMP:
    mOp2 = JMP_AXI;
    break;
  default:
    throw SemanticException( position,
      std::wstring( L"Command " ) + OpcodeNames[ mnemonic ] + L" does not have indexed indirect addressing mode" );
    break;
  }
}



} //namespace cmd
} //namespace las

