#ifndef LAS_CMD_OPCODES_HPP
#define LAS_CMD_OPCODES_HPP

#include "Command.hpp"

namespace las
{
namespace cmd
{
  enum Mnemonic : int
  {
    ADC,
    AND,
    ASL,
    BCC,
    BCS,
    BEQ,
    BIT,
    BMI,
    BNE,
    BPL,
    BRA,
    BRK,
    BRL,
    BVC,
    BVS,
    CLC,
    CLD,
    CLI,
    CLV,
    CMP,
    COP,
    CPX,
    CPY,
    DEC,
    DEX,
    DEY,
    EOR,
    INC,
    INX,
    INY,
    JMP,
    JSR,
    LDA,
    LDX,
    LDY,
    LSR,
    MVN,
    MVP,
    NOP,
    ORA,
    PEA,
    PEI,
    PER,
    PHA,
    PHB,
    PHD,
    PHK,
    PHP,
    PHX,
    PHY,
    PLA,
    PLB,
    PLD,
    PLP,
    PLX,
    PLY,
    REP,
    ROL,
    ROR,
    RTI,
    RTL,
    RTS,
    SBC,
    SEC,
    SED,
    SEI,
    SEP,
    STA,
    STP,
    STX,
    STY,
    STZ,
    TAX,
    TAY,
    TCD,
    TCS,
    TDC,
    TRB,
    TSB,
    TSC,
    TSX,
    TXA,
    TXS,
    TXY,
    TYA,
    TYX,
    WAI,
    WDM,
    XBA,
    XCE
  };

  enum Opcodes : int
  {
    BRK_IMP = 0x00,   //implied
    ORA_DXI = 0x01,   //direct indexed indirect
    COP_IMM = 0x02,   //immediate
    ORA_SRL = 0x03,   //stack relative
    TSB_DPR = 0x04,   //direct
    ORA_DPR = 0x05,
    ASL_DPR = 0x06,
    ORA_DIL = 0x07,   //direct indirect long
    PHP_IMP = 0x08,
    ORA_IMM = 0x09,
    ASL_IMP = 0x0A,
    PHD_IMP = 0x0B,
    TSB_ABS = 0x0C,   //absolute
    ORA_ABS = 0x0D,
    ASL_ABS = 0x0E,
    ORA_ABL = 0x0F,   //absolute long
    BPL_REL = 0x10,   //relative
    ORA_DIY = 0x11,   //direct indirect indexed
    ORA_DIN = 0x12,   //direct indirect
    ORA_SIY = 0x13,   //stack relative indirect indexed
    TRB_DPR = 0x14,
    ORA_DPX = 0x15,   //direct indexed X
    ASL_DPX = 0x16,
    ORA_DLY = 0x17,   //direct indirect long indexed
    CLC_IMP = 0x18,
    ORA_ABY = 0x19,   //absolute indexed Y
    INC_IMP = 0x1A,
    TCS_IMP = 0x1B,
    TRB_ABS = 0x1C,
    ORA_ABX = 0x1D,   //absolute indexed X
    ASL_ABX = 0x1E,
    ORA_ALX = 0x1F,   //absolute long indexed X
    JSR_ABS = 0x20,
    AND_DXI = 0x21,
    JSR_ABL = 0x22,
    AND_SRL = 0x23,
    BIT_DPR = 0x24,
    AND_DPR = 0x25,
    ROL_DPR = 0x26,
    AND_DIL = 0x27,
    PLP_IMP = 0x28,
    AND_IMM = 0x29,
    ROL_IMP = 0x2A,
    PLD_IMP = 0x2B,
    BIT_ABS = 0x2C,
    AND_ABS = 0x2D,
    ROL_ABS = 0x2E,
    AND_ABL = 0x2F,
    BMI_REL = 0x30,
    AND_DIY = 0x31,
    AND_DIN = 0x32,
    AND_SIY = 0x33,
    BIT_DPX = 0x34,
    AND_DPX = 0x35,
    ROL_DPX = 0x36,
    AND_DLY = 0x37,
    SEC_IMP = 0x38,
    AND_ABY = 0x39,
    DEC_IMP = 0x3A,
    TSC_IMP = 0x3B,
    BIT_ABX = 0x3C,
    AND_ABX = 0x3D,
    ROL_ABX = 0x3E,
    AND_ALX = 0x3F,
    RTI_IMP = 0x40,
    EOR_DXI = 0x41,
    WDM_IMP = 0x42,
    EOR_SRL = 0x43,
    MVP_MOV = 0x44,   //move
    EOR_DPR = 0x45,
    LSR_DPR = 0x46,
    EOR_DIL = 0x47,
    PHA_IMP = 0x48,
    EOR_IMM = 0x49,
    LSR_IMP = 0x4A,
    PHK_IMP = 0x4B,
    JMP_ABS = 0x4C,
    EOR_ABS = 0x4D,
    LSR_ABS = 0x4E,
    EOR_ABL = 0x4F,
    BVC_REL = 0x50,
    EOR_DIY = 0x51,
    EOR_DIN = 0x52,
    EOR_SIY = 0x53,
    MVN_MOV = 0x54,
    EOR_DPX = 0x55,
    LSR_DPX = 0x56,
    EOR_DLY = 0x57,
    CLI_IMP = 0x58,
    EOR_ABY = 0x59,
    PHY_IMP = 0x5A,
    TCD_IMP = 0x5B,
    JMP_ABL = 0x5C,
    EOR_ABX = 0x5D,
    LSR_ABX = 0x5E,
    EOR_ALX = 0x5F,
    RTS_IMP = 0x60,
    ADC_DXI = 0x61,
    PEA_RLL = 0x62,   //relative long
    ADC_SRL = 0x63,
    STZ_DPR = 0x64,
    ADC_DPR = 0x65,
    ROR_DPR = 0x66,
    ADC_DIL = 0x67,
    PLA_IMP = 0x68,
    ADC_IMM = 0x69,
    ROR_IMP = 0x6A,
    RTL_IMP = 0x6B,
    JMP_ABI = 0x6C,   //absolute indirect
    ADC_ABS = 0x6D,
    ROR_ABS = 0x6E,
    ADC_ABL = 0x6F,
    BVS_REL = 0x70,
    ADC_DIY = 0x71,
    ADC_DIN = 0x72,
    ADC_SIY = 0x73,
    STZ_DPX = 0x74,
    ADC_DPX = 0x75,
    ROR_DPX = 0x76,
    ADC_DLY = 0x77,
    SEI_IMP = 0x78,
    ADC_ABY = 0x79,
    PLY_IMP = 0x7A,
    TDC_IMP = 0x7B,
    JMP_AXI = 0x7C,   //absolute indexed indirect
    ADC_ABX = 0x7D,
    ROR_ABX = 0x7E,
    ADC_ALX = 0x7F,
    BRA_REL = 0x80,
    STA_DXI = 0x81,
    BRL_RLL = 0x82,
    STA_SRL = 0x83,
    STY_DPR = 0x84,
    STA_DPR = 0x85,
    STX_DPR = 0x86,
    STA_DIL = 0x87,
    DEY_IMP = 0x88,
    BIT_IMM = 0x89,
    TXA_IMP = 0x8A,
    PHB_IMP = 0x8B,
    STY_ABS = 0x8C,
    STA_ABS = 0x8D,
    STX_ABS = 0x8E,
    STA_ABL = 0x8F,
    BCC_REL = 0x90,
    STA_DIY = 0x91,
    STA_DIN = 0x92,
    STA_SIY = 0x93,
    STY_DPX = 0x94,
    STA_DPX = 0x95,
    STX_DPY = 0x96,   //direct indexed Y
    STA_DLY = 0x97,
    TYA_IMP = 0x98,
    STA_ABY = 0x99,
    TXS_IMP = 0x9A,
    TXY_IMP = 0x9B,
    STZ_ABS = 0x9C,
    STA_ABX = 0x9D,
    STZ_ABX = 0x9E,
    STA_ALX = 0x9F,
    LDY_IMM = 0xA0,
    LDA_DXI = 0xA1,
    LDX_IMM = 0xA2,
    LDA_SRL = 0xA3,
    LDY_DPR = 0xA4,
    LDA_DPR = 0xA5,
    LDX_DPR = 0xA6,
    LDA_DIL = 0xA7,
    TAY_IMP = 0xA8,
    LDA_IMM = 0xA9,
    TAX_IMP = 0xAA,
    PLB_IMP = 0xAB,
    LDY_ABS = 0xAC,
    LDA_ABS = 0xAD,
    LDX_ABS = 0xAE,
    LDA_ABL = 0xAF,
    BCS_REL = 0xB0,
    LDA_DIY = 0xB1,
    LDA_DIN = 0xB2,
    LDA_SIY = 0xB3,
    LDY_DPX = 0xB4,
    LDA_DPX = 0xB5,
    LDX_DPY = 0xB6,
    LDA_DLY = 0xB7,
    CLV_IMP = 0xB8,
    LDA_ABY = 0xB9,
    TSX_IMP = 0xBA,
    TYX_IMP = 0xBB,
    LDY_ABX = 0xBC,
    LDA_ABX = 0xBD,
    LDX_ABY = 0xBE,
    LDA_ALX = 0xBF,
    CPY_IMM = 0xC0,
    CMP_DXI = 0xC1,
    REP_IMM = 0xC2,
    CMP_SRL = 0xC3,
    CPY_DPR = 0xC4,
    CMP_DPR = 0xC5,
    DEC_DPR = 0xC6,
    CMP_DIL = 0xC7,
    INY_IMP = 0xC8,
    CMP_IMM = 0xC9,
    DEX_IMP = 0xCA,
    WAI_IMP = 0xCB,
    CPY_ABS = 0xCC,
    CMP_ABS = 0xCD,
    DEC_ABS = 0xCE,
    CMP_ABL = 0xCF,
    BNE_REL = 0xD0,
    CMP_DIY = 0xD1,
    CMP_DIN = 0xD2,
    CMP_SIY = 0xD3,
    PEA_DIN = 0xD4,
    CMP_DPX = 0xD5,
    DEC_DPX = 0xD6,
    CMP_DLY = 0xD7,
    CLD_IMP = 0xD8,
    CMP_ABY = 0xD9,
    PHX_IMP = 0xDA,
    STP_IMP = 0xDB,
    JMP_AIL = 0xDC,   //absolute indirect long
    CMP_ABX = 0xDD,
    DEC_ABX = 0xDE,
    CMP_ALX = 0xDF,
    CPX_IMM = 0xE0,
    SBC_DXI = 0xE1,
    SEP_IMM = 0xE2,
    SBC_SRL = 0xE3,
    CPX_DPR = 0xE4,
    SBC_DPR = 0xE5,
    INC_DPR = 0xE6,
    SBC_DIL = 0xE7,
    INX_IMP = 0xE8,
    SBC_IMM = 0xE9,
    NOP_IMP = 0xEA,
    XBA_IMP = 0xEB,
    CPX_ABS = 0xEC,
    SBC_ABS = 0xED,
    INC_ABS = 0xEE,
    SBC_ABL = 0xEF,
    BEQ_REL = 0xF0,
    SBC_DIY = 0xF1,
    SBC_DIN = 0xF2,
    SBC_SIY = 0xF3,
    PEA_IMM = 0xF4,
    SBC_DPX = 0xF5,
    INC_DPX = 0xF6,
    SBC_DLY = 0xF7,
    SED_IMP = 0xF8,
    SBC_ABY = 0xF9,
    PLX_IMP = 0xFA,
    XCE_IMP = 0xFB,
    JSR_AXI = 0xFC,   //absolute indexed indirext
    SBC_ABX = 0xFD,
    INC_ABX = 0xFE,
    SBC_ALX = 0xFF,
  };

extern wchar_t const* const OpcodeNames[];

} //namespace cmd
} //namespace las

#endif //LAS_CMD_OPCODES_HPP
