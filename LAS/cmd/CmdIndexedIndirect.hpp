#ifndef LAS_CMD_CMDINDEXEDINDIRECT_HPP
#define LAS_CMD_CMDINDEXEDINDIRECT_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdIndexedIndirect : public Command
{
public:
  CmdIndexedIndirect( GE5::Position const& position, Mnemonic mnemonic, Value value );
  virtual ~CmdIndexedIndirect();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  int mOp1;
  int mOp2;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDINDEXEDINDIRECT_HPP
