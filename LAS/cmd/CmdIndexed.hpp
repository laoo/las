#ifndef LAS_CMD_CMDINDEXED_HPP
#define LAS_CMD_CMDINDEXED_HPP

#include "Command.hpp"
#include "Opcodes.hpp"

namespace las
{
namespace cmd
{

class CmdIndexed : public Command
{
public:

  enum Register
  {
    X,
    Y,
    S
  };

  CmdIndexed( GE5::Position const& position, Mnemonic mnemonic, Value value, Register reg );
  virtual ~CmdIndexed();

  virtual void gather( GatherSession & session ) const;
  virtual void emit( EmitSession & session ) const;

private:
  // sizes of the argument
  int mOp1;
  int mOp2;
  int mOp3;

  Register mReg;

  Value mValue;
};

} //namespace cmd
} //namespace las

#endif //LAS_CMD_CMDINDEXED_HPP
